package org.fullguys.swing.domain.usecases.incidents.impl;

import org.fullguys.swing.domain.entities.IncidentType;
import org.fullguys.swing.domain.entities.enums.IncidentTypeState;
import org.fullguys.swing.domain.usecases.incidents.ports.out.persistence.SaveIncidentTypePort;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import java.util.Date;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

class RegisterIncidentTypeUseCaseTest {

    @Test
    void givenIncidentTypeToRegister_thenIncidentShouldBeSavedAndEnabled() {
        var incidentType = givenAnIncidentTypeToRegister();
        var saveIncidentTypePort = givenTheSaveIncidentTypePort();

        var beforeRegister = new Date();
        var registerService = new RegisterIncidentTypeUseCase(saveIncidentTypePort);
        var incidentTypeRegistered = registerService.register(incidentType);
        var afterRegister = new Date();

        assertEquals("incident type description", incidentTypeRegistered.getDescription());
        assertEquals("incident type name", incidentTypeRegistered.getName());
        assertEquals(IncidentTypeState.ENABLED, incidentTypeRegistered.getState());

        var createdAt = incidentTypeRegistered.getCreatedAt();
        assertTrue(!createdAt.before(beforeRegister) && !createdAt.after(afterRegister));
    }

    private SaveIncidentTypePort givenTheSaveIncidentTypePort() {
        var saveIncidentTypePort = Mockito.mock(SaveIncidentTypePort.class);
        when(saveIncidentTypePort.save(any())).thenAnswer(i -> i.getArgument(0));
        return saveIncidentTypePort;
    }

    private IncidentType givenAnIncidentTypeToRegister() {
        return IncidentType.builder()
                .description("incident type description")
                .name("incident type name")
                .build();
    }
}
