package org.fullguys.swing.common.queries;

import lombok.Builder;
import lombok.Data;

import java.time.LocalDateTime;

@Data
@Builder
public class Filters {
    private String search;
    private LocalDateTime initDateRange;
    private LocalDateTime endDateRange;

    private Integer page;
    private Integer pagesize;
}
