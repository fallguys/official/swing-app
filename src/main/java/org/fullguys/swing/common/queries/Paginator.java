package org.fullguys.swing.common.queries;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

import java.util.List;

@Data
@Builder
@AllArgsConstructor
public class Paginator<T> {
    private List<T> data;
    private Integer page;
    private Integer pagesize;
    private Long total;
}
