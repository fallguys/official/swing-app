package org.fullguys.swing.common;

public class Messages {
    private static final String ACCOUNT_NOT_FOUND = "La cuenta %s no existe";
    private static final String WRONG_PASSWORD = "La contraseña no es correcta";
}
