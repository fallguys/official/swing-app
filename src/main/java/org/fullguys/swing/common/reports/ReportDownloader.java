package org.fullguys.swing.common.reports;

import net.sf.jasperreports.engine.*;
import net.sf.jasperreports.engine.data.JRTableModelDataSource;
import net.sf.jasperreports.engine.design.JasperDesign;
import net.sf.jasperreports.engine.export.JRXlsAbstractExporterParameter;
import net.sf.jasperreports.engine.export.JRXlsExporter;
import net.sf.jasperreports.engine.xml.JRXmlLoader;

import javax.swing.table.DefaultTableModel;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.DateFormat;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

public class ReportDownloader {
    public void downloadPDF(OutputStream outputStream,
                            String reportUrl, String filename, DefaultTableModel tableModel, Integer cant)
            throws JRException, IOException {
        Map parameters = new HashMap();
        parameters.put("Title", filename);
        parameters.put("Cant",cant);
        InputStream in = new URL(reportUrl).openStream();
        JasperReport jasperReport = JasperCompileManager.compileReport(in);
        JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, parameters,new JRTableModelDataSource(tableModel));
        JasperExportManager.exportReportToPdfStream(jasperPrint, outputStream);
    }

    public void downloadXLS(OutputStream outputStream,
                            String reportUrl, String filename, DefaultTableModel tableModel, Integer cant)
            throws JRException, IOException {

        Map parameters = new HashMap();
        parameters.put("Title", filename);
        parameters.put("Cant",cant);
        InputStream in = new URL(reportUrl).openStream();
        JasperReport jasperReport = JasperCompileManager.compileReport(in);
        JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, parameters,
                new JRTableModelDataSource(tableModel));
        JRXlsExporter exporter = new JRXlsExporter();
        exporter.setParameter(JRXlsAbstractExporterParameter.IS_ONE_PAGE_PER_SHEET, Boolean.FALSE);
        exporter.setParameter(JRXlsAbstractExporterParameter.IS_REMOVE_EMPTY_SPACE_BETWEEN_ROWS, Boolean.TRUE);
        exporter.setParameter(JRXlsAbstractExporterParameter.IS_REMOVE_EMPTY_SPACE_BETWEEN_COLUMNS, Boolean.TRUE);
        exporter.setParameter(JRXlsAbstractExporterParameter.IS_WHITE_PAGE_BACKGROUND, Boolean.TRUE);

        exporter.setParameter(JRExporterParameter.JASPER_PRINT, jasperPrint);
        exporter.setParameter(JRExporterParameter.OUTPUT_STREAM, outputStream);
        exporter.exportReport();
    }
}
