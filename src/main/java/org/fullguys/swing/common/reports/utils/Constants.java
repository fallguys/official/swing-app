package org.fullguys.swing.common.reports.utils;

public class Constants {
    public static final String TEMPLATE_INFO_BENEFICIARY = "https://pruebaswingapp.s3.amazonaws.com/reportBeneficiary.jrxml";
    public static final String TEMPLATE_INFO_WITHDRAWAL_POINT = "https://pruebaswingapp.s3.amazonaws.com/reportWithdrawalPoint.jrxml";
}
