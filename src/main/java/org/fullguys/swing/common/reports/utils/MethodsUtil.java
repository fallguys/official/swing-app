package org.fullguys.swing.common.reports.utils;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class MethodsUtil {
    public static String formatAssignedDate(LocalDateTime date){
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        return date.format(formatter);
    }

    public static String formatAssignedTime(LocalDateTime date){
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("HH:mm");
        return date.format(formatter);
    }

    public static String formatBonusRetired(Boolean bonusRetired){
        if (bonusRetired)
            return "Si";
        else return "No";
    }

    public static String assignedTime(String time1, String time2){
        return time1+" - "+time2;
    }
}
