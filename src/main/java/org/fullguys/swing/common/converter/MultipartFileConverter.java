package org.fullguys.swing.common.converter;

import org.fullguys.swing.common.annotations.PersistenceAdapter;
import org.fullguys.swing.common.annotations.S3Adapter;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

@S3Adapter
public class MultipartFileConverter {
    public File convertMultipartFileToFile(MultipartFile multipartFile) throws IOException {
        File file = new File(multipartFile.getOriginalFilename());
        FileOutputStream fileOutputStream = new FileOutputStream(file);
        fileOutputStream.write(multipartFile.getBytes());
        fileOutputStream.close();
        return file;
    }
}
