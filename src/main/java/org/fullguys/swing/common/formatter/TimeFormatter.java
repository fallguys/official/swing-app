package org.fullguys.swing.common.formatter;

import java.time.format.DateTimeFormatter;

public class TimeFormatter {
    public static final DateTimeFormatter DATE = DateTimeFormatter.ofPattern("dd/MM/yyyy");
    public static final DateTimeFormatter TIME = DateTimeFormatter.ofPattern("hh:mm a");
}
