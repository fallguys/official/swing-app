package org.fullguys.swing.adapters.persistence.repositories;

import org.fullguys.swing.adapters.persistence.models.ContagionAreaModel;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface ContagionAreaRepository extends JpaRepository<ContagionAreaModel, Long> {
    List<ContagionAreaModel> findAll();
    Long countAllBy();
}
