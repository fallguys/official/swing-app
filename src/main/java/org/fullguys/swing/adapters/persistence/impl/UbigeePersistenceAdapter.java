package org.fullguys.swing.adapters.persistence.impl;

import lombok.RequiredArgsConstructor;
import org.fullguys.swing.adapters.persistence.mappers.UbigeeMapper;
import org.fullguys.swing.adapters.persistence.repositories.UbigeeRepository;
import org.fullguys.swing.common.annotations.PersistenceAdapter;
import org.fullguys.swing.domain.entities.Ubigee;
import org.fullguys.swing.domain.entities.enums.UbigeeType;
import org.fullguys.swing.domain.usecases.beneficiaries.ports.out.persistence.ShowUbigeesPort;

import java.util.List;

@PersistenceAdapter
@RequiredArgsConstructor
public class UbigeePersistenceAdapter implements ShowUbigeesPort {
    private final UbigeeMapper ubigeeMapper;
    private final UbigeeRepository ubigeeRepository;

    @Override
    public List<Ubigee> showUbigees(Long id, UbigeeType ubigeeType) {
        if (ubigeeType.equals(UbigeeType.DEPARTMENT)){
            var ubigeesModel = ubigeeRepository.findAllByTypeEquals(ubigeeType.toString());
            var ubigees = ubigeeMapper.toUbigeeList(ubigeesModel);
            return ubigees;
        }
        else{
            var ubigeesModel = ubigeeRepository.findAllByFatherUbigee_IdAndTypeEquals(id, ubigeeType.toString());
            var ubigees = ubigeeMapper.toUbigeeList(ubigeesModel);
            return ubigees;
        }
    }
}
