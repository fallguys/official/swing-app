package org.fullguys.swing.adapters.persistence.mappers;

import org.fullguys.swing.adapters.persistence.models.BeneficiaryModel;
import org.fullguys.swing.adapters.persistence.models.WithdrawalPointChoosedModel;
import org.fullguys.swing.adapters.persistence.models.WithdrawalPointModel;
import org.fullguys.swing.domain.entities.Beneficiary;
import org.fullguys.swing.domain.entities.WithdrawalPoint;
import org.mapstruct.*;

import java.util.List;

@Mapper(componentModel="spring")
public interface WithdrawalPointChoosedMapper {
    @Mappings({
            @Mapping(target = "id", ignore = true),
            @Mapping(source = "withdrawalPoint.id", target = "withdrawalPointId"),
            @Mapping(target = "beneficiaryId", ignore = true)
    })

    WithdrawalPointChoosedModel toWithdrawalPointChoosedModel(WithdrawalPointModel withdrawalPoint, BeneficiaryModel beneficiary);

    @AfterMapping
    default void setBeneficiaryCode(BeneficiaryModel beneficiary, @MappingTarget WithdrawalPointChoosedModel withdrawalPointChoosedModels){
        withdrawalPointChoosedModels.setBeneficiaryId(beneficiary.getId());
    }
}
