package org.fullguys.swing.adapters.persistence.mappers;

import org.fullguys.swing.adapters.persistence.models.IncidentModel;
import org.fullguys.swing.domain.entities.Incident;
import org.hibernate.annotations.ListIndexBase;
import org.mapstruct.InheritInverseConfiguration;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;

import java.util.List;

@Mapper(componentModel="spring", uses = {IncidentTypeMapper.class, BeneficiaryMapper.class})
public interface IncidentMapper {
    @Mappings({
            @Mapping(source = "id", target = "id"),
            @Mapping(source = "message", target = "message"),
            @Mapping(source = "beneficiary", target = "beneficiary"),
            @Mapping(source = "reportedDate", target = "reportedDate"),
            @Mapping(source = "incidentType", target = "type"),
    })
    Incident toIncident(IncidentModel incidentModel);
    List<Incident> toIncidentList(List<IncidentModel> incidentModelList);

    @InheritInverseConfiguration
    @Mapping(source = "type.id", target = "incidentTypeId")
    @Mapping(source = "beneficiary.id", target = "beneficiaryId")
    @Mapping(target = "incidentType", ignore = true)
    IncidentModel toIncidentModel(Incident incident);
    List<IncidentModel> toIncidentModelList(List<Incident> incidentList);

}
