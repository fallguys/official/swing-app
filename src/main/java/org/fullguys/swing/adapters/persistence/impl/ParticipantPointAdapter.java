package org.fullguys.swing.adapters.persistence.impl;

import lombok.RequiredArgsConstructor;
import org.fullguys.swing.adapters.persistence.mappers.ParticipantPointMapper;
import org.fullguys.swing.adapters.persistence.mappers.WithdrawalPointMapper;
import org.fullguys.swing.adapters.persistence.repositories.ParticipantPointRepository;
import org.fullguys.swing.adapters.persistence.repositories.WithdrawalPointRepository;
import org.fullguys.swing.common.annotations.PersistenceAdapter;
import org.fullguys.swing.common.queries.Filters;
import org.fullguys.swing.common.queries.Paginator;
import org.fullguys.swing.domain.entities.Assignment;
import org.fullguys.swing.domain.entities.WithdrawalPoint;
import org.fullguys.swing.domain.entities.enums.ScheduleState;
import org.fullguys.swing.domain.usecases.beneficiaries.ports.out.persistence.GetWithdrawalPointsByUbigeePort;
import org.fullguys.swing.domain.usecases.feedback.ports.out.persistence.GetParticipantWithdrawalPointPort;
import org.fullguys.swing.domain.usecases.scheduling.ports.out.scheduler.ShowWithdrawalPointInfoPort;
import org.springframework.data.domain.PageRequest;
import org.fullguys.swing.domain.usecases.scheduling.ports.in.services.ShowTotalWithdrawalPointsForScheduleService;
import org.fullguys.swing.domain.usecases.scheduling.ports.out.scheduler.ShowTotalWithdrawalPointsForSchedulePort;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@PersistenceAdapter
@RequiredArgsConstructor
public class ParticipantPointAdapter implements GetParticipantWithdrawalPointPort, ShowWithdrawalPointInfoPort, ShowTotalWithdrawalPointsForSchedulePort {

    private final WithdrawalPointMapper withdrawalPointMapper;
    private final ParticipantPointMapper participantPointMapper;
    private final ParticipantPointRepository participantPointRepository;
    private final WithdrawalPointRepository withdrawalPointRepository;

    @Override
    public Optional<WithdrawalPoint> getParticipantWithdrawalPoint(String codeWithdrawalPoint, Long idSchedule) {
        var row = participantPointRepository.findByWithdrawalPoint_CodeAndScheduleId(codeWithdrawalPoint, idSchedule);
        return row.map(r -> withdrawalPointMapper.toWithdrawalPoint(r.getWithdrawalPoint()));
    }

    @Override
    public Long getIdParticipantPoint(Long idPoint, Long idSchedule) {
        return participantPointRepository.getIdParticipantPoint(idPoint, idSchedule);
    }

    @Override
    public WithdrawalPoint getPoint(String code) {
        var row = withdrawalPointRepository.findByCode(code);
        return withdrawalPointMapper.toWithdrawalPoint(row);
    }

    @Override
    public Paginator<WithdrawalPoint> showBeneficiariesInfo(Filters filters) {
        var rows = participantPointRepository.findByScheduleIdEquals(
                (long) Integer.parseInt(filters.getSearch()),
                PageRequest.of(filters.getPage(), filters.getPagesize()));
        var assignments = participantPointMapper.toAssignmentList(rows.getContent());
        List<WithdrawalPoint> withdrawalPointList = new ArrayList<WithdrawalPoint>();
        for(Assignment assignment: assignments){
            withdrawalPointList.add(assignment.getWithdrawalPoint());
        }
        return new Paginator<>(withdrawalPointList, filters.getPage(),
                filters.getPagesize(), rows.getTotalElements());
    }

    @Override
    public Integer showTotalWithdrawalPointsForSchedule(Long scheduleId) {
        return participantPointRepository.countAllByScheduleId(scheduleId);
    }
}
