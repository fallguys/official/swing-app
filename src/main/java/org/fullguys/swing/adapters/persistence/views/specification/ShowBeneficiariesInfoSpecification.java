package org.fullguys.swing.adapters.persistence.views.specification;

import org.fullguys.swing.adapters.persistence.views.models.InfoBeneficiaryView;
import org.fullguys.swing.adapters.web.controllers.admin.schedules.ShowBeneficiariesInfoSearch;
import org.springframework.data.jpa.domain.Specification;

import javax.persistence.criteria.*;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

public class ShowBeneficiariesInfoSpecification implements Specification<InfoBeneficiaryView>{
    private ShowBeneficiariesInfoSearch criteria;

    public ShowBeneficiariesInfoSpecification(ShowBeneficiariesInfoSearch showBeneficiariesInfoSearch){
        criteria = showBeneficiariesInfoSearch;
    }

    @Override
    public Predicate toPredicate(Root<InfoBeneficiaryView> root, CriteriaQuery<?> query, CriteriaBuilder criteriaBuilder) {
        Path<Long> scheduleId = root.get("scheduleId");
        Path<String> beneficiaryCode = root.get("beneficiaryCode");
        Path<String> withdrawalPointCode = root.get("withdrawalPontCode");
        Path<String> ubigee = root.get("ubigee");
        Path<LocalDateTime> date = root.get("initialDateAssigned");
        Path<Boolean> isBonusRetired = root.get("bonusRetired");
        Path<Long> mistakes = root.get("mistakes");

        final List<Predicate> predicateList = new ArrayList<>();
        if (criteria.getScheduleId() != null)
            predicateList.add(criteriaBuilder.equal(scheduleId, criteria.getScheduleId()));

        if (criteria.getBeneficiaryCode() != null)
            predicateList.add(criteriaBuilder.equal(beneficiaryCode, criteria.getBeneficiaryCode()));

        if (criteria.getWithdrawalPointCode() != null)
            predicateList.add(criteriaBuilder.equal(withdrawalPointCode, criteria.getWithdrawalPointCode()));

        if (criteria.getUbigee() != null){
            predicateList.add(criteriaBuilder.like(ubigee, criteria.getUbigee()+"%"));
        }

        if (criteria.getInitialDate() != null && criteria.getFinalDate() != null){
            var iniDate = criteriaBuilder.greaterThanOrEqualTo(date, criteria.getInitialDate().atStartOfDay());
            var endDate = criteriaBuilder.lessThanOrEqualTo(date, criteria.getFinalDate().atTime(23, 59, 59));
            predicateList.add(iniDate);
            predicateList.add(endDate);
        }

        if (criteria.getState() != null)
            predicateList.add(criteriaBuilder.equal(isBonusRetired, criteria.getState()));

        query.orderBy(criteriaBuilder.desc(isBonusRetired), criteriaBuilder.desc(mistakes), criteriaBuilder.desc(date), criteriaBuilder.desc(beneficiaryCode));
        return criteriaBuilder.and(predicateList.toArray(new Predicate[predicateList.size()]));
    }
}
