package org.fullguys.swing.adapters.persistence.s3.config;

import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Configuration
@Getter @Setter
@ConfigurationProperties(prefix = "storage.aws")
public class StorageCredentials {
    private String region;
    private Credentials credentials;
    private Bucket bucket;

    @Getter @Setter
    public static class Credentials {
        private String accessKey;
        private String secretKey;
    }

    @Getter @Setter
    public static class Bucket {
        private String name;
    }
}
