package org.fullguys.swing.adapters.persistence.mappers;

import org.fullguys.swing.adapters.persistence.models.ContagionAreaModel;
import org.fullguys.swing.domain.entities.ContagionArea;
import org.mapstruct.InheritInverseConfiguration;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;

import java.util.List;

@Mapper(componentModel="spring")
public interface ContagionAreaMapper {

    @Mappings({
            @Mapping(source = "id", target = "id"),
            @Mapping(source = "district", target = "district"),
            @Mapping(source = "quantity", target = "quantity"),
            @Mapping(source = "createdAt", target = "createdAt"),
            @Mapping(source = "updatedAt", target = "updatedAt")
    })
    ContagionArea toContagionArea(ContagionAreaModel contagionAreaModel);

    @InheritInverseConfiguration
    @Mapping(source = "district.id", target = "districtId")
    @Mapping(target = "updatedAt", ignore = true)
    ContagionAreaModel tocontagionAreaModel(ContagionArea contagionArea);

    List<ContagionArea> toContagionAreas(List<ContagionAreaModel> contagionAreaModels);
}
