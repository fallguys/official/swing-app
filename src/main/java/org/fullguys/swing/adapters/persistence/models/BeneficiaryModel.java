package org.fullguys.swing.adapters.persistence.models;

import lombok.Data;
import org.fullguys.swing.adapters.persistence.utils.Constants;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.Date;
import java.util.List;

@Data
@Entity
@Table(name = "MT_BENEFICIARY")
public class BeneficiaryModel {

    @Id @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "beneficiary_id")
    private Long id;

    @Column(name = "district_id")
    private Long districtId;

    @Column(name = "code", length = Constants.BENEFICIARY_CODE_LENGTH)
    private String code;

    @Column(name = "is_older_adult")
    private Boolean isOlderAdult;

    @Column(name = "is_disabled")
    private Boolean isDisabled;

    @Column(name = "gender", length = Constants.BENEFICIARY_GENDER_LENGTH)
    private String gender;

    @Column(name = "state", length = Constants.BENEFICIARY_STATE_LENGTH)
    private String state;

    @Column(name = "created_at")
    private LocalDateTime createdAt;

    @Column(name = "updated_at")
    private LocalDateTime updatedAt;

    @Column(name = "hits", columnDefinition = "BIGINT DEFAULT 0")
    private Long hits = 0L;

    @Column(name = "infractions", columnDefinition = "BIGINT DEFAULT 0")
    private Long infractions = 0L;

    @ManyToOne()
    @JoinColumn(name = "district_id", insertable = false, updatable = false)
    private UbigeeModel district;

    @OneToMany()
    @JoinColumn(name = "beneficiary_id", referencedColumnName = "beneficiary_id", insertable = false, updatable = false)
    private List<WithdrawalPointChoosedModel> withdrawalPointChoosedModels;
}
