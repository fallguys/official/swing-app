package org.fullguys.swing.adapters.persistence.repositories;

import org.fullguys.swing.adapters.persistence.models.BeneficiaryModel;
import org.fullguys.swing.adapters.persistence.models.WithdrawalPointModel;
import org.fullguys.swing.domain.entities.WithdrawalPoint;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface WithdrawalPointRepository extends JpaRepository<WithdrawalPointModel, Long> {
    WithdrawalPointModel findByCode(String core);
    long countAllByState(String state);
    Page<WithdrawalPointModel> findAllByDistrictCode(String search, Pageable pageable);
    List<WithdrawalPointModel> findAllByCodeEqualsOrCodeEqualsOrCodeEquals(String code1, String code2, String code3);
    List<WithdrawalPointModel> findAllByDistrictCodeAndState(String code, String state);
}
