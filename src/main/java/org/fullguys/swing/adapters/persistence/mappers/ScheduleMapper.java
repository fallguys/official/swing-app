package org.fullguys.swing.adapters.persistence.mappers;

import org.fullguys.swing.adapters.persistence.models.ScheduleModel;
import org.fullguys.swing.domain.entities.Schedule;
import org.mapstruct.InheritInverseConfiguration;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import java.util.List;

@Mapper(componentModel="spring", uses = {AdministratorMapper.class})
public interface ScheduleMapper {

    @Mapping(source = "id", target = "id")
    @Mapping(source = "state", target = "state")
    @Mapping(source = "administrator", target = "administrator")
    @Mapping(source = "creationDate", target = "creationDate")
    @Mapping(source = "initialDate", target = "initialDate")
    @Mapping(source = "finalDate", target = "finalDate")
    @Mapping(source = "type", target = "type")
    @Mapping(target = "assignments", ignore = true)
    Schedule toSchedule(ScheduleModel scheduleModel);

    @InheritInverseConfiguration
    @Mapping(source = "administrator.id", target = "administratorId")
    ScheduleModel toScheduleModel(Schedule schedule);

    List<Schedule> toSchedules(List<ScheduleModel> scheduleModels);
}
