package org.fullguys.swing.adapters.persistence.impl;

import lombok.RequiredArgsConstructor;
import org.fullguys.swing.adapters.persistence.mappers.ScheduleMapper;
import org.fullguys.swing.adapters.persistence.repositories.ScheduleRepository;
import org.fullguys.swing.common.annotations.PersistenceAdapter;
import org.fullguys.swing.common.queries.Filters;
import org.fullguys.swing.common.queries.Paginator;
import org.fullguys.swing.domain.entities.Schedule;
import org.fullguys.swing.domain.entities.enums.ScheduleState;
import org.fullguys.swing.domain.entities.enums.ScheduleType;
import org.fullguys.swing.domain.usecases.feedback.ports.out.persistence.GetCurrentSchedulePort;
import org.fullguys.swing.domain.usecases.scheduling.ports.out.scheduler.ShowScheduleDetailsPort;
import org.fullguys.swing.domain.usecases.scheduling.ports.out.persistence.*;
import org.springframework.data.domain.PageRequest;

import java.util.Optional;

@PersistenceAdapter
@RequiredArgsConstructor
public class SchedulePersistenceAdapter implements GetSchedulesPort, GetLastScheduleRegisteredPort, CheckForScheduleProcessingPort, RegisterSchedulePort, GetCurrentSchedulePort, ShowScheduleDetailsPort, UpdateScheduleStatePort {

    private final ScheduleRepository scheduleRepository;
    private final ScheduleMapper scheduleMapper;

    @Override
    public Paginator<Schedule> getSchedules(ScheduleType scheduleType, Filters filters) {
        var rows = scheduleRepository.findAllByTypeOrderById(scheduleType.name(), PageRequest.of(filters.getPage(), filters.getPagesize()));
        var schedules = scheduleMapper.toSchedules(rows.getContent());
        return new Paginator<>(schedules, filters.getPage(), filters.getPagesize(), rows.getTotalElements());
    }

    @Override
    public Optional<Schedule> getSchedule(Long idSchedule) {
        var row = scheduleRepository.findById(idSchedule);
        return row.map(scheduleMapper::toSchedule);
    }

    @Override
    public Optional<Schedule> getLastRegistered(ScheduleType scheduleType, ScheduleState scheduleState) {
        var row = scheduleRepository.findFirstByTypeAndStateOrderByFinalDateDesc(scheduleType.name(), scheduleState.name());
        return row.map(scheduleMapper::toSchedule);
    }

    @Override
    public boolean checkForScheduleProcessing() {
        return scheduleRepository.existsByState(ScheduleState.PROCESSING.name());
    }

    @Override
    public Schedule registerSchedule(Schedule schedule) {
        var row = scheduleMapper.toScheduleModel(schedule);
        var saved = scheduleRepository.save(row);
        return scheduleMapper.toSchedule(saved);
    }

    @Override
    public Optional<Schedule> getCurrentSchedule() {
        var row = scheduleRepository.findByStateEquals(ScheduleState.ACTIVE.toString());
        return row.map(scheduleMapper::toSchedule);
    }

    @Override
    public Schedule showScheduleDetails(Long scheduleId) {
        var row = scheduleRepository.findById(scheduleId);
        var schedule = row.map(scheduleMapper::toSchedule);
        return schedule.orElseThrow();
    }

    @Override
    public void updateScheduleState(Schedule schedule) {
        var row = scheduleRepository.findById(schedule.getId());
        row.ifPresent(r -> {
            r.setType(schedule.getType().toString());
            r.setState(schedule.getState().toString());
            scheduleRepository.save(r);
        });
    }
}
