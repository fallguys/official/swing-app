package org.fullguys.swing.adapters.persistence.repositories;

import org.fullguys.swing.adapters.persistence.models.SynchroResultModel;
import org.springframework.data.jpa.repository.JpaRepository;

public interface SynchroResultRepository extends JpaRepository<SynchroResultModel, Long> {
    SynchroResultModel save(SynchroResultModel synchroResultModel);
    SynchroResultModel findBySynchroId(Long id);
}
