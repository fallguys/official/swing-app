package org.fullguys.swing.adapters.persistence.models;

import lombok.Data;
import org.fullguys.swing.adapters.persistence.utils.Constants;

import javax.persistence.*;

@Data
@Entity
@Table(name = "ST_SYNCHRO_RESULT")
public class SynchroResultModel {

    @Id @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "synchro_result_id")
    private Long id;

    @Column(name = "synchro_id")
    private Long synchroId;

    @Column(name = "source_data_file", length = Constants.SYNCHRO_RESULT_NAME_FILE_LENGTH)
    private String sourceDataFile;

    @Column(name = "error_data_file", length = Constants.SYNCHRO_RESULT_NAME_FILE_LENGTH)
    private String errorDataFile;

    @Column(name = "total_registries")
    private Long totalRegistries;

    @Column(name = "successfull_registries")
    private Long successfullRegistries;

    @Column(name = "failed_registries", columnDefinition = "BIGINT DEFAULT 0")
    private Long failedRegistries;

    @OneToOne
    @JoinColumn(name = "synchro_id", insertable = false, updatable = false)
    private SynchroModel synchro;
}
