package org.fullguys.swing.adapters.persistence.utils;

public class Constants {
    public static final int EMAIL_LENTGH = 30;
    public static final int NAMES_LENGTH = 50;
    public static final int HASH_LENGTH = 64;

    // INCIDENT TYPE
    public static final int INCIDENT_TYPE_NAME_LENGTH = 100;
    public static final int INCIDENT_TYPE_DESCRIPTION_LENGTH = 250;
    public static final int INCIDENT_TYPE_STATE_LENGTH = 250;

    // INCIDENT
    public static final int INCIDENT_MESSAGE_LENGTH = 200;

    // ADMINISTRATOR

    // SCHEDULE

    public static final int SCHEDULE_TYPE_LENGTH = 100;
    public static final int SCHEDULE_STATE_LENGTH = 250;

    //BENEFICIARIES
    public static final int BENEFICIARY_CODE_LENGTH = 25;
    public static final int BENEFICIARY_GENDER_LENGTH = 15;
    public static final int BENEFICIARY_STATE_LENGTH = 250;

    //UBIGEE
    public static final int UBIGEE_CODE_LENGTH = 25;
    public static final int UBIGEE_NAME_LENGTH = 100;
    public static final int UBIGEE_TYPE_LENGHT = 50;

    //WITHDRAWAL POINT
    public static final int WITHDRAWAL_POINT_CODE_LENGTH = 25;
    public static final int WITHDRAWAL_POINT_NAME_LENGTH = 100;
    public static final int WITHDRAWAL_POINT_ADDRESS_LENGTH = 250;
    public static final int WITHDRAWAL_POINT_HOURS_WEEKDAY_LENGTH = 100;
    public static final int WITHDRAWAL_POINT_HOURS_WEEKEND_LENGTH = 100;
    public static final int WITHDRAWAL_POINT_STATE_LENGTH = 250;

    //SYNCHRO
    public static final int SYNCHRO_FILE_TYPE_LENGTH = 50;
    public static final int SYNCHRO_STATE_LENGTH = 250;

    //SYNCHRO RESULT

    public static final int SYNCHRO_RESULT_NAME_FILE_LENGTH = 300;

}
