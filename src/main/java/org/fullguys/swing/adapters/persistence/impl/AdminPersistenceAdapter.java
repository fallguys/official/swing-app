package org.fullguys.swing.adapters.persistence.impl;

import lombok.RequiredArgsConstructor;
import org.fullguys.swing.adapters.persistence.mappers.AdministratorMapper;
import org.fullguys.swing.adapters.persistence.repositories.AdminRepository;
import org.fullguys.swing.common.annotations.PersistenceAdapter;
import org.fullguys.swing.domain.entities.Administrator;
import org.fullguys.swing.domain.usecases.user.ports.out.persistence.GetAdminPort;

import java.util.Optional;

@PersistenceAdapter
@RequiredArgsConstructor
public class AdminPersistenceAdapter implements GetAdminPort {

    private final AdminRepository adminRepository;
    private final AdministratorMapper adminMapper;

    @Override
    public Optional<Administrator> getAdmin(String email) {
        var admin = adminRepository.findByEmail(email);
        return admin.map(adminMapper::toAdministrator);
    }
}
