package org.fullguys.swing.adapters.persistence.impl;

import lombok.RequiredArgsConstructor;
import org.fullguys.swing.adapters.persistence.mappers.BeneficiaryMapper;
import org.fullguys.swing.adapters.persistence.mappers.IncidentMapper;
import org.fullguys.swing.adapters.persistence.repositories.BeneficiaryRepository;
import org.fullguys.swing.adapters.persistence.repositories.IncidentRepository;
import org.fullguys.swing.common.annotations.PersistenceAdapter;
import org.fullguys.swing.common.queries.Filters;
import org.fullguys.swing.common.queries.Paginator;
import org.fullguys.swing.domain.entities.Incident;
import org.fullguys.swing.domain.usecases.incidents.ports.out.persistence.SaveIncidentPort;
import org.fullguys.swing.domain.usecases.incidents.ports.out.persistence.ShowIncidentForBeneficiaryPort;
import org.fullguys.swing.domain.usecases.incidents.ports.out.persistence.ShowIncidentPort;
import org.fullguys.swing.domain.usecases.scheduling.ports.out.scheduler.ShowTotalIncidentBeneficiaryPort;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;

@PersistenceAdapter
@RequiredArgsConstructor
public class IncidentPersistenceAdapter implements SaveIncidentPort, ShowIncidentPort, ShowIncidentForBeneficiaryPort, ShowTotalIncidentBeneficiaryPort {

    private final IncidentRepository incidentRepository;
    private final IncidentMapper incidentMapper;

    @Override
    public Incident save(Incident incident) {
        var row = incidentMapper.toIncidentModel(incident);
        var saved = incidentRepository.save(row);
        return incidentMapper.toIncident(saved);
    }

    @Override
    public Paginator<Incident> showIncident(Filters filters) {
        var rows = incidentRepository.findAllByIncidentTypeNameContainingOrBeneficiary_CodeContainingAndReportedDateBetween(
                filters.getSearch(), filters.getSearch(), java.util.Date.from(filters.getInitDateRange().atZone(ZoneId.systemDefault())
                .toInstant()), java.util.Date.from(filters.getEndDateRange().atZone(ZoneId.systemDefault())
                        .toInstant()),
                PageRequest.of(filters.getPage(), filters.getPagesize(), Sort.by("reportedDate").descending()));
        var saved = incidentMapper.toIncidentList(rows.getContent());
        return new Paginator<>(saved, filters.getPage(), filters.getPagesize(), rows.getTotalElements());
    }

    @Override
    public List<Incident> showIncidentTypesForBeneficiary(String beneficiaryCode) {
        var rows = incidentRepository.findAllByBeneficiary_Code(beneficiaryCode);
        var incidents = incidentMapper.toIncidentList(rows);
        return incidents;
    }

    @Override
    public Integer showTotalIncidentsForBeneficiary(String beneficiaryCode) {
        return incidentRepository.countAllByBeneficiaryCode(beneficiaryCode);
    }
}
