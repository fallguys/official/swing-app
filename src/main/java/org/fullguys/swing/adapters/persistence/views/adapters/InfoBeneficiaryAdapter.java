package org.fullguys.swing.adapters.persistence.views.adapters;

import lombok.RequiredArgsConstructor;

import java.io.OutputStream;
import java.util.List;

import net.sf.jasperreports.engine.JRException;
import org.fullguys.swing.adapters.persistence.views.models.InfoBeneficiaryView;
import org.fullguys.swing.adapters.persistence.views.repositories.InfoBeneficiaryRepository;
import org.fullguys.swing.adapters.persistence.views.specification.ShowBeneficiariesInfoSpecification;
import org.fullguys.swing.adapters.web.controllers.admin.schedules.ShowBeneficiariesInfoDTO;
import org.fullguys.swing.adapters.web.controllers.admin.schedules.ShowBeneficiariesInfoSearch;
import org.fullguys.swing.common.annotations.PersistenceAdapter;
import org.fullguys.swing.common.queries.Paginator;
import org.fullguys.swing.common.reports.ReportDownloader;
import org.fullguys.swing.common.reports.utils.Constants;
import org.fullguys.swing.common.reports.utils.MethodsUtil;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import javax.swing.table.DefaultTableModel;

@PersistenceAdapter
@RequiredArgsConstructor
public class InfoBeneficiaryAdapter {
    private final InfoBeneficiaryRepository infoBeneficiaryRepository;

    public Paginator<InfoBeneficiaryView> showInfoBeneficiaries(ShowBeneficiariesInfoSearch showBeneficiariesInfoSearch, Pageable pageable) {
        ShowBeneficiariesInfoSpecification infoBeneficiariesSpec =
                new ShowBeneficiariesInfoSpecification(showBeneficiariesInfoSearch);
        Page<InfoBeneficiaryView> rows = infoBeneficiaryRepository.findAll(infoBeneficiariesSpec, pageable);
        return new Paginator<>(rows.getContent(), pageable.getPageNumber(), pageable.getPageSize(), rows.getTotalElements());
    }

    public List<InfoBeneficiaryView> showAllInfoBeneficiaries(ShowBeneficiariesInfoSearch showBeneficiariesInfoSearch){
        ShowBeneficiariesInfoSpecification infoBeneficiariesSpec =
                new ShowBeneficiariesInfoSpecification(showBeneficiariesInfoSearch);
        List<InfoBeneficiaryView> rows = infoBeneficiaryRepository.findAll(infoBeneficiariesSpec);
        return rows;
    }

    public void exportBeneficiaries(List<ShowBeneficiariesInfoDTO> assignmentList, OutputStream outputStream, String type) throws JRException {
        String[][] data = obtenerCampos(assignmentList);
        ReportDownloader reportDownloader = new ReportDownloader();
        DefaultTableModel tableModel = new DefaultTableModel(data, new String[7]);
        try{
            if (type.equals("PDF"))
                reportDownloader.downloadPDF(outputStream, Constants.TEMPLATE_INFO_BENEFICIARY,
                        "Reporte de Beneficiarios", tableModel, data.length);
            else
                reportDownloader.downloadXLS(outputStream, Constants.TEMPLATE_INFO_BENEFICIARY,
                        "Reporte de Beneficiarios", tableModel, data.length);
        }catch(Exception e){

        }

    }

    private String[][] obtenerCampos(List<ShowBeneficiariesInfoDTO> assignmentList){
        String[][] data = new String[assignmentList.size()][7];

        for (int i = 0; i < assignmentList.size(); i++) {
            data[i][0] = assignmentList.get(i).getId();
            data[i][1] = assignmentList.get(i).getWithdrawalPoint();
            data[i][2] = MethodsUtil.formatAssignedDate(assignmentList.get(i).getInitialDate());
            data[i][3] = MethodsUtil.assignedTime(
                    MethodsUtil.formatAssignedTime(assignmentList.get(i).getInitialDate()),
                    MethodsUtil.formatAssignedTime(assignmentList.get(i).getFinalDate()));
            data[i][4] = MethodsUtil.formatBonusRetired(assignmentList.get(i).getBonusRetired());
            if (assignmentList.get(i).getBonusRetiredDate() != null)
                data[i][5] = MethodsUtil.formatAssignedDate(assignmentList.get(i).getBonusRetiredDate())+ " "+
                        MethodsUtil.formatAssignedTime(assignmentList.get(i).getBonusRetiredDate());
            else
                data[i][5] = "";
            data[i][6] = assignmentList.get(i).getMistakes().toString();
        }

        return data;
    }
}
