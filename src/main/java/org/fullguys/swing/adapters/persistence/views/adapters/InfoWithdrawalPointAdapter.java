package org.fullguys.swing.adapters.persistence.views.adapters;

import lombok.RequiredArgsConstructor;
import net.sf.jasperreports.engine.JRException;
import org.fullguys.swing.adapters.persistence.views.models.InfoWithdrawalPointView;
import org.fullguys.swing.adapters.persistence.views.repositories.InfoWithdrawalPointRepository;
import org.fullguys.swing.adapters.persistence.views.specification.ShowWithdrawalPointInfoSpecification;
import org.fullguys.swing.adapters.web.controllers.admin.schedules.ShowWithdrawalPointDTO;
import org.fullguys.swing.adapters.web.controllers.admin.schedules.ShowWithdrawalPointInfoSearch;
import org.fullguys.swing.common.annotations.PersistenceAdapter;
import org.fullguys.swing.common.queries.Paginator;
import org.fullguys.swing.common.reports.ReportDownloader;
import org.fullguys.swing.common.reports.utils.Constants;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import javax.swing.table.DefaultTableModel;
import java.io.OutputStream;
import java.util.List;

@PersistenceAdapter
@RequiredArgsConstructor
public class InfoWithdrawalPointAdapter {
    private final InfoWithdrawalPointRepository infoWithdrawalPointRepository;
    public Paginator<InfoWithdrawalPointView> showInfoBeneficiaries(ShowWithdrawalPointInfoSearch showWithdrawalPointInfoSearch, Pageable pageable) {
        ShowWithdrawalPointInfoSpecification infoWithdrawalPointSpec =
                new ShowWithdrawalPointInfoSpecification(showWithdrawalPointInfoSearch);
        Page<InfoWithdrawalPointView> rows = infoWithdrawalPointRepository.findAll(infoWithdrawalPointSpec, pageable);
        return new Paginator<>(rows.getContent(), pageable.getPageNumber(), pageable.getPageSize(), rows.getTotalElements());
    }

    public List<InfoWithdrawalPointView> showAllInfoBeneficiaries(ShowWithdrawalPointInfoSearch showBeneficiariesInfoSearch){
        ShowWithdrawalPointInfoSpecification showWithdrawalPointInfoSpecification =
                new ShowWithdrawalPointInfoSpecification(showBeneficiariesInfoSearch);
        List<InfoWithdrawalPointView> rows = infoWithdrawalPointRepository.findAll(showWithdrawalPointInfoSpecification);
        return rows;
    }

    public void exportWithdrawalPoints(List<ShowWithdrawalPointDTO> assignmentList, OutputStream outputStream, String type) throws JRException {
        String[][] data = obtenerCampos(assignmentList);
        ReportDownloader reportDownloader = new ReportDownloader();
        DefaultTableModel tableModel = new DefaultTableModel(data, new String[5]);
        try{
            if (type.equals("PDF"))
                reportDownloader.downloadPDF(outputStream, Constants.TEMPLATE_INFO_WITHDRAWAL_POINT,
                        "Reporte de Puntos de Retiro", tableModel, data.length);
            else
                reportDownloader.downloadXLS(outputStream, Constants.TEMPLATE_INFO_WITHDRAWAL_POINT,
                        "Reporte de Puntos de Retiro", tableModel, data.length);
        }catch(Exception e){

        }

    }

    private String[][] obtenerCampos(List<ShowWithdrawalPointDTO> assignmentList){
        String[][] data = new String[assignmentList.size()][5];

        for (int i = 0; i < assignmentList.size(); i++) {
            data[i][0] = assignmentList.get(i).getId();
            data[i][1] = assignmentList.get(i).getAddress();
            data[i][2] = assignmentList.get(i).getUbigee();
            data[i][3] = assignmentList.get(i).getCantAssigned().toString();
            data[i][4] = assignmentList.get(i).getCantAttended().toString();
        }

        return data;
    }
}
