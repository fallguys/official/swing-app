package org.fullguys.swing.adapters.persistence.impl;

import lombok.RequiredArgsConstructor;
import org.fullguys.swing.adapters.persistence.mappers.ContagionAreaMapper;
import org.fullguys.swing.adapters.persistence.repositories.ContagionAreaRepository;
import org.fullguys.swing.common.annotations.PersistenceAdapter;
import org.fullguys.swing.common.queries.Filters;
import org.fullguys.swing.common.queries.Paginator;
import org.fullguys.swing.domain.entities.ContagionArea;
import org.fullguys.swing.domain.usecases.synchro.ports.out.persistence.CountContagionAreasPort;
import org.fullguys.swing.domain.usecases.synchro.ports.out.persistence.ListContagionAreaPort;
import org.springframework.data.domain.PageRequest;

import java.io.FileOutputStream;
import java.util.List;

@PersistenceAdapter
@RequiredArgsConstructor
public class ContagionAreaPersistenceAdapter implements ListContagionAreaPort, CountContagionAreasPort {

    private final ContagionAreaRepository contagionAreaRepository;
    private final ContagionAreaMapper contagionAreaMapper;

    @Override
    public Paginator<ContagionArea> listContagionAreas(Filters filters) {
        var contagionAreasRows = contagionAreaRepository.findAll(PageRequest.of(filters.getPage(), filters.getPagesize()));
        var contagionAreas = contagionAreaMapper.toContagionAreas(contagionAreasRows.getContent());
        return new Paginator<>(contagionAreas, filters.getPage(), filters.getPagesize(), contagionAreasRows.getTotalElements());
    }

    @Override
    public long count() {
        return contagionAreaRepository.count();
    }
}
