package org.fullguys.swing.adapters.persistence.repositories;

import org.fullguys.swing.adapters.persistence.models.IncidentModel;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Date;
import java.util.List;

public interface IncidentRepository extends JpaRepository<IncidentModel, Long> {
    Page<IncidentModel> findAllByIncidentTypeNameContainingOrBeneficiary_CodeContainingAndReportedDateBetween(
            String search, String search2, Date startDate, Date endDate,
            Pageable pageable);
    List<IncidentModel> findAllByBeneficiary_Code(String code);
    Integer countAllByBeneficiaryCode(String code);
}
