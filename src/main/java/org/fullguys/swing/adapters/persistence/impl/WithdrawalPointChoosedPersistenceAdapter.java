package org.fullguys.swing.adapters.persistence.impl;

import lombok.RequiredArgsConstructor;
import org.fullguys.swing.adapters.persistence.mappers.WithdrawalPointChoosedMapper;
import org.fullguys.swing.adapters.persistence.mappers.WithdrawalPointMapper;
import org.fullguys.swing.adapters.persistence.models.WithdrawalPointChoosedModel;
import org.fullguys.swing.adapters.persistence.models.WithdrawalPointModel;
import org.fullguys.swing.adapters.persistence.repositories.BeneficiaryRepository;
import org.fullguys.swing.adapters.persistence.repositories.WithdrawalPointChoosedRepository;
import org.fullguys.swing.adapters.persistence.repositories.WithdrawalPointRepository;
import org.fullguys.swing.common.annotations.PersistenceAdapter;
import org.fullguys.swing.domain.entities.WithdrawalPoint;
import org.fullguys.swing.domain.usecases.beneficiaries.ports.out.persistence.SelectWithdrawalPointsPort;

import java.util.ArrayList;
import java.util.List;

@PersistenceAdapter
@RequiredArgsConstructor
public class WithdrawalPointChoosedPersistenceAdapter implements SelectWithdrawalPointsPort {

    private final BeneficiaryRepository beneficiaryRepository;
    private final WithdrawalPointMapper withdrawalPointMapper;
    private final WithdrawalPointChoosedRepository withdrawalPointChoosedRepository;
    private final WithdrawalPointRepository withdrawalPointRepository;
    private final WithdrawalPointChoosedMapper withdrawalPointChoosedMapper;
    @Override
    public List<WithdrawalPoint> selectWithdrawalPoints(List<String> codes, String beneficiaryCode) {
        withdrawalPointChoosedRepository.deleteByBeneficiaryCode(beneficiaryCode);
        var beneficiary = beneficiaryRepository.findByCodeEquals(beneficiaryCode).orElseThrow();
        var withdrawalPoints = withdrawalPointRepository.findAllByCodeEqualsOrCodeEqualsOrCodeEquals(codes.get(0), codes.get(1), codes.get(2));
        List<WithdrawalPointChoosedModel> withdrawalPointChoosedModels = new ArrayList<>();
        for (WithdrawalPointModel withdrawalPointChoosedModel: withdrawalPoints) {
            var withdrawalPointsChoosed = withdrawalPointChoosedMapper.toWithdrawalPointChoosedModel(withdrawalPointChoosedModel, beneficiary);
            withdrawalPointChoosedModels.add(withdrawalPointsChoosed);
        }
        withdrawalPointChoosedRepository.saveAll(withdrawalPointChoosedModels);
        return withdrawalPointMapper.toWithdrawalPoints(withdrawalPoints);
    }
}
