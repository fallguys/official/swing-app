CREATE OR REPLACE VIEW VW_INFO_BENEFICIARY AS
SELECT ctb.participant_beneficiary_id AS info_beneficiary_id, mtb.code as beneficiary_code, mwp.code AS withdrawal_point_code, mwp.name AS withdrawal_point_name, mu.code AS ubigee, cib.initial_date_assigned, cib.bonus_retired,
       cib.bonus_retired_date, ms.schedule_id, count(c.feedback_id) AS mistakes
FROM ct_info_bonus cib
         JOIN ct_participant_benefeciary ctb
              ON cib.participant_beneficiary_id = ctb.participant_beneficiary_id
         JOIN mt_beneficiary mtb
              ON ctb.beneficiary_id = mtb.beneficiary_id
         JOIN ct_participant_point cpp on cib.participant_point_id = cpp.participant_point_id
         JOIN mt_withdrawal_point mwp on cpp.withdrawal_point_id = mwp.withdrawal_point_id
         JOIN mt_schedule ms on ms.schedule_id = ctb.schedule_id
         JOIN mt_ubigee mu on mu.ubigee_id = mwp.district_id
         LEFT JOIN ct_feedback c
         ON ctb.participant_beneficiary_id = c.beneficiary_id
        GROUP BY ctb.participant_beneficiary_id,
        cib.initial_date_assigned, mwp.code, mu.code,
        mwp.name, mtb.code, cib.bonus_retired, cib.bonus_retired_date, ms.schedule_id;



