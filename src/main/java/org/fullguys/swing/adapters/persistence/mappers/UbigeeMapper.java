package org.fullguys.swing.adapters.persistence.mappers;

import org.fullguys.swing.adapters.persistence.models.BeneficiaryModel;
import org.fullguys.swing.adapters.persistence.models.UbigeeModel;
import org.fullguys.swing.domain.entities.Beneficiary;
import org.fullguys.swing.domain.entities.Ubigee;
import org.mapstruct.*;

import java.util.List;

@Mapper(componentModel="spring")
public interface UbigeeMapper {

    @Mappings({
            @Mapping(source = "id", target = "id"),
            @Mapping(source = "fatherUbigee", target = "fatherUbigee"),
            @Mapping(source = "code", target = "code"),
            @Mapping(source = "name", target = "name"),
            @Mapping(source = "type", target = "type")
    })
    Ubigee toUbigee(UbigeeModel ubigeeModel);
    List<Ubigee> toUbigeeList(List<UbigeeModel> ubigeeModelList);

    @InheritInverseConfiguration
    UbigeeModel toUbigeeModel(Ubigee ubigee);

}
