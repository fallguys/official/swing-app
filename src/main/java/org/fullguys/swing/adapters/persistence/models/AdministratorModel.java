package org.fullguys.swing.adapters.persistence.models;

import lombok.Data;
import org.fullguys.swing.adapters.persistence.utils.Constants;

import javax.persistence.*;

@Data @Entity
@Table(name = "MT_ADMINISTRATOR")
public class AdministratorModel {
    @Id @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "administrator_id")
    private Long id;

    @Column(name = "names", length = Constants.NAMES_LENGTH)
    private String names;

    @Column(name = "father_lastname", length = Constants.NAMES_LENGTH)
    private String fatherLastname;

    @Column(name = "mother_lastname", length = Constants.NAMES_LENGTH)
    private String motherLastname;

    @Column(name = "email", length = Constants.EMAIL_LENTGH)
    private String email;

    @Column(name = "password", length = Constants.HASH_LENGTH)
    private String password;
}
