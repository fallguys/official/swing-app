package org.fullguys.swing.adapters.persistence.mappers;

import org.fullguys.swing.adapters.persistence.models.ParticipantPointModel;
import org.fullguys.swing.domain.entities.Assignment;
import org.fullguys.swing.domain.entities.WithdrawalPoint;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;

import java.util.List;

@Mapper(componentModel = "spring", uses = {WithdrawalPointMapper.class})
public interface ParticipantPointMapper {
    @Mappings({
            @Mapping(source = "withdrawalPoint", target = "withdrawalPoint"),
            @Mapping(target = "beneficiary", ignore = true),
            @Mapping(target = "infoBonus", ignore = true)
    })
    Assignment toAssigment(ParticipantPointModel participantPointModel);
    List<Assignment> toAssignmentList(List<ParticipantPointModel> participantPointModel);
    @Mappings({
            @Mapping(source = "withdrawalPoint.id", target = "id"),
            @Mapping(source = "withdrawalPoint.code", target = "code"),
            @Mapping(source = "withdrawalPoint.name", target = "name"),
            @Mapping(source = "withdrawalPoint.address", target= "address"),
            @Mapping(target = "weekdayHourStart", ignore = true),
            @Mapping(target = "weekdayHourEnd", ignore = true),
            @Mapping(target = "weekendHourStart", ignore = true),
            @Mapping(target = "weekendHourEnd", ignore = true),
            @Mapping(target = "state", ignore = true),
            @Mapping(target = "createdAt", ignore = true),
            @Mapping(target = "updatedAt", ignore = true),
            @Mapping(source = "withdrawalPoint.district", target = "district")
    })
    WithdrawalPoint toWithdrawalPoint(ParticipantPointModel participantPointModel);
    List<WithdrawalPoint> toWithdrawalPoints(List<ParticipantPointModel> participantPointModel);

}
