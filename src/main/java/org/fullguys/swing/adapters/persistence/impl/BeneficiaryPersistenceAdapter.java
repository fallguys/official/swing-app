package org.fullguys.swing.adapters.persistence.impl;

import lombok.RequiredArgsConstructor;
import org.fullguys.swing.adapters.persistence.mappers.BeneficiaryMapper;
import org.fullguys.swing.adapters.persistence.mappers.WithdrawalPointChoosedMapper;
import org.fullguys.swing.adapters.persistence.mappers.WithdrawalPointMapper;
import org.fullguys.swing.adapters.persistence.models.BeneficiaryModel;
import org.fullguys.swing.adapters.persistence.models.WithdrawalPointChoosedModel;
import org.fullguys.swing.adapters.persistence.repositories.BeneficiaryRepository;
import org.fullguys.swing.common.annotations.PersistenceAdapter;
import org.fullguys.swing.common.queries.Filters;
import org.fullguys.swing.common.queries.Paginator;
import org.fullguys.swing.domain.entities.Beneficiary;
import org.fullguys.swing.domain.entities.enums.BeneficiaryState;
import org.fullguys.swing.domain.usecases.feedback.ports.out.persistence.UpdateBeneficiaryPort;
import org.fullguys.swing.domain.usecases.synchro.ports.out.persistence.CountBeneficiariesByStatusPort;
import org.fullguys.swing.domain.usecases.synchro.ports.out.persistence.ListBeneficiariesPort;
import org.springframework.data.domain.PageRequest;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@PersistenceAdapter
@RequiredArgsConstructor
public class BeneficiaryPersistenceAdapter implements ListBeneficiariesPort, CountBeneficiariesByStatusPort,
        UpdateBeneficiaryPort {

    private final BeneficiaryRepository beneficiaryRepository;
    private final BeneficiaryMapper beneficiaryMapper;


    @Override
    public Paginator<Beneficiary> listBeneficiaries(Filters filters) {
        var beneficiariesRows = beneficiaryRepository.findAll(PageRequest.of(filters.getPage(), filters.getPagesize()));
        var beneficiaries = beneficiaryMapper.toBeneficiaries(beneficiariesRows.getContent());
        return new Paginator<>(beneficiaries, filters.getPage(), filters.getPagesize(), beneficiariesRows.getTotalElements());
    }

    public void ListBeneficiaryValidate(List<Beneficiary> beneficiaries, List<Beneficiary> beneficiariesError,
                                        List<Beneficiary> beneficiariesPotential){
        for (Beneficiary beneficiary:beneficiaries) {
            if(beneficiary.getCode().isEmpty())
                beneficiariesError.add(beneficiary);
            else if(beneficiary.getDistrict().getCode().isEmpty())
                beneficiariesError.add(beneficiary);
            else if(beneficiary.getIsDisabled() == null)
                beneficiariesError.add(beneficiary);
            else if(beneficiary.getIsOlderAdult() == null)
                beneficiariesError.add(beneficiary);
            else
                beneficiariesPotential.add(beneficiary);
        }
    }

    public List<Beneficiary> ListBeneficiaryToSave(List<Beneficiary> beneficiariesPotential,
                                                   List<Beneficiary> beneficiariesToUpdate){
        List<Beneficiary> beneficiariesSaved = new ArrayList<>();
        boolean flag;
        for(int i=0; i<beneficiariesPotential.size(); i++){
            flag = true;
            for(int j=0; j<beneficiariesToUpdate.size(); j++){
                if(beneficiariesPotential.get(i).equals(beneficiariesToUpdate.get(j))){
                    flag = false;
                    break;
                }
            }
            if (flag == true)
                beneficiariesSaved.add(beneficiariesPotential.get(i));
        }

        return beneficiariesSaved;
    }

    @Override
    public long countByState(BeneficiaryState beneficiaryState) {
        return beneficiaryRepository.countAllByState(beneficiaryState.name());
    }

    @Override
    public void updateHitBeneficiary(Long idBeneficiary) {
        var row = beneficiaryRepository.findById(idBeneficiary);
        row.ifPresent(r -> {
        r.setHits(r.getHits() + 1L);
        beneficiaryRepository.save(r);
        });
    }

    @Override
    public void updateInfractionBeneficiary(Long idBeneficiary) {
        var row = beneficiaryRepository.findById(idBeneficiary);
        row.ifPresent(r -> {
            r.setInfractions(r.getInfractions() + 1L);
            beneficiaryRepository.save(r);
        });
    }
}
