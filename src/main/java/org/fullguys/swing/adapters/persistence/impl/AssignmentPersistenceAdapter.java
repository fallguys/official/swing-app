package org.fullguys.swing.adapters.persistence.impl;

import lombok.RequiredArgsConstructor;
import org.fullguys.swing.adapters.persistence.mappers.AssignmentMapper;
import org.fullguys.swing.adapters.persistence.repositories.InfoBonusRepository;
import org.fullguys.swing.common.annotations.PersistenceAdapter;
import org.fullguys.swing.common.queries.Filters;
import org.fullguys.swing.common.queries.Paginator;
import org.fullguys.swing.domain.entities.Assignment;
import org.fullguys.swing.domain.usecases.feedback.ports.out.persistence.GetAssignmentPort;
import org.fullguys.swing.domain.usecases.scheduling.ports.out.scheduler.ShowBeneficiariesInfoPort;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;


@PersistenceAdapter
@RequiredArgsConstructor
public class AssignmentPersistenceAdapter implements GetAssignmentPort, ShowBeneficiariesInfoPort {

    private final InfoBonusRepository infoBonusRepository;
    private final AssignmentMapper assignmentMapper;

    @Override
    public Assignment getAssignment(Long idParticipantBeneficiary) {
        var row = infoBonusRepository.findByParticipantBeneficiary_Id(idParticipantBeneficiary);
        return assignmentMapper.toAssigment(row);
    }

    @Override
    public Paginator<Assignment> showBeneficiariesInfo(Filters filters) {
        var rows = infoBonusRepository.
                findByParticipantBeneficiaryScheduleIdAndParticipantPointScheduleId(
                        (long) Integer.parseInt(filters.getSearch()),
                        (long) Integer.parseInt(filters.getSearch()),
                        PageRequest.of(filters.getPage(),filters.getPagesize(), Sort.by("bonusRetired").descending()));
        var assignments = assignmentMapper.toAssignmentList(rows.getContent());
        return new Paginator<>(assignments, filters.getPage(), filters.getPagesize(), rows.getTotalElements());
    }
}
