package org.fullguys.swing.adapters.persistence.models;

import lombok.Data;

import javax.persistence.*;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Date;

@Data
@Entity
@Table(name = "CT_INFO_BONUS")
public class InfoBonusModel {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "info_bonus_id")
    private Long id;

    @Column(name = "participant_beneficiary_id")
    private Long beneficiaryId;

    @Column(name = "participant_point_id")
    private Long withdrawalPointId;

    @Column(name = "initial_date_assigned")
    private LocalDateTime initialDateAssigned;

    @Column(name = "final_date_assigned")
    private LocalDateTime finalDateAssigned;

    @Column(name = "bonus_retired")
    private Boolean bonusRetired;

    @Column(name = "bonus_retired_date")
    private LocalDateTime bonusRetiredDate;

    @OneToOne
    @JoinColumn(name = "participant_beneficiary_id", insertable = false, updatable = false)
    private ParticipantBeneficiaryModel participantBeneficiary;

    @ManyToOne
    @JoinColumn(name = "participant_point_id", insertable = false, updatable = false)
    private ParticipantPointModel participantPoint;

}
