package org.fullguys.swing.adapters.persistence.mappers;

import org.fullguys.swing.adapters.persistence.models.FeedbackModel;
import org.fullguys.swing.domain.entities.Feedback;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel="spring")
public interface FeedbackMapper {

    @Mapping(source = "reportedErrorDate", target = "reportedErrorDate")

    Feedback toFeedback(FeedbackModel feedbackModel);

}
