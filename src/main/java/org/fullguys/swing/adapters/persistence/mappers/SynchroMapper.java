package org.fullguys.swing.adapters.persistence.mappers;

import org.fullguys.swing.adapters.persistence.models.SynchroModel;
import org.fullguys.swing.adapters.persistence.models.SynchroResultModel;
import org.fullguys.swing.domain.entities.Synchro;
import org.mapstruct.InheritInverseConfiguration;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;

import java.util.List;

@Mapper(componentModel="spring")
public interface SynchroMapper {

    @Mappings({
            @Mapping(source = "id", target = "id"),
            @Mapping(source = "fileType", target = "fileType"),
            @Mapping(source = "initialDate", target = "initialDate"),
            @Mapping(source = "administrator", target = "administrator")
    })
    @Mapping(target = "result", ignore = true)
    Synchro toSynchro(SynchroModel synchroModel);
    List<Synchro> toSynchros(List<SynchroModel> synchroModels);

    @Mappings({
            @Mapping(source = "result.sourceDataFile", target = "sourceDataFile"),
            @Mapping(source = "id", target = "synchroId"),
            @Mapping(target = "errorDataFile", ignore = true),
            @Mapping(target = "failedRegistries", ignore = true),
            @Mapping(target = "successfullRegistries", ignore = true),
            @Mapping(target = "totalRegistries", ignore = true)
    })
    SynchroResultModel toSynchroResultModel(Synchro synchro);

    @Mappings({
            @Mapping(source = "synchro.administrator", target = "administrator"),
            @Mapping(source = "synchro.fileType", target = "fileType"),
            @Mapping(source = "synchro.initialDate", target = "initialDate"),
            @Mapping(source = "synchro.id", target = "id"),
            @Mapping(source = "synchro.state", target = "state"),
            @Mapping(source = "successfullRegistries", target = "result.successfullRegistries"),
            @Mapping(source = "totalRegistries", target = "result.totalRegistries"),
            @Mapping(source = "failedRegistries", target = "result.failedRegistries"),
            @Mapping(target = "result.errorDataFile", ignore = true),
            @Mapping(target = "result.sourceDataFile", ignore = true),
    })
    Synchro toSynchroWithResult(SynchroResultModel synchroResultModel);
    List<Synchro> toSynchrosWithResult(List<SynchroResultModel> synchroResultModels);

    @InheritInverseConfiguration
    SynchroModel toSynchroModel(Synchro synchro);
    List<SynchroModel> toSynchroModels(List<Synchro> synchros);

}
