package org.fullguys.swing.adapters.persistence.impl;

import lombok.RequiredArgsConstructor;
import org.fullguys.swing.adapters.persistence.mappers.IncidentTypeMapper;
import org.fullguys.swing.adapters.persistence.models.IncidentTypeModel;
import org.fullguys.swing.adapters.persistence.repositories.IncidentTypeRepository;
import org.fullguys.swing.common.annotations.PersistenceAdapter;
import org.fullguys.swing.common.queries.Filters;
import org.fullguys.swing.common.queries.Paginator;
import org.fullguys.swing.domain.entities.IncidentType;
import org.fullguys.swing.domain.entities.enums.IncidentTypeState;
import org.fullguys.swing.domain.usecases.incidents.ports.out.persistence.SaveIncidentTypePort;
import org.fullguys.swing.domain.usecases.incidents.ports.out.persistence.ShowIncidentTypesPortForAdmin;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;

import java.util.List;
import java.util.Map;

@PersistenceAdapter
@RequiredArgsConstructor
public class IncidentTypePersistenceAdapter implements SaveIncidentTypePort, ShowIncidentTypesPortForAdmin {

    private final IncidentTypeRepository incidentTypeRepository;
    private final IncidentTypeMapper incidentTypeMapper;

    @Override
    public IncidentType save(IncidentType incidentType) {
        var row = incidentTypeMapper.toIncidentTypeModel(incidentType);
        var saved = incidentTypeRepository.save(row);
        return incidentTypeMapper.toIncidentType(saved);
    }

    @Override
    public Paginator<IncidentType> showIncidentTypes(Filters filters) {
        var actualPage = incidentTypeRepository.findAllByNameContaining(filters.getSearch(),
                PageRequest.of(filters.getPage(), filters.getPagesize(), Sort.by("name")));
        var saved = incidentTypeMapper.toIncidentTypes(actualPage.getContent());
        return new Paginator<>(saved, filters.getPage(), filters.getPagesize(),
                actualPage.getTotalElements());
    }
}

