package org.fullguys.swing.adapters.persistence.impl;

import lombok.RequiredArgsConstructor;
import org.fullguys.swing.adapters.persistence.mappers.BeneficiaryMapper;
import org.fullguys.swing.adapters.persistence.mappers.InfoBonusMapper;
import org.fullguys.swing.adapters.persistence.mappers.WithdrawalPointMapper;
import org.fullguys.swing.adapters.persistence.models.BeneficiaryModel;
import org.fullguys.swing.adapters.persistence.repositories.BeneficiaryRepository;
import org.fullguys.swing.adapters.persistence.repositories.InfoBonusRepository;
import org.fullguys.swing.adapters.persistence.repositories.WithdrawalPointRepository;
import org.fullguys.swing.common.annotations.PersistenceAdapter;
import org.fullguys.swing.config.parameter.TimerParameters;
import org.fullguys.swing.domain.entities.Assignment;
import org.fullguys.swing.domain.entities.InfoBonus;
import org.fullguys.swing.domain.entities.enums.ScheduleState;
import org.fullguys.swing.domain.entities.enums.ScheduleType;
import org.fullguys.swing.domain.usecases.beneficiaries.ports.out.persistence.ShowInfoBonusPort;
import org.fullguys.swing.domain.usecases.feedback.ports.out.persistence.SaveInfoBonusPort;
import org.fullguys.swing.domain.usecases.scheduling.ports.out.scheduler.ShowTotalAssignedPort;

import java.time.LocalDateTime;

import java.time.ZoneId;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@PersistenceAdapter
@RequiredArgsConstructor
public class InfoBonusPersistenceAdapter implements ShowInfoBonusPort, SaveInfoBonusPort, ShowTotalAssignedPort {
    private final InfoBonusRepository infoBonusRepository;
    private final InfoBonusMapper infoBonusMapper;
    private final BeneficiaryRepository beneficiaryRepository;
    private final BeneficiaryMapper beneficiaryMapper;
    private final WithdrawalPointRepository withdrawalPointRepository;
    private final WithdrawalPointMapper withdrawalPointMapper;
    private final TimerParameters timerParameters;

    @Override
    public Assignment showInfoBonus(String search) {
        var infoBonusModel = infoBonusRepository.findByParticipantBeneficiary_BeneficiaryCodeAndParticipantBeneficiaryScheduleState(search, ScheduleState.ACTIVE.toString());
        var beneficiaryModel = beneficiaryRepository.findById(infoBonusModel.orElseThrow().getParticipantBeneficiary().getBeneficiaryId());
        var withdrawalPointModel = withdrawalPointRepository.findById(infoBonusModel.orElseThrow().getParticipantPoint().getWithdrawalPointId());

        Assignment assignment = new Assignment();

        assignment.setBeneficiary(beneficiaryModel.map(beneficiaryMapper::toBeneficiary).orElseThrow());
        assignment.setInfoBonus(infoBonusMapper.toInfoBonus(infoBonusModel.orElseThrow()));
        assignment.setWithdrawalPoint(withdrawalPointModel.map(withdrawalPointMapper::toWithdrawalPoint).orElseThrow());
        return assignment;
    }

    @Override
    public InfoBonus saveInfoBonus(Long idParticipantBeneficiary) {
        var row = infoBonusRepository.findByParticipantBeneficiary_Id(idParticipantBeneficiary);
        row.setBonusRetired(true);
        row.setBonusRetiredDate(LocalDateTime.now(ZoneId.of(timerParameters.getZone())));
        var rowSaved = infoBonusRepository.save(row);
        return infoBonusMapper.toInfoBonus(rowSaved);
    }

    @Override
    public List<Integer> showTotalAssignedAndAttendedForWithdrawalPointForSchedule(String withdrawalPointCode) {
        var aux = infoBonusRepository.countAllByParticipantPointWithdrawalPointCode(withdrawalPointCode);
        var aux2 = infoBonusRepository.countAllByParticipantPointWithdrawalPointCodeAndBonusRetired(withdrawalPointCode, true);
        List<Integer> cant = new ArrayList<>(2);
        cant.add(aux);
        cant.add(aux2);
        return cant;
    }
}
