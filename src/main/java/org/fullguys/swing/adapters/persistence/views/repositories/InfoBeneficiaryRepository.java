package org.fullguys.swing.adapters.persistence.views.repositories;

import org.fullguys.swing.adapters.persistence.views.models.InfoBeneficiaryView;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;


public interface InfoBeneficiaryRepository extends JpaRepository<InfoBeneficiaryView, Long>, JpaSpecificationExecutor<InfoBeneficiaryView> {

}
