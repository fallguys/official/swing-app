package org.fullguys.swing.adapters.persistence.models;

import lombok.Data;
import org.fullguys.swing.adapters.persistence.utils.Constants;

import javax.persistence.*;
import java.util.Date;

@Data @Entity
@Table(name = "MT_UBIGEE")
public class UbigeeModel {

    @Id @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ubigee_id")
    private Long id;

    @Column(name = "father_ubigee_id")
    private Long fatherUbigeeId;

    @Column(name = "code", length = Constants.UBIGEE_CODE_LENGTH)
    private String code;

    @Column(name = "name", length = Constants.UBIGEE_NAME_LENGTH)
    private String name;

    @Column(name = "type", length = Constants.UBIGEE_TYPE_LENGHT)
    private String type;

    @ManyToOne
    @JoinColumn(name = "father_ubigee_id", insertable = false, updatable = false)
    private UbigeeModel fatherUbigee;

}
