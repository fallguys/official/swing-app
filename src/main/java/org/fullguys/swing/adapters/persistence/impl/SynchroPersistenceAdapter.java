package org.fullguys.swing.adapters.persistence.impl;

import lombok.RequiredArgsConstructor;
import org.fullguys.swing.adapters.persistence.mappers.SynchroMapper;
import org.fullguys.swing.adapters.persistence.mappers.SynchroResultMapper;
import org.fullguys.swing.adapters.persistence.repositories.SynchroRepository;
import org.fullguys.swing.adapters.persistence.repositories.SynchroResultRepository;
import org.fullguys.swing.common.annotations.PersistenceAdapter;
import org.fullguys.swing.common.queries.Filters;
import org.fullguys.swing.common.queries.Paginator;
import org.fullguys.swing.domain.entities.Synchro;
import org.fullguys.swing.domain.entities.enums.SynchroFileType;
import org.fullguys.swing.domain.entities.enums.SynchroState;
import org.fullguys.swing.domain.usecases.scheduling.ports.out.persistence.CheckForSynchrosProcessingPort;
import org.fullguys.swing.domain.usecases.synchro.ports.out.persistence.GetLastSynchroPort;
import org.fullguys.swing.domain.usecases.synchro.ports.out.persistence.GetSynchroPort;
import org.fullguys.swing.domain.usecases.synchro.ports.out.persistence.RegisterSynchronizationPort;
import org.fullguys.swing.domain.usecases.synchro.ports.out.synchronizer.ShowSynchroDetailPort;
import org.springframework.data.domain.PageRequest;

import java.util.Optional;

@PersistenceAdapter
@RequiredArgsConstructor
public class SynchroPersistenceAdapter implements RegisterSynchronizationPort, CheckForSynchrosProcessingPort,
                                                    GetSynchroPort, ShowSynchroDetailPort, GetLastSynchroPort {

    private final SynchroRepository synchroRepository;
    private final SynchroResultRepository synchroResultRepository;
    private final SynchroMapper synchroMapper;
    private final SynchroResultMapper synchroResultMapper;

    @Override
    public Synchro save(Synchro synchro) {
        var row=synchroMapper.toSynchroModel(synchro);
        //Se pone por defecto el administrador actual, hasta q haya un login
        row.setAdministratorId((long) 1);
        var saved = synchroRepository.save(row);
        return synchroMapper.toSynchro(saved);
    }

    @Override
    public void saveResult(Synchro synchro, String urlFile) {
        var row = synchroMapper.toSynchroResultModel(synchro);
        row.setSourceDataFile(urlFile);
        var rowSaved = synchroResultRepository.save(row);
    }

    @Override
    public boolean checkForSynchrosProcessing() {
        return synchroRepository.existsByState(SynchroState.IN_PROCESS.name());
    }

    @Override
    public Optional<Synchro> getLastActiveSynchro(SynchroFileType synchroFileType, SynchroState synchroState) {
        var row = synchroRepository.findFirstByFileTypeAndStateOrderByInitialDateDesc(synchroFileType.toString(), synchroState.toString());
        return row.map(synchroMapper::toSynchro);
    }

    @Override
    public Paginator<Synchro> getSynchros(Filters filters) {
        var rows = synchroResultRepository.findAll(PageRequest.of(filters.getPage(), filters.getPagesize()));
        var synchros = synchroMapper.toSynchrosWithResult(rows.getContent());
        return new Paginator<>(synchros, filters.getPage(), filters.getPagesize(), rows.getTotalElements());
    }

    @Override
    public Synchro showSynchroDetail(Long id) {
        var synchroModel = synchroRepository.findById(id);
        var synchroResultModel = synchroResultRepository.findBySynchroId(id);
        var synchro = synchroMapper.toSynchro(synchroModel.orElseThrow());
        var synchroResult = synchroResultMapper.toSynchroResult(synchroResultModel);
        synchro.setResult(synchroResult);
        return synchro;
    }

    @Override
    public Optional<Synchro> getLastSynchro(SynchroFileType synchroFileType) {
        var synchroModel = synchroRepository.findFirstByFileTypeOrderByInitialDateDesc(synchroFileType.toString());
        return synchroModel.map(synchroMapper::toSynchro);
    }
}
