package org.fullguys.swing.adapters.persistence.mappers;

import org.fullguys.swing.adapters.persistence.models.BeneficiaryModel;
import org.fullguys.swing.adapters.persistence.models.WithdrawalPointModel;
import org.fullguys.swing.domain.entities.Beneficiary;
import org.fullguys.swing.domain.entities.WithdrawalPoint;
import org.mapstruct.InheritInverseConfiguration;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;

import java.util.List;

@Mapper(componentModel="spring")
public interface WithdrawalPointMapper {
    @Mappings({
            @Mapping(source = "id", target = "id"),
            @Mapping(source = "code", target = "code"),
            @Mapping(source = "name", target = "name"),
            @Mapping(source = "district", target = "district"),
            @Mapping(source = "address", target = "address"),
            @Mapping(source = "weekdayHourStart", target = "weekdayHourStart"),
            @Mapping(source = "weekdayHourEnd", target = "weekdayHourEnd"),
            @Mapping(source = "weekendHourStart", target = "weekendHourStart"),
            @Mapping(source = "weekendHourEnd", target = "weekendHourEnd"),
            @Mapping(source = "createdAt", target = "createdAt"),
            @Mapping(source = "updatedAt", target = "updatedAt")
    })
    WithdrawalPoint toWithdrawalPoint(WithdrawalPointModel withdrawalPointModel);

    @InheritInverseConfiguration
    @Mapping(source = "district.id", target = "districtId")
    @Mapping(target = "updatedAt", ignore = true)
    @Mapping(target = "infractions", ignore = true)
    WithdrawalPointModel toWithdrawalPointModel(WithdrawalPoint withdrawalPoint);
    List<WithdrawalPointModel> toWithdrawalPointModels(List<WithdrawalPoint> withdrawalPoints);

    List<WithdrawalPoint> toWithdrawalPoints(List<WithdrawalPointModel> withdrawalPointModels);

}
