package org.fullguys.swing.adapters.persistence.models;

import lombok.Data;

import javax.persistence.*;

@Data
@Entity
@Table(name = "CT_WITHDRAWAL_POINT_CHOOSED")
public class WithdrawalPointChoosedModel {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "withdrawal_point_choosed_id")
    private Long id;

    @Column(name = "beneficiary_id")
    private Long beneficiaryId;

    @Column(name = "withdrawal_point_id")
    private Long withdrawalPointId;

    @ManyToOne
    @JoinColumn(name = "beneficiary_id", insertable = false, updatable = false)
    private BeneficiaryModel beneficiary;

    @ManyToOne
    @JoinColumn(name = "withdrawal_point_id", insertable = false, updatable = false)
    private WithdrawalPointModel withdrawalPoint;

}
