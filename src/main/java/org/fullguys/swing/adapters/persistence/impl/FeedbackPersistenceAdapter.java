package org.fullguys.swing.adapters.persistence.impl;

import lombok.RequiredArgsConstructor;
import org.fullguys.swing.adapters.persistence.mappers.FeedbackMapper;
import org.fullguys.swing.adapters.persistence.models.FeedbackModel;
import org.fullguys.swing.adapters.persistence.repositories.FeedbackRepository;
import org.fullguys.swing.common.annotations.PersistenceAdapter;
import org.fullguys.swing.config.parameter.TimerParameters;
import org.fullguys.swing.domain.entities.Feedback;
import org.fullguys.swing.domain.entities.Synchro;
import org.fullguys.swing.domain.entities.enums.SynchroFileType;
import org.fullguys.swing.domain.entities.enums.SynchroState;
import org.fullguys.swing.domain.usecases.feedback.commands.CheckBonusCommand;
import org.fullguys.swing.domain.usecases.feedback.ports.out.persistence.SaveFeedbackPort;

import java.time.LocalDateTime;
import java.time.ZoneId;

@PersistenceAdapter
@RequiredArgsConstructor
public class FeedbackPersistenceAdapter implements SaveFeedbackPort {

    private final FeedbackRepository feedbackRepository;
    private final FeedbackMapper feedbackMapper;
    private final TimerParameters timerParameters;

    @Override
    public Feedback saveFeedback(CheckBonusCommand checkBonusCommand) {
        var now = LocalDateTime.now(ZoneId.of(timerParameters.getZone()));

        FeedbackModel feedbackModel = FeedbackModel.builder()
                .beneficiaryId(checkBonusCommand.getIdParticipantBeneficiary())
                .withdrawalPointId(checkBonusCommand.getIdParticipantPoint())
                .reportedErrorDate(now)
                .isWrongDate(!checkBonusCommand.getIsValidateDate())
                .isWrongPoint(!checkBonusCommand.getIsPointAssigned())
                .build();

        var rowSaved = feedbackRepository.save(feedbackModel);

        return feedbackMapper.toFeedback(rowSaved);
    }
}
