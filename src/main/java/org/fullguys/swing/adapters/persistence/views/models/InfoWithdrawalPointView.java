package org.fullguys.swing.adapters.persistence.views.models;

import lombok.Data;
import org.fullguys.swing.adapters.persistence.utils.Constants;
import org.springframework.data.annotation.Immutable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Data
@Entity
@Immutable
@Table(name = "VW_INFO_WITHDRAWAL_POINT")
public class InfoWithdrawalPointView {
    @Id
    @Column(name = "participant_point_id")
    private Long id;

    @Column(name = "schedule_id")
    private Long scheduleId;

    @Column(name = "withdrawal_point_code", length = Constants.WITHDRAWAL_POINT_CODE_LENGTH)
    private String withdrawalPointCode;

    @Column(name = "withdrawal_point_name", length = Constants.WITHDRAWAL_POINT_NAME_LENGTH)
    private String withdrawalPointName;

    @Column(name = "address", length = Constants.WITHDRAWAL_POINT_ADDRESS_LENGTH)
    private String address;

    @Column(name = "ubigee")
    private String ubigee;

    @Column(name = "assigned")
    private Long assigned;

    @Column(name = "attended")
    private Long attended;

}
