package org.fullguys.swing.adapters.persistence.mappers;

import org.fullguys.swing.adapters.persistence.models.IncidentTypeModel;
import org.fullguys.swing.domain.entities.IncidentType;
import org.fullguys.swing.domain.entities.enums.IncidentTypeState;
import org.mapstruct.*;
import org.mapstruct.factory.Mappers;

import java.util.List;

@Mapper(componentModel="spring")
public interface IncidentTypeMapper {
    IncidentTypeMapper INSTANCE = Mappers.getMapper(IncidentTypeMapper.class);
    @Mappings({
            @Mapping(source = "id", target = "id"),
            @Mapping(source = "name", target = "name"),
            @Mapping(source = "description", target = "description"),
            @Mapping(source = "createdAt", target = "createdAt")
    })
    @InheritInverseConfiguration
    @Mapping(target = "updatedAt", ignore = true)
    IncidentTypeModel toIncidentTypeModel(IncidentType incidentType);
    IncidentType toIncidentType(IncidentTypeModel incidentTypesModel);
    List<IncidentType> toIncidentTypes(List<IncidentTypeModel> incidentTypesModel);
}
