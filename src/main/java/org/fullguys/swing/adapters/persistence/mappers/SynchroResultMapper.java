package org.fullguys.swing.adapters.persistence.mappers;

import org.fullguys.swing.adapters.persistence.models.SynchroModel;
import org.fullguys.swing.adapters.persistence.models.SynchroResultModel;
import org.fullguys.swing.domain.entities.Synchro;
import org.fullguys.swing.domain.entities.SynchroResult;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;

@Mapper(componentModel="spring")
public interface SynchroResultMapper {

    @Mappings({
            @Mapping(source = "id", target = "synchro.id"),
            @Mapping(source = "result.sourceDataFile", target = "sourceDataFile"),
            @Mapping(target =  "errorDataFile", ignore = true),
            @Mapping(target =  "failedRegistries", ignore = true),
            @Mapping(target =  "successfullRegistries", ignore = true),
            @Mapping(target =  "totalRegistries", ignore = true),
    })
    SynchroResultModel toSynchroResultModel(Synchro synchro);

    @Mappings({
            @Mapping(source = "sourceDataFile", target = "sourceDataFile"),
            @Mapping(target =  "errorDataFile", ignore = true),
            @Mapping(source = "totalRegistries", target =  "totalRegistries"),
            @Mapping(source = "successfullRegistries", target =  "successfullRegistries"),
            @Mapping(source = "failedRegistries", target = "failedRegistries"),
    })
    SynchroResult toSynchroResult(SynchroResultModel synchroResultModel);
}
