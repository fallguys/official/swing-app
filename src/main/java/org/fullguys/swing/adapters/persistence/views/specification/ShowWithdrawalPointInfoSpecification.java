package org.fullguys.swing.adapters.persistence.views.specification;

import org.fullguys.swing.adapters.persistence.views.models.InfoWithdrawalPointView;
import org.fullguys.swing.adapters.web.controllers.admin.schedules.ShowWithdrawalPointInfoSearch;
import org.springframework.data.jpa.domain.Specification;

import javax.persistence.criteria.*;
import java.util.ArrayList;
import java.util.List;

public class ShowWithdrawalPointInfoSpecification implements Specification<InfoWithdrawalPointView> {
    private ShowWithdrawalPointInfoSearch criteria;

    public ShowWithdrawalPointInfoSpecification(ShowWithdrawalPointInfoSearch showWithdrawalPointInfoSearch){
        criteria = showWithdrawalPointInfoSearch;
    }

    @Override
    public Predicate toPredicate(Root<InfoWithdrawalPointView> root, CriteriaQuery<?> query, CriteriaBuilder criteriaBuilder) {
        Path<Long> scheduleId = root.get("scheduleId");
        Path<String> withdrawalPointCode = root.get("withdrawalPointCode");
        Path<String> ubigee = root.get("ubigee");

        final List<Predicate> predicateList = new ArrayList<>();

        if (criteria.getScheduleId() != null)
            predicateList.add(criteriaBuilder.equal(scheduleId, criteria.getScheduleId()));

        if (criteria.getWithdrawalPointCode() != null)
            predicateList.add(criteriaBuilder.equal(withdrawalPointCode, criteria.getWithdrawalPointCode()));

        if (criteria.getUbigee() != null){
            predicateList.add(criteriaBuilder.like(ubigee, criteria.getUbigee()+"%"));
        }

        query.orderBy(criteriaBuilder.desc(root.get("attended")), criteriaBuilder.desc(root.get("assigned")), criteriaBuilder.desc(withdrawalPointCode));
        return criteriaBuilder.and(predicateList.toArray(new Predicate[predicateList.size()]));
    }
}
