package org.fullguys.swing.adapters.persistence.repositories;

import org.fullguys.swing.adapters.persistence.models.IncidentTypeModel;
import org.fullguys.swing.domain.entities.enums.IncidentTypeState;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import java.util.List;

public interface IncidentTypeRepository extends JpaRepository<IncidentTypeModel, Long> {
    List<IncidentTypeModel> findAllByNameContainingOrDescriptionContaining(String name, String description);
    Page<IncidentTypeModel> findAllByNameContaining(String name, Pageable pageable);
    List<IncidentTypeModel> findAllByState(IncidentTypeState incidentTypeState);
}
