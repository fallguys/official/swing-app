package org.fullguys.swing.adapters.persistence.models;

import lombok.Data;
import org.fullguys.swing.adapters.persistence.utils.Constants;

import javax.persistence.*;
import java.util.Date;

@Data
@Entity
@Table(name = "CT_INCIDENT")
public class IncidentModel {

    @Id @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "incident_id")
    private Long id;

    @Column(name = "incident_type_id")
    private Long incidentTypeId;

    @Column(name = "beneficiary_id")
    private Long beneficiaryId;

    @Column(name = "message", length = Constants.INCIDENT_MESSAGE_LENGTH)
    private String message;

    @Column(name = "reported_date")
    private Date reportedDate;

    @ManyToOne
    @JoinColumn(name = "incident_type_id", insertable = false, updatable = false)
    private IncidentTypeModel incidentType;

    @ManyToOne
    @JoinColumn(name = "beneficiary_id", insertable = false, updatable = false)
    private BeneficiaryModel beneficiary;
}
