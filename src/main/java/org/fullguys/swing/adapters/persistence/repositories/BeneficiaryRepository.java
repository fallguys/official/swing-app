package org.fullguys.swing.adapters.persistence.repositories;

import org.fullguys.swing.adapters.persistence.models.BeneficiaryModel;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface BeneficiaryRepository extends JpaRepository<BeneficiaryModel, Long> {

    long countAllByState(String state);
    Optional<BeneficiaryModel> findByCodeEquals(String code);
}
