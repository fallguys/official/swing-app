package org.fullguys.swing.adapters.persistence.repositories;

import org.fullguys.swing.adapters.persistence.models.ScheduleModel;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface ScheduleRepository extends JpaRepository<ScheduleModel, Long> {
    Page<ScheduleModel> findAllByTypeOrderById(String type, Pageable pageable);
    Optional<ScheduleModel> findFirstByTypeAndStateOrderByFinalDateDesc(String type, String state);
    Boolean existsByState(String state);
    Optional<ScheduleModel> findByStateEquals(String scheduleState);
    Optional<ScheduleModel> findFirstByTypeOrderByFinalDateDesc(String type);
    Optional<ScheduleModel> findByIdEqualsAndStateEquals(Long id, String state);
}
