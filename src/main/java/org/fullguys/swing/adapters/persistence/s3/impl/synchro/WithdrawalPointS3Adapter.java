package org.fullguys.swing.adapters.persistence.s3.impl.synchro;

import lombok.RequiredArgsConstructor;
import org.fullguys.swing.adapters.persistence.s3.impl.client.StorageAdapter;
import org.fullguys.swing.common.annotations.PersistenceAdapter;
import org.fullguys.swing.domain.usecases.synchro.ports.out.persistence.SaveWithdrawalPointFilePort;
import org.springframework.web.multipart.MultipartFile;

@PersistenceAdapter
@RequiredArgsConstructor
public class WithdrawalPointS3Adapter implements SaveWithdrawalPointFilePort {

    private final StorageAdapter client;

    @Override
    public String saveWithdrawalPointFile(MultipartFile file) {
        var url = client.saveFile(file);
        return url;
    }
}
