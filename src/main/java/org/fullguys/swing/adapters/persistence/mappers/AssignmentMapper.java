package org.fullguys.swing.adapters.persistence.mappers;

import org.fullguys.swing.adapters.persistence.models.InfoBonusModel;
import org.fullguys.swing.domain.entities.Assignment;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import java.util.List;

@Mapper(componentModel="spring", uses = {WithdrawalPointMapper.class})
public interface AssignmentMapper {

    @Mapping(source = "participantBeneficiary.beneficiary", target = "beneficiary")
    @Mapping(source = "participantPoint.withdrawalPoint", target = "withdrawalPoint")
    @Mapping(source = "initialDateAssigned", target = "infoBonus.assignedInitialDate")
    @Mapping(source = "finalDateAssigned", target = "infoBonus.assignedFinalDate")
    @Mapping(source = "bonusRetired", target = "infoBonus.bonusRetired")
    @Mapping(source = "bonusRetiredDate", target = "infoBonus.bonusRetiredDate")

    Assignment toAssigment(InfoBonusModel infoBonusModel);
    List<Assignment> toAssignmentList(List<InfoBonusModel> infoBonusModels);
}
