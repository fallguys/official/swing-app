package org.fullguys.swing.adapters.persistence.views.repositories;

import org.fullguys.swing.adapters.persistence.views.models.InfoWithdrawalPointView;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface InfoWithdrawalPointRepository extends JpaRepository<InfoWithdrawalPointView, Long>, JpaSpecificationExecutor<InfoWithdrawalPointView> {
}
