package org.fullguys.swing.adapters.persistence.models;

import lombok.Data;

import javax.persistence.*;

@Data
@Entity
@Table(name = "CT_PARTICIPANT_POINT")
public class ParticipantPointModel {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "participant_point_id")
    private Long id;

    @Column(name = "schedule_id")
    private Long scheduleId;

    @Column(name = "withdrawal_point_id")
    private Long withdrawalPointId;

    @ManyToOne
    @JoinColumn(name = "schedule_id", insertable = false, updatable = false)
    private ScheduleModel schedule;

    @ManyToOne
    @JoinColumn(name = "withdrawal_point_id", insertable = false, updatable = false)
    private WithdrawalPointModel withdrawalPoint;

}


