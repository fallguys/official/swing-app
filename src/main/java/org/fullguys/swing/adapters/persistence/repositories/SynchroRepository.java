package org.fullguys.swing.adapters.persistence.repositories;

import org.fullguys.swing.adapters.persistence.models.SynchroModel;
import org.fullguys.swing.domain.entities.enums.BeneficiaryState;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Date;
import java.util.Optional;

public interface SynchroRepository extends JpaRepository<SynchroModel,Long> {
    SynchroModel save(SynchroModel synchroModel);
    Boolean existsByState(String state);
    Optional<SynchroModel> findFirstByFileTypeAndStateOrderByInitialDateDesc(String fileType, String state);
    Optional<SynchroModel> findFirstByFileTypeOrderByInitialDateDesc(String fileType);
}
