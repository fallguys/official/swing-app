CREATE OR REPLACE VIEW VW_INFO_WITHDRAWAL_POINT AS
SELECT ctp.participant_point_id,
       ms.schedule_id,mwp.code AS withdrawal_point_code, mwp.name
           AS withdrawal_point_name, mwp.address, mu.code AS ubigee,
       COUNT(cib.info_bonus_id) AS assigned,
       COUNT(cib2.info_bonus_id) AS attended
FROM mt_withdrawal_point mwp
JOIN mt_ubigee mu on mwp.district_id = mu.ubigee_id
JOIN ct_participant_point ctp ON ctp.withdrawal_point_id = mwp.withdrawal_point_id
JOIN mt_schedule ms ON ms.schedule_id = ctp.schedule_id
LEFT JOIN ct_info_bonus cib ON ctp.participant_point_id = cib.participant_point_id
LEFT JOIN ct_info_bonus cib2 ON ctp.participant_point_id = cib2.participant_point_id
AND cib2.bonus_retired = true
GROUP BY ctp.participant_point_id, ms.schedule_id, mwp.code, mwp.name, mwp.address, mu.code
