package org.fullguys.swing.adapters.persistence.models;

import lombok.Data;
import org.fullguys.swing.adapters.persistence.utils.Constants;

import javax.persistence.*;
import java.util.Date;
import java.util.List;

@Data @Entity
@Table(name = "MT_INCIDENT_TYPE")
public class IncidentTypeModel {

    @Id @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "incident_type_id")
    private Long id;

    @Column(name = "name", length = Constants.INCIDENT_TYPE_NAME_LENGTH)
    private String name;

    @Column(name = "description", length = Constants.INCIDENT_TYPE_DESCRIPTION_LENGTH)
    private String description;

    @Column(name = "created_at")
    private Date createdAt;

    @Column(name = "updated_at")
    private Date updatedAt;

    @Column(name = "state", length = Constants.INCIDENT_TYPE_STATE_LENGTH)
    private String state;
}
