package org.fullguys.swing.adapters.persistence.mappers;

import org.fullguys.swing.adapters.persistence.models.BeneficiaryModel;
import org.fullguys.swing.domain.entities.Beneficiary;
import org.fullguys.swing.domain.entities.enums.BeneficiaryState;
import org.mapstruct.*;

import java.util.List;

@Mapper(componentModel="spring")
public interface BeneficiaryMapper {
    @Mappings({
            @Mapping(source = "id", target = "id"),
            @Mapping(source = "district", target = "district"),
            @Mapping(source = "code", target = "code"),
            @Mapping(source = "isOlderAdult", target = "isOlderAdult"),
            @Mapping(source = "isDisabled", target = "isDisabled"),
            @Mapping(source = "createdAt", target = "createdAt"),
            @Mapping(source = "updatedAt", target = "updatedAt")
    })
    @Mapping(target = "pointsChoosed", ignore = true)
    Beneficiary toBeneficiary(BeneficiaryModel beneficiaryModel);
    List<Beneficiary> toBeneficiaries(List<BeneficiaryModel> beneficiaryModels);

    @InheritInverseConfiguration
    @Mapping(source = "district.id", target = "districtId")
    @Mapping(target = "updatedAt", ignore = true)
    @Mapping(target = "hits", ignore = true)
    @Mapping(target = "infractions", ignore = true)
    BeneficiaryModel toBeneficiaryModel(Beneficiary beneficiary);
    List<BeneficiaryModel> toBeneficiaryModels(List<Beneficiary> beneficiaries);




}
