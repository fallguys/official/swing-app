package org.fullguys.swing.adapters.persistence.repositories;

import org.fullguys.swing.adapters.persistence.models.ParticipantBeneficiaryModel;
import org.fullguys.swing.domain.entities.enums.BeneficiaryState;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;
import java.util.Optional;

public interface ParticipantBeneficiaryRepository extends JpaRepository<ParticipantBeneficiaryModel, Long> {

    Optional<ParticipantBeneficiaryModel> findByBeneficiary_CodeAndScheduleId(String codeBeneficiary, Long idSchedule);

    Optional<ParticipantBeneficiaryModel> findFirstByBeneficiary_CodeAndBeneficiary_State(String codeBeneficiary, String beneficiaryState);

    @Query("SELECT A.id FROM ParticipantBeneficiaryModel A WHERE A.beneficiaryId = (:idBeneficiary) AND A.scheduleId = (:idSchedule)")
    Long getIdParticipantBeneficiary(@Param("idBeneficiary") Long idBeneficiary, @Param("idSchedule") Long idSchedule);

    List<ParticipantBeneficiaryModel> findAllBySchedule_Id(Long scheduleId);
    Integer countAllByScheduleId(Long scheduleId);
    Optional<ParticipantBeneficiaryModel> findByBeneficiary_CodeAndScheduleState(String codeBeneficiary, String status);
}
