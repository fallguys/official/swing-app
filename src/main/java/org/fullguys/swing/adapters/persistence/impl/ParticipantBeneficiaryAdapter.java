package org.fullguys.swing.adapters.persistence.impl;

import lombok.RequiredArgsConstructor;
import org.fullguys.swing.adapters.persistence.mappers.BeneficiaryMapper;
import org.fullguys.swing.adapters.persistence.repositories.ParticipantBeneficiaryRepository;
import org.fullguys.swing.common.annotations.PersistenceAdapter;
import org.fullguys.swing.domain.entities.Beneficiary;
import org.fullguys.swing.domain.entities.enums.BeneficiaryState;
import org.fullguys.swing.domain.entities.enums.ScheduleState;
import org.fullguys.swing.domain.usecases.feedback.ports.out.persistence.GetParticipantBeneficiaryPort;
import org.fullguys.swing.domain.usecases.incidents.ports.out.persistence.GetBeneficiaryByCodePort;
import org.fullguys.swing.domain.usecases.scheduling.ports.out.scheduler.ShowTotalBeneficiariesForSchedulePort;

import java.util.Optional;

@PersistenceAdapter
@RequiredArgsConstructor
public class ParticipantBeneficiaryAdapter implements GetParticipantBeneficiaryPort, ShowTotalBeneficiariesForSchedulePort, GetBeneficiaryByCodePort {

    private final BeneficiaryMapper beneficiaryMapper;
    private final ParticipantBeneficiaryRepository participantBeneficiaryRepository;

    @Override
    public Optional<Beneficiary> getParticipantBeneficiary(String codeBeneficiary, Long idSchedule) {
        var row = participantBeneficiaryRepository.findByBeneficiary_CodeAndScheduleId(codeBeneficiary, idSchedule);
        return row.map(r -> beneficiaryMapper.toBeneficiary(r.getBeneficiary()));
    }

    @Override
    public Long getIdParticipantBeneficiary(Long idBeneficiary, Long idSchedule) {
        return participantBeneficiaryRepository.getIdParticipantBeneficiary(idBeneficiary, idSchedule);
    }

    @Override
    public Integer showTotalBeneficiariesForSchedule(Long scheduleId) {
        return participantBeneficiaryRepository.countAllByScheduleId(scheduleId);
    }

    @Override
    public Optional<Beneficiary> getBeneficiaryByCode(String code) {
        var row = participantBeneficiaryRepository.findFirstByBeneficiary_CodeAndBeneficiary_State(code, BeneficiaryState.ACTIVE.toString());
        return row.map(r -> beneficiaryMapper.toBeneficiary(r.getBeneficiary()));
    }
}
