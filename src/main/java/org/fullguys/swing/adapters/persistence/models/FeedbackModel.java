package org.fullguys.swing.adapters.persistence.models;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.Date;

@Data @Builder
@AllArgsConstructor
@Entity
@Table(name = "CT_FEEDBACK")
public class FeedbackModel {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "feedback_id")
    private Long id;

    @Column(name = "beneficiary_id")
    private Long beneficiaryId;

    @Column(name = "withdrawal_point_id")
    private Long withdrawalPointId;

    @Column(name = "reported_error_date")
    private LocalDateTime reportedErrorDate;

    @Column(name = "is_wrong_date")
    private Boolean isWrongDate;

    @Column(name = "is_wrong_point")
    private Boolean isWrongPoint;

    @ManyToOne
    @JoinColumn(name = "beneficiary_id", insertable = false, updatable = false)
    private ParticipantBeneficiaryModel participantBeneficiary;

    @ManyToOne
    @JoinColumn(name = "withdrawal_point_id", insertable = false, updatable = false)
    private ParticipantPointModel participantWithdrawalPoint;

}
