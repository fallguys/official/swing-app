package org.fullguys.swing.adapters.persistence.repositories;


import org.fullguys.swing.adapters.persistence.models.InfoBonusModel;
import org.springframework.data.domain.Page;
import org.springframework.data.jpa.repository.JpaRepository;


import org.springframework.data.domain.Pageable;
import java.util.List;
import java.util.Optional;

public interface InfoBonusRepository extends JpaRepository<InfoBonusModel, Long> {
    InfoBonusModel findById(int beneficiaryId);
    InfoBonusModel findByParticipantBeneficiary_Id(Long idParticipantBeneficiary);
    InfoBonusModel save(InfoBonusModel infoBonusModel);
    Optional<InfoBonusModel> findByParticipantBeneficiary_BeneficiaryCodeAndParticipantBeneficiaryScheduleState(String search, String state);
    InfoBonusModel findByParticipantBeneficiaryId(Long id);
    List<InfoBonusModel> findByParticipantPointWithdrawalPointCode(String withdrawalPointCode);
    List<InfoBonusModel> findByParticipantPointWithdrawalPointCodeAndBonusRetiredEquals(String withdrawalPointCode, boolean bonusRetired);
    Page<InfoBonusModel> findByParticipantBeneficiaryScheduleIdAndParticipantPointScheduleId(Long id1, Long id2, Pageable pageable);
    Integer countAllByParticipantPointWithdrawalPointCode(String withdrawalPointCode);
    Integer countAllByParticipantPointWithdrawalPointCodeAndBonusRetired(String withdrawalPointCode, boolean bonusRetired);
}
