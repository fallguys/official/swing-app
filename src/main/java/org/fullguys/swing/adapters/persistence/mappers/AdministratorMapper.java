package org.fullguys.swing.adapters.persistence.mappers;

import org.fullguys.swing.adapters.persistence.models.AdministratorModel;
import org.fullguys.swing.domain.entities.Administrator;
import org.mapstruct.*;

@Mapper(componentModel="spring")
public interface AdministratorMapper {
    @Mappings({
            @Mapping(source = "id", target = "id"),
            @Mapping(source = "names", target = "names"),
            @Mapping(source = "fatherLastname", target = "fatherLastname"),
            @Mapping(source = "motherLastname", target = "motherLastname"),
            @Mapping(source = "email", target = "email"),
            @Mapping(source = "password", target = "password")
    })
    Administrator toAdministrator(AdministratorModel administratorModel);

    @InheritInverseConfiguration
    AdministratorModel toProductEntity(Administrator administrator);
}
