package org.fullguys.swing.adapters.persistence.mappers;

import org.fullguys.swing.adapters.persistence.models.InfoBonusModel;
import org.fullguys.swing.domain.entities.InfoBonus;
import org.mapstruct.InheritInverseConfiguration;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;

@Mapper(componentModel="spring", uses = {InfoBonusMapper.class})
public interface InfoBonusMapper {
    @Mappings({
            @Mapping(source = "initialDateAssigned", target = "assignedInitialDate"),
            @Mapping(source = "finalDateAssigned", target = "assignedFinalDate"),
            @Mapping(source = "bonusRetired", target = "bonusRetired"),
            @Mapping(source = "bonusRetiredDate", target = "bonusRetiredDate"),
    })
    InfoBonus toInfoBonus(InfoBonusModel infoBonusModel);

    @InheritInverseConfiguration
    InfoBonusModel toInfoBonusModel(InfoBonus infoBonus);
}
