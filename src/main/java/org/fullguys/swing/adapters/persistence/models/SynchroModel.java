package org.fullguys.swing.adapters.persistence.models;

import lombok.Data;
import org.fullguys.swing.adapters.persistence.utils.Constants;

import javax.persistence.*;
import java.util.Date;

@Data
@Entity
@Table(name = "MT_SYNCHRO")
public class SynchroModel {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "synchro_id")
    private Long id;

    @Column(name = "administrator_id")
    private Long administratorId;

    @Column(name = "file_type", length = Constants.SYNCHRO_FILE_TYPE_LENGTH)
    private String fileType;

    @Column(name = "initial_date")
    private Date initialDate;

    @Column(name = "state", length = Constants.SYNCHRO_STATE_LENGTH)
    private String state;

    @ManyToOne
    @JoinColumn(name = "administrator_id", insertable = false, updatable = false)
    private AdministratorModel administrator;

}
