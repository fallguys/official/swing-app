package org.fullguys.swing.adapters.persistence.models;

import lombok.Data;

import javax.persistence.*;

@Data
@Entity
@Table(name = "CT_PARTICIPANT_BENEFECIARY")
public class ParticipantBeneficiaryModel {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "participant_beneficiary_id")
    private Long id;

    @Column(name = "schedule_id")
    private Long scheduleId;

    @Column(name = "beneficiary_id")
    private Long beneficiaryId;

    @ManyToOne
    @JoinColumn(name = "schedule_id", insertable = false, updatable = false)
    private ScheduleModel schedule;

    @ManyToOne
    @JoinColumn(name = "beneficiary_id", referencedColumnName = "beneficiary_id", insertable = false, updatable = false)
    private BeneficiaryModel beneficiary;

}
