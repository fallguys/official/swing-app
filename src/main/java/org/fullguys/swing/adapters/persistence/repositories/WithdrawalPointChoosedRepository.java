package org.fullguys.swing.adapters.persistence.repositories;

import org.fullguys.swing.adapters.persistence.models.WithdrawalPointChoosedModel;
import org.springframework.data.jpa.repository.JpaRepository;

import javax.transaction.Transactional;

public interface WithdrawalPointChoosedRepository extends JpaRepository<WithdrawalPointChoosedModel, Long> {
    @Transactional
    Long deleteByBeneficiaryCode(String code);
}
