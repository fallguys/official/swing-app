package org.fullguys.swing.adapters.persistence.repositories;

import org.fullguys.swing.adapters.persistence.models.BeneficiaryModel;
import org.fullguys.swing.adapters.persistence.models.ParticipantPointModel;
import org.fullguys.swing.domain.entities.Beneficiary;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;
import java.util.Optional;

public interface ParticipantPointRepository extends JpaRepository<ParticipantPointModel,Long> {
    Optional<ParticipantPointModel> findByWithdrawalPoint_CodeAndScheduleId(String codeWithdrawalPoint, Long idSchedule);

    @Query("SELECT A.id FROM ParticipantPointModel A WHERE A.withdrawalPointId = (:idPoint) AND A.scheduleId = (:idSchedule)")
    Long getIdParticipantPoint(@Param("idPoint") Long idPoint, @Param("idSchedule") Long idSchedule);

    List<ParticipantPointModel> findAllBySchedule_Id(Long scheduleId);

    Page<ParticipantPointModel> findByScheduleIdEquals(Long id, Pageable pageable);
    List<ParticipantPointModel> findByWithdrawalPointCode(String code);
    Integer countAllByScheduleId(Long scheduleId);
    List<ParticipantPointModel> findByWithdrawalPoint_DistrictCodeAndScheduleState(String ubigee, String state);
}
