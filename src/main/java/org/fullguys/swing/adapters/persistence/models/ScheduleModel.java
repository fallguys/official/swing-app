package org.fullguys.swing.adapters.persistence.models;

import lombok.Data;
import org.fullguys.swing.adapters.persistence.utils.Constants;

import javax.persistence.*;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Date;

@Data
@Entity
@Table(name = "MT_SCHEDULE")
public class ScheduleModel {

    @Id @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "schedule_id")
    private Long id;

    @Column(name = "administrator_id")
    private Long administratorId;

    @Column(name = "creation_date")
    private LocalDateTime creationDate;

    @Column(name = "initial_date")
    private LocalDate initialDate;

    @Column(name = "final_date")
    private LocalDate finalDate;

    @Column(name = "type", length = Constants.SCHEDULE_TYPE_LENGTH)
    private String type;

    @Column(name = "state", length = Constants.SCHEDULE_STATE_LENGTH)
    private String state;

    @ManyToOne
    @JoinColumn(name = "administrator_id", insertable = false, updatable = false)
    private AdministratorModel administrator;

}
