package org.fullguys.swing.adapters.persistence.impl;

import lombok.RequiredArgsConstructor;
import org.fullguys.swing.adapters.persistence.mappers.WithdrawalPointMapper;
import org.fullguys.swing.adapters.persistence.repositories.WithdrawalPointRepository;
import org.fullguys.swing.common.annotations.PersistenceAdapter;
import org.fullguys.swing.common.queries.Filters;
import org.fullguys.swing.common.queries.Paginator;
import org.fullguys.swing.domain.entities.WithdrawalPoint;
import org.fullguys.swing.domain.entities.enums.WithdrawalPointState;
import org.fullguys.swing.domain.usecases.beneficiaries.ports.out.persistence.GetWithdrawalPointsByUbigeePort;
import org.fullguys.swing.domain.usecases.feedback.ports.out.persistence.UpdatePointPort;
import org.fullguys.swing.domain.usecases.synchro.ports.out.persistence.CountWithdrawalPointsByStatusPort;
import org.fullguys.swing.domain.usecases.beneficiaries.ports.out.persistence.ShowWithdrawalPointsForUbigeePort;
import org.fullguys.swing.domain.usecases.synchro.ports.out.persistence.ListWithdrawalPointsPort;
import org.springframework.data.domain.PageRequest;

import java.util.List;

@PersistenceAdapter
@RequiredArgsConstructor
public class WithdrawalPointPersistenceAdapter implements ListWithdrawalPointsPort, CountWithdrawalPointsByStatusPort, UpdatePointPort, ShowWithdrawalPointsForUbigeePort, GetWithdrawalPointsByUbigeePort {

    private final WithdrawalPointRepository withdrawalPointRepository;
    private final WithdrawalPointMapper withdrawalPointMapper;

    @Override
    public Paginator<WithdrawalPoint> listWithdrawalPoints(Filters filters) {
        var withdrawalPointsRows = withdrawalPointRepository.findAll(PageRequest.of(filters.getPage(), filters.getPagesize()));
        var withdrawalPoints = withdrawalPointMapper.toWithdrawalPoints(withdrawalPointsRows.getContent());
        return new Paginator<>(withdrawalPoints, filters.getPage(), filters.getPagesize(), withdrawalPointsRows.getTotalElements());
    }

    @Override
    public long countByState(WithdrawalPointState withdrawalPointState) {
        return withdrawalPointRepository.countAllByState(withdrawalPointState.name());
    }

    @Override
    public void updatePoint(Long idPoint) {
        var row = withdrawalPointRepository.findById(idPoint);
        row.ifPresent(r -> {
            r.setInfractions(r.getInfractions()+1L);
            withdrawalPointRepository.save(r);
        });
    }

    @Override
    public Paginator<WithdrawalPoint> showWithdrawalPoints(Filters filters) {
        var actualPage = withdrawalPointRepository
                .findAllByDistrictCode(filters.getSearch(),
                        PageRequest.of(filters.getPage(), filters.getPagesize()));
        var saved = withdrawalPointMapper.toWithdrawalPoints(actualPage.getContent());
        return new Paginator<>(saved, filters.getPage(), filters.getPagesize(),
                actualPage.getTotalElements());
    }

    @Override
    public List<WithdrawalPoint> getWithdrawalPointsByUbigee(String ubigee) {
        var withdrawalPointModels = withdrawalPointRepository.findAllByDistrictCodeAndState(ubigee, WithdrawalPointState.ACTIVE.toString());
        var withdrawalPoints = withdrawalPointMapper.toWithdrawalPoints(withdrawalPointModels);
        return withdrawalPoints;
    }
}
