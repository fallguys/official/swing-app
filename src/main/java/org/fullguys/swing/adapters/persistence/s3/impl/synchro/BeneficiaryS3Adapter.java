package org.fullguys.swing.adapters.persistence.s3.impl.synchro;

import lombok.RequiredArgsConstructor;
import org.fullguys.swing.adapters.persistence.s3.impl.client.StorageAdapter;
import org.fullguys.swing.common.annotations.PersistenceAdapter;
import org.fullguys.swing.domain.usecases.synchro.ports.out.persistence.SaveBeneficiaryFilePort;
import org.springframework.web.multipart.MultipartFile;


@PersistenceAdapter
@RequiredArgsConstructor
public class BeneficiaryS3Adapter implements SaveBeneficiaryFilePort {

    private final StorageAdapter client;

    @Override
    public String saveBeneficiaryFile(MultipartFile multipartFile) {
        var url = client.saveFile(multipartFile);
        return url;
    }
}
