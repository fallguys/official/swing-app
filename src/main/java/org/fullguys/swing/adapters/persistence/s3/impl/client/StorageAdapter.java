package org.fullguys.swing.adapters.persistence.s3.impl.client;

import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;
import com.amazonaws.services.s3.model.CannedAccessControlList;
import com.amazonaws.services.s3.model.PutObjectRequest;
import lombok.Data;
import lombok.RequiredArgsConstructor;
import org.fullguys.swing.adapters.persistence.s3.config.StorageCredentials;
import org.fullguys.swing.common.annotations.S3Adapter;
import org.fullguys.swing.common.converter.MultipartFileConverter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;

@Data
@S3Adapter
@RequiredArgsConstructor
public class StorageAdapter {

    private final StorageCredentials storageCredentials;
    private final MultipartFileConverter multipartFileConverter;
    private final Logger logger = LoggerFactory.getLogger(StorageAdapter.class);

    public AmazonS3 getS3Client(){
        BasicAWSCredentials awsCredentials = new BasicAWSCredentials(
                storageCredentials.getCredentials().getAccessKey(),
                storageCredentials.getCredentials().getSecretKey());

        return AmazonS3ClientBuilder.standard()
                .withRegion(storageCredentials.getRegion())
                .withCredentials(new AWSStaticCredentialsProvider(awsCredentials))
                .build();
    }

    public String saveFile(MultipartFile multipartFile) {
        try {
            File file = multipartFileConverter.convertMultipartFileToFile(multipartFile);

            var request = new PutObjectRequest(storageCredentials.getBucket().getName(), file.getName(), file)
                    .withCannedAcl(CannedAccessControlList.PublicRead);
            this.getS3Client().putObject(request);

            return this.getS3Client()
                    .getUrl(storageCredentials.getBucket().getName(), file.getName())
                    .toString();

        } catch (Exception e) {
            logger.error("Ha ocurrido un error al momento de guardar el archivo de datos al servicio AWS S3", e);
            return null;
        }
    }
}
