package org.fullguys.swing.adapters.persistence.views.models;

import lombok.Data;
import org.fullguys.swing.adapters.persistence.utils.Constants;
import org.hibernate.annotations.Subselect;
import org.springframework.data.annotation.Immutable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.time.LocalDateTime;

@Data
@Entity
@Immutable
@Table(name = "VW_INFO_BENEFICIARY")
public class InfoBeneficiaryView {

    @Id
    @Column(name = "info_beneficiary_id")
    private Long id;

    @Column(name = "beneficiary_code", length = Constants.BENEFICIARY_CODE_LENGTH)
    private String beneficiaryCode;

    @Column(name = "schedule_id")
    private Long scheduleId;

    @Column(name = "withdrawal_point_code", length = Constants.WITHDRAWAL_POINT_CODE_LENGTH)
    private String withdrawalPontCode;

    @Column(name = "withdrawal_point_name", length = Constants.WITHDRAWAL_POINT_NAME_LENGTH)
    private String withdrawalPointName;

    @Column(name = "ubigee", length = Constants.UBIGEE_CODE_LENGTH)
    private String ubigee;

    @Column(name = "initial_date_assigned")
    private LocalDateTime initialDateAssigned;

    @Column(name = "final_date_assigned")
    private LocalDateTime finalDateAssigned;

    @Column(name = "bonus_retired")
    private Boolean bonusRetired;

    @Column(name = "bonus_retired_date")
    private LocalDateTime bonusRetiredDate;

    @Column(name = "mistakes")
    private Long mistakes;
}
