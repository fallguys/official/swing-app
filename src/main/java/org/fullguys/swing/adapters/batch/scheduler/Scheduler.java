package org.fullguys.swing.adapters.batch.scheduler;

import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.services.batch.AWSBatch;
import com.amazonaws.services.batch.AWSBatchClientBuilder;
import com.amazonaws.services.batch.model.SubmitJobRequest;
import lombok.RequiredArgsConstructor;
import org.fullguys.swing.adapters.batch.scheduler.config.SchedulerCredentials;
import org.fullguys.swing.common.annotations.BatchAdapter;
import org.fullguys.swing.domain.usecases.scheduling.ports.out.scheduler.RegisterRequestToGenerateScheduleBatchPort;

@BatchAdapter
@RequiredArgsConstructor
public class Scheduler implements RegisterRequestToGenerateScheduleBatchPort {

    private final SchedulerCredentials schedulerCredentials;

    @Override
    public void registerRequestToGenerateSchedule(Long scheduleId) {
        BasicAWSCredentials awsCredentials = new BasicAWSCredentials(
                schedulerCredentials.getCredentials().getAccessKey(),
                schedulerCredentials.getCredentials().getSecretKey());

        AWSBatch client = AWSBatchClientBuilder
                .standard()
                .withCredentials(new AWSStaticCredentialsProvider(awsCredentials))
                .withRegion(schedulerCredentials.getRegion())
                .build();

        SubmitJobRequest request = new SubmitJobRequest()
                .withJobName(schedulerCredentials.getJob().getName())
                .withJobQueue(schedulerCredentials.getJob().getQueue())
                .withJobDefinition(schedulerCredentials.getJob().getDefinition());


        client.submitJob(request);
    }
}
