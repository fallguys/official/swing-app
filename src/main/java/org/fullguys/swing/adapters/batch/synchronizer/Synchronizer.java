package org.fullguys.swing.adapters.batch.synchronizer;

import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.services.batch.AWSBatch;
import com.amazonaws.services.batch.AWSBatchClientBuilder;
import com.amazonaws.services.batch.model.SubmitJobRequest;
import lombok.RequiredArgsConstructor;
import org.fullguys.swing.adapters.batch.synchronizer.config.SynchroCredentials;
import org.fullguys.swing.common.annotations.PersistenceAdapter;
import org.fullguys.swing.domain.usecases.synchro.ports.out.synchronizer.RegisterBeneficiariesDataBatchPort;
import org.fullguys.swing.domain.usecases.synchro.ports.out.synchronizer.RegisterContagionAreasDataBatchPort;
import org.fullguys.swing.domain.usecases.synchro.ports.out.synchronizer.RegisterWithdrawalPointsDataBatchPort;

@PersistenceAdapter
@RequiredArgsConstructor
public class Synchronizer implements RegisterBeneficiariesDataBatchPort, RegisterContagionAreasDataBatchPort, RegisterWithdrawalPointsDataBatchPort {

    private final SynchroCredentials synchroCredentials;

    private void startJob() {
        SubmitJobRequest request = new SubmitJobRequest()
                .withJobName(synchroCredentials.getJob().getName())
                .withJobQueue(synchroCredentials.getJob().getQueue())
                .withJobDefinition(synchroCredentials.getJob().getDefinition());

        makeClient().submitJob(request);
    }

    private AWSBatch makeClient() {
        BasicAWSCredentials awsCredentials = new BasicAWSCredentials(
                synchroCredentials.getCredentials().getAccessKey(),
                synchroCredentials.getCredentials().getSecretKey());

        return AWSBatchClientBuilder.standard()
                .withCredentials(new AWSStaticCredentialsProvider(awsCredentials))
                .withRegion(synchroCredentials.getRegion())
                .build();
    }

    @Override
    public void registerBeneficiariesDataBatch() {
        startJob();
    }

    @Override
    public void registerContagionAreasDataBatch() {
        startJob();
    }

    @Override
    public void registerWithdrawalPointsDataBatch() {
        startJob();
    }
}
