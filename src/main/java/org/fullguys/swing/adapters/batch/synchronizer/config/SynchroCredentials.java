package org.fullguys.swing.adapters.batch.synchronizer.config;

import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Configuration
@Getter @Setter
@ConfigurationProperties(prefix = "synchronizer.aws")
public class SynchroCredentials {

    private String region;
    private Credentials credentials;
    private Job job;

    @Getter @Setter
    public static class Credentials {
        private String accessKey;
        private String secretKey;
    }

    @Getter @Setter
    public static class Job {
        private String name;
        private String queue;
        private String definition;
    }
}
