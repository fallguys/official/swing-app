package org.fullguys.swing.adapters.web.controllers.sim;

import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.fullguys.swing.common.annotations.WebAdapter;
import org.fullguys.swing.domain.usecases.feedback.ports.in.services.SaveFeedbackService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@WebAdapter
@CrossOrigin
@RestController
@RequiredArgsConstructor
@RequestMapping(value = "/sim/participants/beneficiaries")
public class SaveFeedbackController {

    private final SaveFeedbackService saveFeedbackService;

    @PostMapping("/{codeBeneficiary}/feedback")
    @ApiOperation("Save Feedback")
    public ResponseEntity<?> saveFeedback(@PathVariable String codeBeneficiary, @RequestParam String codePoint){

        saveFeedbackService.saveFeedback(codeBeneficiary, codePoint);

        return ResponseEntity.ok().build();
    }
}
