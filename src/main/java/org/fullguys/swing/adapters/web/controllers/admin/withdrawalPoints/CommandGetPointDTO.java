package org.fullguys.swing.adapters.web.controllers.admin.withdrawalPoints;

import lombok.Builder;
import lombok.Data;
import org.fullguys.swing.common.queries.Paginator;

import java.time.LocalDateTime;

@Data
@Builder
public class CommandGetPointDTO {
    private Paginator<GetWithdrawalPointsDTO> pointsPaginator;
    private LocalDateTime lastUpdated;
    private Long totalActive;
}
