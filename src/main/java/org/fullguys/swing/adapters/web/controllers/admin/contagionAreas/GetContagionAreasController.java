package org.fullguys.swing.adapters.web.controllers.admin.contagionAreas;

import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.fullguys.swing.common.annotations.WebAdapter;
import org.fullguys.swing.common.queries.Filters;
import org.fullguys.swing.common.queries.Paginator;
import org.fullguys.swing.domain.entities.ContagionArea;
import org.fullguys.swing.domain.usecases.synchro.ports.in.services.GetContagionAreaService;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;


@WebAdapter
@CrossOrigin
@RestController
@RequiredArgsConstructor
@RequestMapping(value = "/contagionAreas")
public class GetContagionAreasController {

    private final GetContagionAreaService getContagionAreaService;
    private final ContagionAreaDTOMapper contagionAreaDTOMapper;

    @GetMapping("")
    @ApiOperation("Get contagion areas")
    public ResponseEntity<CommandGetContagionAreaDTO> getContagionAreas(
            Pageable pageable,
            @RequestParam(defaultValue = "") String search) {

        var filters = Filters.builder()
                .page(pageable.getPageNumber())
                .pagesize(pageable.getPageSize())
                .search(search)
                .build();

        var contagionAreasCommand = getContagionAreaService.getContagionAreas(filters);
        var pagination = contagionAreasCommand.getPaginator();
        var data = contagionAreaDTOMapper.toDTOContagionAreas(pagination.getData());
        var res = new Paginator<>(data, pagination.getPage(), pagination.getPagesize(), pagination.getTotal());

        var commandDTO = CommandGetContagionAreaDTO.builder()
                .contagionAreasPaginator(res)
                .lastUpdated(contagionAreasCommand.getLastUpdated())
                .totalActive(contagionAreasCommand.getTotalActive())
                .build();

        return ResponseEntity.ok(commandDTO);
    }
}
