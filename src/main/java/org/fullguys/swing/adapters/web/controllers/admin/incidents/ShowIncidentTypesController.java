package org.fullguys.swing.adapters.web.controllers.admin.incidents;

import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.fullguys.swing.common.annotations.WebAdapter;
import org.fullguys.swing.common.queries.Filters;
import org.fullguys.swing.common.queries.Paginator;
import org.fullguys.swing.domain.entities.IncidentType;
import org.fullguys.swing.domain.usecases.incidents.ports.in.services.ShowIncidentTypesService;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;


@WebAdapter
@CrossOrigin
@RestController
@RequiredArgsConstructor
@RequestMapping(value = "/incidentType")
public class ShowIncidentTypesController {

    private final ShowIncidentTypesService showIncidentTypesService;

    @GetMapping("")
    @ApiOperation("Show incident types for the respective filters")
    public ResponseEntity<Paginator<IncidentType>> showIncidentTypes(
            Pageable pageable,
            @RequestParam(value="search", defaultValue = "") String search) {
        var filters = Filters.builder()
                .page(pageable.getPageNumber())
                .pagesize(pageable.getPageSize())
                .search(search)
                .build();
        var rows = showIncidentTypesService.showIncidentTypes(filters);
        return ResponseEntity.ok(rows);
    }
}
