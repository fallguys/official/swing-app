package org.fullguys.swing.adapters.web.controllers.admin.withdrawalPoints;

import lombok.Data;

import java.util.List;

@Data
public class SelectWithdrawalPointDTO {
    private String beneficiaryCode;
    private List<String> withdrawalPointCodes;
}
