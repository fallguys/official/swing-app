package org.fullguys.swing.adapters.web.controllers.admin.schedules;


import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.time.LocalDate;

@Data
public class ShowBeneficiariesInfoSearch {
    Long scheduleId;

    String beneficiaryCode;

    String withdrawalPointCode;

    String ubigee;

    @DateTimeFormat(pattern = "yyyy-MM-dd")
    LocalDate initialDate;

    @DateTimeFormat(pattern = "yyyy-MM-dd")
    LocalDate finalDate;

    Boolean state;
}
