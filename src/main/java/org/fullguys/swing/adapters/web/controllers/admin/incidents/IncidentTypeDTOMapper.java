package org.fullguys.swing.adapters.web.controllers.admin.incidents;

import org.fullguys.swing.domain.entities.IncidentType;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;

@Mapper(componentModel="spring")
public interface IncidentTypeDTOMapper {
    @Mappings({
            @Mapping(source = "name", target = "name"),
            @Mapping(source = "description", target = "description"),
            @Mapping(target = "id", ignore = true),
            @Mapping(target = "state", ignore = true),
            @Mapping(target = "createdAt", ignore = true)
    })
    IncidentType toIncidentType(RegisterIncidentTypeDTO incidentTypeDTO);
}
