package org.fullguys.swing.adapters.web.controllers.admin.infoBonus;

import org.fullguys.swing.domain.entities.Assignment;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;

@Mapper(componentModel="spring")
public interface ShowInfoBonusDTOMapper {
    @Mappings({
            @Mapping(source = "withdrawalPoint.address", target = "address"),
            @Mapping(source = "withdrawalPoint.name", target = "withdrawalPointName"),
            @Mapping(source = "infoBonus.assignedInitialDate", target = "assignedInitialDate"),
            @Mapping(source = "infoBonus.assignedFinalDate", target = "assignedFinalDate")
    })
    ShowInfoBonusDTO toShowInfoBonusDto(Assignment infoBonus);
}
