package org.fullguys.swing.adapters.web.controllers.admin.contagionAreas;

import lombok.Builder;
import lombok.Data;
import org.fullguys.swing.adapters.web.controllers.admin.beneficiaries.GetBeneficiaryDTO;
import org.fullguys.swing.common.queries.Paginator;

import java.time.LocalDateTime;

@Data
@Builder
public class CommandGetContagionAreaDTO {
    private Paginator<GetContagionAreaDTO> contagionAreasPaginator;
    private LocalDateTime lastUpdated;
    private Long totalActive;
}
