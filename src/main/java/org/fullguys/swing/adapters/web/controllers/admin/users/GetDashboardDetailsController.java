package org.fullguys.swing.adapters.web.controllers.admin.users;

import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.fullguys.swing.common.annotations.WebAdapter;
import org.fullguys.swing.domain.entities.enums.ScheduleType;
import org.fullguys.swing.domain.usecases.scheduling.ports.in.services.GetCurrentScheduleService;
import org.fullguys.swing.domain.usecases.synchro.ports.in.services.GetLastActiveSynchroService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@WebAdapter
@CrossOrigin
@RestController
@RequiredArgsConstructor
@RequestMapping(value = "/dashboard")
public class GetDashboardDetailsController {

    private final GetLastActiveSynchroService getSynchrosService;
    private final GetCurrentScheduleService getCurrentScheduleService;
    private final GetDashboardDetailsMapper getDashboardDetailsMapper;

    @GetMapping("")
    @ApiOperation("Get dashboard details")
    public ResponseEntity<GetDashboardDetailsDTO> getSynchros(){
        var activeSynchro = getSynchrosService.getLastActiveSynchro();
        var activeSchedule = getCurrentScheduleService.getCurrentSchedule();
        var getDashboardDetailsDTO = getDashboardDetailsMapper.getDashboardDetailsDTO(activeSynchro, activeSchedule);
        return ResponseEntity.ok(getDashboardDetailsDTO);
    }
}
