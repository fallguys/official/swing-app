package org.fullguys.swing.adapters.web.controllers.admin.beneficiaries;

import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.fullguys.swing.common.annotations.WebAdapter;
import org.fullguys.swing.domain.entities.Ubigee;
import org.fullguys.swing.domain.entities.enums.UbigeeType;
import org.fullguys.swing.domain.usecases.beneficiaries.ports.in.services.ShowUbigeesService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@WebAdapter
@CrossOrigin
@RestController
@RequiredArgsConstructor
@RequestMapping(value = "/ubigees")
public class ShowUbigeesForBeneficiaryController {
    private final ShowUbigeesService showUbigeesService;
    @GetMapping("")
    @ApiOperation("Get ubigees for combobox")
    public ResponseEntity<List<Ubigee>> showUbigeesForBeneficiary(
            @RequestParam (defaultValue = "") Long id,
            @RequestParam String type
    ){
        try{
            var ubigees = showUbigeesService.showUbigees(id, UbigeeType.valueOf(type));
            return ResponseEntity.ok(ubigees);
        }catch (Exception e){
            return null;
        }

    }
}
