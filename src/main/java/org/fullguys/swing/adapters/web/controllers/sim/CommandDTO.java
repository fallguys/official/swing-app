package org.fullguys.swing.adapters.web.controllers.sim;

import lombok.Data;

import java.time.LocalDateTime;

@Data
public class CommandDTO {
    private Boolean isPointAssigned;
    private String namePointAssigned;
    private Boolean isValidateDate;
    private LocalDateTime initialDateAssigned;
    private LocalDateTime finalDateAssigned;
    private Boolean retiredBonus;
    private LocalDateTime dateRetiredBonus;
}
