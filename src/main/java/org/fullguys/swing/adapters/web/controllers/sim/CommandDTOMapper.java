package org.fullguys.swing.adapters.web.controllers.sim;

import org.fullguys.swing.domain.usecases.feedback.commands.CheckBonusCommand;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;

@Mapper(componentModel = "spring")
public interface CommandDTOMapper {

    @Mappings({
            @Mapping(source = "isPointAssigned", target = "isPointAssigned"),
            @Mapping(source = "namePointAssigned", target = "namePointAssigned"),
            @Mapping(source = "isValidateDate", target = "isValidateDate"),
            @Mapping(source = "initialDateAssigned", target = "initialDateAssigned"),
            @Mapping(source = "finalDateAssigned", target = "finalDateAssigned"),
            @Mapping(source = "retiredBonus", target = "retiredBonus"),
            @Mapping(source = "dateRetiredBonus", target = "dateRetiredBonus"),
    })

    CommandDTO toDTOCommand(CheckBonusCommand checkBonusCommand);
}
