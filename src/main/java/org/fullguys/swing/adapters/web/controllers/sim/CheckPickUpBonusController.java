package org.fullguys.swing.adapters.web.controllers.sim;

import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.fullguys.swing.common.annotations.WebAdapter;
import org.fullguys.swing.domain.usecases.feedback.commands.CheckBonusCommand;
import org.fullguys.swing.domain.usecases.feedback.ports.in.services.CheckPickUpBonusService;
import org.springframework.data.repository.query.Param;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@WebAdapter
@CrossOrigin
@RestController
@RequiredArgsConstructor
@RequestMapping(value = "/sim/participants/beneficiaries")
public class CheckPickUpBonusController {

    private final CheckPickUpBonusService checkPickUpBonusService;
    private final CommandDTOMapper commandDTOMapper;

    @GetMapping("/{codeBeneficiary}/checking")
    @ApiOperation("Check pick up Bonus")
    public ResponseEntity<CommandDTO> checkPickUpBonus(@PathVariable String codeBeneficiary, @RequestParam String codePoint){
        var checkPickUp = checkPickUpBonusService.checkPickUpBonus(codeBeneficiary, codePoint);
        return ResponseEntity.ok(commandDTOMapper.toDTOCommand(checkPickUp));
    }
}
