package org.fullguys.swing.adapters.web.controllers.admin.contagionAreas;

import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.fullguys.swing.common.annotations.WebAdapter;
import org.fullguys.swing.domain.usecases.synchro.ports.in.services.LoadContagionAreasDataService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

@WebAdapter
@CrossOrigin
@RestController
@RequiredArgsConstructor
@RequestMapping(value = "/contagionAreas")
public class LoadContagionAreasDataController {

    private final LoadContagionAreasDataService loadContagionAreasDataService;

    @ApiOperation("Register contagion Areas")
    @PostMapping(value = "", consumes = "multipart/form-data")
    public ResponseEntity<?> registerContagionAreas(@RequestBody MultipartFile contagionAreaFile){

        // TODO: Cambiar ID del admin por default
        loadContagionAreasDataService.loadContagionAreasData(contagionAreaFile,1L);

        return ResponseEntity.ok().build();
    }

}
