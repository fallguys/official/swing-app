package org.fullguys.swing.adapters.web.controllers.admin.incidents;

import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.fullguys.swing.common.annotations.WebAdapter;
import org.fullguys.swing.common.queries.Filters;
import org.fullguys.swing.common.queries.Paginator;
import org.fullguys.swing.config.parameter.TimerParameters;
import org.fullguys.swing.domain.usecases.incidents.ports.in.services.ShowIncidentService;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Date;
import java.util.List;

@WebAdapter
@CrossOrigin
@RestController
@RequiredArgsConstructor
@RequestMapping(value = "/incident")

public class ShowIncidentController {

    private final ShowIncidentService showIncidentService;
    private final IncidentDTOMapper incidentDTOMapper;
    private final TimerParameters timerParameters;

    @GetMapping("")
    @ApiOperation("Show all the incidents")
    public ResponseEntity<Paginator<ShowIncidentDTO>> showIncident(
            Pageable pageable,
            @RequestParam(value="search", defaultValue = "") String search,
            @RequestParam(required = false) Date initDate,
            @RequestParam(required = false) Date endDate){
        var filters = Filters.builder()
                .search(search)
                .pagesize(pageable.getPageSize())
                .page(pageable.getPageNumber())
                .build();
        if (initDate == null){
            filters.setInitDateRange(LocalDateTime.of(2020,01,01, 00, 00, 00));
        }
        else{
            filters.setInitDateRange(initDate.toInstant().atZone(ZoneId.of(timerParameters.getZone())).toLocalDateTime());
        }
        if (endDate == null){
            filters.setEndDateRange(LocalDateTime.now(ZoneId.of(timerParameters.getZone())));
        }
        else{
            filters.setEndDateRange(endDate.toInstant().atZone(ZoneId.of(timerParameters.getZone())).toLocalDateTime());
        }
        var rows = showIncidentService.showIncident(filters);
        var response = incidentDTOMapper.toDTOIncidents(rows.getData());

        return ResponseEntity.ok(new Paginator<>(response,
                rows.getPage(), rows.getPagesize(), rows.getTotal()));
    }
}
