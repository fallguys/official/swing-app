package org.fullguys.swing.adapters.web.controllers.admin.synchro;

import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.fullguys.swing.common.annotations.WebAdapter;
import org.fullguys.swing.domain.usecases.synchro.ports.in.services.ShowSynchroDetailService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@WebAdapter
@CrossOrigin
@RestController
@RequiredArgsConstructor
@RequestMapping(value = "/synchroDetail")
public class ShowSynchroDetailsController {
    private final ShowSynchroDetailService showSynchroDetailService;
    private final ShowSynchroDetailsMapper showSynchroDetailsMapper;

    @GetMapping("")
    @ApiOperation("Get synchro details for a given synchro id")
    public ResponseEntity<ShowSynchroDetailsDTO> showSynchroDetails(
            @RequestParam Long id
    ){
        var synchro = showSynchroDetailService.showSynchroDetail(id);
        var showSynchroDTO = showSynchroDetailsMapper.toShowSynchroDetailsDTO(synchro);
        return ResponseEntity.ok(showSynchroDTO);
    }
}
