package org.fullguys.swing.adapters.web.controllers.admin.schedules;

import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.fullguys.swing.adapters.persistence.views.adapters.InfoBeneficiaryAdapter;
import org.fullguys.swing.common.annotations.WebAdapter;

import java.util.List;
import org.fullguys.swing.domain.usecases.scheduling.ports.in.services.ShowBeneficiariesInfoService;
import org.mapstruct.Context;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;

@WebAdapter
@CrossOrigin
@RestController
@RequiredArgsConstructor
@RequestMapping(value = "/schedule/beneficiaries/export")
public class ExportBeneficiariesInfoController {
    private final InfoBeneficiaryAdapter infoBeneficiaryAdapter;
    private final ShowBeneficiariesInfoMapper showBeneficiariesInfoMapper;

    @GetMapping("")
    @ApiOperation("Export the beneficiaries info for a schedule")
    public ResponseEntity<List<ShowBeneficiariesInfoDTO>> exportBeneficiariesInfo(
            @Context HttpServletResponse response,
            @ModelAttribute ShowBeneficiariesInfoSearch showBeneficiariesInfoSearch,
            @RequestParam String type
    ){
        var rows = infoBeneficiaryAdapter.showAllInfoBeneficiaries(showBeneficiariesInfoSearch);
        var showBeneficiariesDTO = showBeneficiariesInfoMapper.toShowBeneficiariesInfoDTOList(rows);
        try{
            infoBeneficiaryAdapter.exportBeneficiaries(showBeneficiariesDTO, response.getOutputStream(), type);
        }
        catch(Exception e){
            return null;
        }

        return ResponseEntity.ok(showBeneficiariesDTO);
    }
}
