package org.fullguys.swing.adapters.web.controllers.admin.schedules;

import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.fullguys.swing.adapters.persistence.views.adapters.InfoWithdrawalPointAdapter;
import org.fullguys.swing.common.annotations.WebAdapter;
import org.fullguys.swing.common.queries.Filters;
import org.fullguys.swing.common.queries.Paginator;
import org.fullguys.swing.domain.usecases.scheduling.ports.in.services.ShowTotalAssignedService;
import org.fullguys.swing.domain.usecases.scheduling.ports.in.services.ShowWithdrawalPointInfoService;
import org.fullguys.swing.domain.usecases.scheduling.ports.out.scheduler.ShowTotalAssignedPort;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@WebAdapter
@CrossOrigin
@RestController
@RequiredArgsConstructor
@RequestMapping(value = "/schedules/withdrawalPoint")
public class ShowWithdrawalPointForScheduleController {
    private final ShowWithdrawalPointInfoService showWithdrawalPointInfoService;
    private final ShowTotalAssignedService showTotalAssignedService;
    private final ShowWithdrawalPointMapper showWithdrawalPointMapper;
    private final InfoWithdrawalPointAdapter infoWithdrawalPointAdapter;

    @GetMapping("")
    @ApiOperation("Show Withdrawal Points for a given scheduler")
    public ResponseEntity<Paginator<ShowWithdrawalPointDTO>> showWithdrawalPointsForGivenSchedule(
            Pageable pageable,
            @ModelAttribute ShowWithdrawalPointInfoSearch showWithdrawalPointInfoSearch
    ){
        var rows = infoWithdrawalPointAdapter.showInfoBeneficiaries(showWithdrawalPointInfoSearch, pageable);
        var showWithdrawalPointDTO = showWithdrawalPointMapper.toShowWithdrawalPointDto(rows.getData());
        return ResponseEntity.ok(new Paginator<>(showWithdrawalPointDTO,
                rows.getPage(),
                rows.getPagesize(),
                rows.getTotal()));
    }

}
