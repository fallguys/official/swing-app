package org.fullguys.swing.adapters.web.controllers.admin.schedules;

import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.fullguys.swing.common.annotations.WebAdapter;
import org.fullguys.swing.domain.usecases.scheduling.info.InfoBeforeGenerateSchedule;
import org.fullguys.swing.domain.usecases.scheduling.ports.in.services.CheckForInfoBeforeGenerateScheduleService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@WebAdapter
@CrossOrigin
@RestController
@RequiredArgsConstructor
@RequestMapping(value = "/schedules/checking")
public class CheckForInfoBeforeGenerateScheduleController {

    private final CheckForInfoBeforeGenerateScheduleService checkForInfoBeforeGenerateScheduleService;

    @GetMapping("")
    @ApiOperation("Get info to know if it's possible to generate a schedule")
    public ResponseEntity<InfoBeforeGenerateSchedule> checkForInfoBeforeGenerateSchedule() {
        var info = checkForInfoBeforeGenerateScheduleService.checkForInfoBeforeGenerateSchedule();
        return ResponseEntity.ok(info);
    }
}
