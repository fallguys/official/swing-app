package org.fullguys.swing.adapters.web.controllers.admin.infoBonus;

import io.swagger.annotations.ApiOperation;
import io.swagger.models.Response;
import lombok.RequiredArgsConstructor;
import org.fullguys.swing.common.annotations.WebAdapter;
import org.fullguys.swing.domain.entities.Assignment;
import org.fullguys.swing.domain.usecases.beneficiaries.ports.in.services.ShowInfoBonusService;
import org.springframework.http.ResponseEntity;
import org.springframework.util.NumberUtils;
import org.springframework.web.bind.annotation.*;

import javax.swing.text.DateFormatter;
import javax.swing.text.html.parser.Entity;
import java.awt.print.Pageable;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.NoSuchElementException;

@WebAdapter
@CrossOrigin
@RestController
@RequiredArgsConstructor
@RequestMapping(value = "/infoBonus")
public class ShowInfoBonusController {
    private final ShowInfoBonusService showInfoBonusService;
    private final ShowInfoBonusDTOMapper showInfoBonusDTOMapper;

    @GetMapping("")
    @ApiOperation("Show the information about the assignment of the bonus")
    public ResponseEntity<ShowInfoBonusDTO> showInfoBonus(
            @RequestParam(value="code", defaultValue = "") String search){
        try{
            Integer.parseInt(search);
            var showInfoBonus = showInfoBonusService.showInfoBonus(search);
            var showInfoBonusDTO = showInfoBonusDTOMapper.toShowInfoBonusDto(showInfoBonus);
            showInfoBonusDTO.setMessage("Ok");
            showInfoBonusDTO.setFound(1l);
            return ResponseEntity.ok(showInfoBonusDTO);
        }catch(NumberFormatException e){
            ShowInfoBonusDTO showInfoBonusDTO = new ShowInfoBonusDTO();
            showInfoBonusDTO.setFound(0l);
            showInfoBonusDTO.setMessage("Mal formato");
            return ResponseEntity.ok(showInfoBonusDTO);
        }catch(Exception e){
            ShowInfoBonusDTO showInfoBonusDTO = new ShowInfoBonusDTO();
            showInfoBonusDTO.setFound(0l);
            showInfoBonusDTO.setMessage("No se encontro el código de beneficiario en el padrón activo");
            return ResponseEntity.ok(showInfoBonusDTO);
        }
    }
}
