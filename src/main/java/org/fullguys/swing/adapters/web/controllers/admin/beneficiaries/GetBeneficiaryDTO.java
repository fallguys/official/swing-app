package org.fullguys.swing.adapters.web.controllers.admin.beneficiaries;

import lombok.Data;
import org.fullguys.swing.domain.entities.enums.BeneficiaryState;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

@Data
public class GetBeneficiaryDTO {
    private Long id;
    private String code;
    private String department;
    private String province;
    private String district;
    private String ubigee;
    private Date registerDate;
    private Date updatedDate;
    private BeneficiaryState state;

}
