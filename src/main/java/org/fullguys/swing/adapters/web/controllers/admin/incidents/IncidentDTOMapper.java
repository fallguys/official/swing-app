package org.fullguys.swing.adapters.web.controllers.admin.incidents;

import org.fullguys.swing.domain.entities.Incident;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;

import java.util.List;

@Mapper(componentModel="spring")
public interface IncidentDTOMapper {

    @Mappings({
            @Mapping(source = "message", target = "message"),
            @Mapping(source = "incidentTypeId", target = "type.id"),
            @Mapping(target = "reportedDate", ignore = true),
            @Mapping(target = "id", ignore = true),
            @Mapping(target = "type", ignore = true)
    })
    Incident toRegisterIncident(RegisterIncidentDTO incidentDTO);

    @Mappings({
            @Mapping(source = "message", target = "message"),
            @Mapping(source = "reportedDate", target = "date"),
            @Mapping(source = "beneficiary.code", target = "id"),
            @Mapping(source = "type.name", target = "type")
    })
    ShowIncidentDTO toDTOIncident(Incident incident);
    List<ShowIncidentDTO> toDTOIncidents(List<Incident> incident);
}
