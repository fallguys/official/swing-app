package org.fullguys.swing.adapters.web.controllers.admin.schedules;

import org.fullguys.swing.domain.entities.Schedule;
import org.mapstruct.*;

import java.util.List;

@Mapper(componentModel="spring")
public interface GetSchedulesMapper {

    @Mappings({
            @Mapping(source = "id", target = "id"),
            @Mapping(source = "type", target = "type"),
            @Mapping(source = "state", target = "state"),
            @Mapping(source = "creationDate", target = "creationDate"),
            @Mapping(source = "initialDate", target = "initialDate"),
            @Mapping(source = "finalDate", target = "finalDate"),
            @Mapping(target = "responsible", ignore = true)
    })
    GetScheduleDTO toGetScheduleDTO(Schedule schedule);

    List<GetScheduleDTO> toGetSchedulesDTO(List<Schedule> schedules);

    @AfterMapping
    default void setResponsible(Schedule schedule, @MappingTarget GetScheduleDTO scheduleDTO) {
        var admin = schedule.getAdministrator();
        var responsible = String.format("%s %s %s", admin.getNames(), admin.getFatherLastname(), admin.getMotherLastname());
        scheduleDTO.setResponsible(responsible);
    }
}
