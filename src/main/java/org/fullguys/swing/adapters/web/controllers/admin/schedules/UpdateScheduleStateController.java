package org.fullguys.swing.adapters.web.controllers.admin.schedules;

import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.fullguys.swing.common.annotations.WebAdapter;
import org.fullguys.swing.domain.usecases.scheduling.ports.in.services.UpdateScheduleStateService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@WebAdapter
@CrossOrigin
@RestController
@RequiredArgsConstructor
@RequestMapping(value = "/schedules")
public class UpdateScheduleStateController {

    private final UpdateScheduleStateService updateScheduleStateService;
    private final UpdateScheduleStateMapper updateScheduleStateMapper;

    @PutMapping("")
    @ApiOperation("Update state for schedule")
    public ResponseEntity<?> updateScheduleState(@RequestBody UpdateScheduleStateDTO updateScheduleStateDTO){
        var schedule = updateScheduleStateMapper.toSchedule(updateScheduleStateDTO);
        updateScheduleStateService.updateScheduleState(schedule);
        return ResponseEntity.ok().build();
    }

}
