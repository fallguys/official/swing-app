package org.fullguys.swing.adapters.web.controllers.admin.contagionAreas;

import lombok.Data;

import java.util.Date;

@Data
public class GetContagionAreaDTO {
    private Long id;
    private String department;
    private String province;
    private String district;
    private String ubigee;
    private String quantity;
    private Date registerDate;
    private Date updatedDate;
}
