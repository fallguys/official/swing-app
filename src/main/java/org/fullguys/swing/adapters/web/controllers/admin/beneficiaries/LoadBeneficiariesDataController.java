package org.fullguys.swing.adapters.web.controllers.admin.beneficiaries;

import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.fullguys.swing.common.annotations.WebAdapter;
import org.fullguys.swing.domain.entities.Beneficiary;
import org.fullguys.swing.domain.usecases.synchro.ports.in.services.LoadBeneficiariesDataService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;
import java.util.List;

@WebAdapter
@CrossOrigin
@RestController
@RequiredArgsConstructor
@RequestMapping(value = "/beneficiaries")
public class LoadBeneficiariesDataController {

    private final LoadBeneficiariesDataService loadBeneficiariesDataService;

    @ApiOperation("Register beneficiaries")
    @PostMapping(value = "", consumes = "multipart/form-data")
    public ResponseEntity<?> registerBeneficiaries(@RequestParam("file") MultipartFile beneficiariesFile) throws IOException {

        // TODO: Cambiar ID del admin por default
        loadBeneficiariesDataService.loadBeneficiariesDataService(beneficiariesFile,1L);

        return ResponseEntity.ok().build();
    }

}
