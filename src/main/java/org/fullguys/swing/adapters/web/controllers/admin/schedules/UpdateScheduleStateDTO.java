package org.fullguys.swing.adapters.web.controllers.admin.schedules;

import lombok.Data;
import org.fullguys.swing.domain.entities.enums.ScheduleState;

@Data
public class UpdateScheduleStateDTO {
    private Long idSchedule;
    private ScheduleState state;
}
