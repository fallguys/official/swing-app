package org.fullguys.swing.adapters.web.controllers.sim;

import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.fullguys.swing.common.annotations.WebAdapter;
import org.fullguys.swing.domain.usecases.feedback.ports.in.services.SaveInfoBonusService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@WebAdapter
@CrossOrigin
@RestController
@RequiredArgsConstructor
@RequestMapping(value = "/sim/participants/beneficiaries")
public class SaveInfoBonusController {

    private final SaveInfoBonusService saveInfoBonusService;

    @PostMapping("/{codeBeneficiary}/bonus")
    @ApiOperation("Save Info Bonus")
    public ResponseEntity<?> saveInfoBonus(@PathVariable String codeBeneficiary, @RequestParam String codePoint){

        saveInfoBonusService.saveInfoBonus(codeBeneficiary, codePoint);

        return ResponseEntity.ok().build();

    }

}
