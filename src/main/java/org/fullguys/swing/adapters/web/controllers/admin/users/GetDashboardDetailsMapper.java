package org.fullguys.swing.adapters.web.controllers.admin.users;

import org.fullguys.swing.adapters.web.controllers.admin.synchro.ShowSynchroDetailsDTO;
import org.fullguys.swing.domain.entities.Schedule;
import org.fullguys.swing.domain.entities.Synchro;
import org.fullguys.swing.domain.entities.enums.ScheduleState;
import org.fullguys.swing.domain.entities.enums.SynchroState;
import org.mapstruct.*;

import java.util.Optional;
import java.util.List;

@Mapper(componentModel="spring")
public interface GetDashboardDetailsMapper {

    @Named("getDasbhoardDetailsDTO")
    default GetDashboardDetailsDTO getDashboardDetailsDTO(List<Optional<Synchro>> activeSynchro,
                                        Optional<Schedule> activeSchedule){
        GetDashboardDetailsDTO getDashboardDetailsDTO = new GetDashboardDetailsDTO();

        if(activeSynchro.get(0).isPresent() &&
                ((activeSynchro.get(0).get().getState().equals(SynchroState.COMPLETED)) ||
                activeSynchro.get(0).get().getState().equals(SynchroState.IN_PROCESS)))
            getDashboardDetailsDTO.setBeneficiarySynchroActive(true);
        else
            getDashboardDetailsDTO.setBeneficiarySynchroActive(false);

        if(activeSynchro.get(1).isPresent() &&
                ((activeSynchro.get(1).get().getState().equals(SynchroState.COMPLETED)) ||
                        activeSynchro.get(1).get().getState().equals(SynchroState.IN_PROCESS)))
            getDashboardDetailsDTO.setContagionAreaSynchroActive(true);
        else
            getDashboardDetailsDTO.setContagionAreaSynchroActive(false);

        if(activeSynchro.get(2).isPresent() &&
                ((activeSynchro.get(2).get().getState().equals(SynchroState.COMPLETED)) ||
                        activeSynchro.get(2).get().getState().equals(SynchroState.IN_PROCESS)))
            getDashboardDetailsDTO.setWithdrawalPointSynchroActive(true);
        else
            getDashboardDetailsDTO.setWithdrawalPointSynchroActive(false);

        getDashboardDetailsDTO.setScheduleActive(activeSchedule.isPresent());
        return getDashboardDetailsDTO;
    }
}
