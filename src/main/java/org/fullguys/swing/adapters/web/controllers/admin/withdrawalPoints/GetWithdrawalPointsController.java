package org.fullguys.swing.adapters.web.controllers.admin.withdrawalPoints;

import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.fullguys.swing.common.annotations.WebAdapter;
import org.fullguys.swing.common.queries.Filters;
import org.fullguys.swing.common.queries.Paginator;
import org.fullguys.swing.domain.entities.WithdrawalPoint;
import org.fullguys.swing.domain.usecases.synchro.ports.in.services.GetWithdrawalPointsService;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@WebAdapter
@CrossOrigin
@RestController
@RequiredArgsConstructor
@RequestMapping(value = "/withdrawalPoints")
public class GetWithdrawalPointsController {

    private final GetWithdrawalPointsService getWithdrawalPointsService;
    private final WithdrawalPointsDTOMapper withdrawalPointsDTOMapper;

    @GetMapping("")
    @ApiOperation("Get Withdrawal Points")
    public ResponseEntity<CommandGetPointDTO> getWithdrawalPoints(
            Pageable pageable,
            @RequestParam(defaultValue = "") String search){

        var filters = Filters.builder()
                .page(pageable.getPageNumber())
                .pagesize(pageable.getPageSize())
                .search(search)
                .build();

        var pointCommand = getWithdrawalPointsService.getWithdrawalPoints(filters);
        var pagination = pointCommand.getPaginator();
        var data = withdrawalPointsDTOMapper.toDTOWithdrawalPoints(pagination.getData());
        var res = new Paginator<>(data, pagination.getPage(), pagination.getPagesize(), pagination.getTotal());

        var commandDTO = CommandGetPointDTO.builder()
                .pointsPaginator(res)
                .lastUpdated(pointCommand.getLastUpdated())
                .totalActive(pointCommand.getTotalActive())
                .build();

        return ResponseEntity.ok(commandDTO);
    }
}
