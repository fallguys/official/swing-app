package org.fullguys.swing.adapters.web.controllers.sim;

import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.fullguys.swing.common.annotations.WebAdapter;
import org.fullguys.swing.domain.usecases.feedback.ports.in.services.GetParticipantWithdrawalPointService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;


@WebAdapter
@CrossOrigin
@RestController
@RequiredArgsConstructor
@RequestMapping(value = "/sim/participants/withdrawalPoints")
public class GetParticipantWithdrawalPointController {

    private final GetParticipantWithdrawalPointService getParticipantWithdrawalPointService;
    private final ParticipantPointDTOMapper participantPointDTOMapper;

    @GetMapping("")
    @ApiOperation("Get a participant withdrawal point")
    public ResponseEntity<GetParticipantPointDTO> getParticipantWithdrawalPoint(@RequestParam String codeWithdrawalPoint){
        var withdrawalPoint = getParticipantWithdrawalPointService.getParticipantWithdrawalPoint(codeWithdrawalPoint);
        var participantPointDTO = participantPointDTOMapper.toDTOParticipantPoint(withdrawalPoint);
        return ResponseEntity.ok(participantPointDTO);
    }
}
