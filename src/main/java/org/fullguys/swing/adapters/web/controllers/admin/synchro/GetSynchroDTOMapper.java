package org.fullguys.swing.adapters.web.controllers.admin.synchro;

import org.fullguys.swing.domain.entities.Synchro;
import org.mapstruct.*;

import java.util.List;

@Mapper(componentModel="spring")
public interface GetSynchroDTOMapper {

    @Mappings({
            @Mapping(source = "id", target = "id"),
            @Mapping(source = "fileType", target = "fileType"),
            @Mapping(source = "result.totalRegistries", target = "successfullRegistries"),
            @Mapping(source = "initialDate", target = "date"),
            @Mapping(source = "state", target = "state"),
            @Mapping(target = "responsible", ignore = true)
    })
    GetSynchroDTO toDTOSynchro(Synchro synchro);
    List<GetSynchroDTO> toDTOSynchros(List<Synchro> synchros);

    @AfterMapping
    default void setResponsible(Synchro synchro, @MappingTarget GetSynchroDTO getSynchroDTO){
        var admin = synchro.getAdministrator();
        var responsible = String.format("%s %s", admin.getNames(), admin.getFatherLastname());
        getSynchroDTO.setResponsible(responsible);
    }

}
