package org.fullguys.swing.adapters.web.controllers.admin.schedules;

import lombok.Data;
import org.fullguys.swing.domain.entities.enums.ScheduleType;

import java.time.LocalDate;

@Data
public class RegisterNewScheduleDTO {
    private ScheduleType scheduleType;
    private LocalDate initialDate;
    private Long adminId;
}
