package org.fullguys.swing.adapters.web.controllers.admin.schedules;

import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.fullguys.swing.adapters.persistence.views.adapters.InfoBeneficiaryAdapter;
import org.fullguys.swing.adapters.persistence.views.models.InfoBeneficiaryView;
import org.fullguys.swing.common.annotations.WebAdapter;
import org.fullguys.swing.common.queries.Paginator;
import org.fullguys.swing.domain.usecases.scheduling.ports.in.services.ShowBeneficiariesInfoService;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDate;

@WebAdapter
@CrossOrigin
@RestController
@RequiredArgsConstructor
@RequestMapping(value = "/schedule/beneficiaries")
public class ShowBeneficiariesInfoController {
    private final ShowBeneficiariesInfoService showBeneficiariesInfoService;
    private final InfoBeneficiaryAdapter infoBeneficiaryAdapter;
    private final ShowBeneficiariesInfoMapper showBeneficiariesInfoMapper;

    @GetMapping("")
    @ApiOperation("Get all the beneficiaries info for a schedule")
    public ResponseEntity<Paginator<ShowBeneficiariesInfoDTO>> getSchedules(
            Pageable pageable,
            @ModelAttribute ShowBeneficiariesInfoSearch showBeneficiariesInfoSearch
    ){
        var rows = infoBeneficiaryAdapter.showInfoBeneficiaries(showBeneficiariesInfoSearch, pageable);
        var showBeneficiariesDTO = showBeneficiariesInfoMapper.toShowBeneficiariesInfoDTOList(rows.getData());
        return ResponseEntity.ok(
                new Paginator<>(showBeneficiariesDTO,
                        rows.getPage(),
                        rows.getPagesize(), rows.getTotal()));
    }
}
