package org.fullguys.swing.adapters.web.controllers.admin.beneficiaries;

import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.fullguys.swing.common.annotations.WebAdapter;
import org.fullguys.swing.common.queries.Filters;
import org.fullguys.swing.common.queries.Paginator;
import org.fullguys.swing.domain.usecases.beneficiaries.ports.in.services.ShowWithdrawalPointService;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;


@WebAdapter
@CrossOrigin
@RestController
@RequiredArgsConstructor
@RequestMapping(value = "/withdrawalPointUbigee")

public class ShowWithdrawalPointsController {
    private final ShowWithdrawalPointService showWithdrawalPointService;
    private final ShowWithdrawalPointDTOMapper showWithdrawalPointDTOMapper;

    @GetMapping("")
    @ApiOperation("Show the withdrawal points")
    public ResponseEntity<Paginator<ShowWithdrawalPointsDTO>> showWithdrawalPoints(
            Pageable pageable,
            @RequestParam(value = "search") String search
    ){
        var filters = Filters.builder()
                .page(pageable.getPageNumber())
                .pagesize(pageable.getPageSize())
                .search(search)
                .build();
        var rows = showWithdrawalPointService.showWithdrawalPoint(filters);
        var response = showWithdrawalPointDTOMapper.toShowWithdrawalPointsDTOs(rows.getData());
        return ResponseEntity.ok(new Paginator<>(response, rows.getPage(), rows.getPagesize(), rows.getTotal()));
    }
}
