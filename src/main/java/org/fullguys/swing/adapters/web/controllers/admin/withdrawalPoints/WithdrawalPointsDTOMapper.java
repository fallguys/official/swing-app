package org.fullguys.swing.adapters.web.controllers.admin.withdrawalPoints;

import org.fullguys.swing.adapters.web.controllers.admin.beneficiaries.GetBeneficiaryDTO;
import org.fullguys.swing.domain.entities.Beneficiary;
import org.fullguys.swing.domain.entities.WithdrawalPoint;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;

import java.util.List;

@Mapper(componentModel="spring")
public interface WithdrawalPointsDTOMapper {

    @Mappings({
            @Mapping(source = "id", target = "id"),
            @Mapping(source = "code", target = "code"),
            @Mapping(source = "name", target = "agency"),
            @Mapping(source = "district.fatherUbigee.fatherUbigee.name", target = "department"),
            @Mapping(source = "district.fatherUbigee.name", target = "province"),
            @Mapping(source = "district.name", target = "district"),
            @Mapping(source = "district.code", target = "ubigee"),
            @Mapping(source = "address", target = "address"),
            @Mapping(source = "weekdayHourStart", target = "weekdayHourStart"),
            @Mapping(source = "weekdayHourEnd", target = "weekdayHourEnd"),
            @Mapping(source = "weekendHourStart", target = "weekendHourStart"),
            @Mapping(source = "weekendHourEnd", target = "weekendHourEnd"),
            @Mapping(source = "createdAt", target = "registerDate"),
            @Mapping(source = "updatedAt", target = "updatedDate"),
            @Mapping(source = "state", target = "state")
    })
    GetWithdrawalPointsDTO toDTOWithdrawalPoint(WithdrawalPoint withdrawalPoint);
    List<GetWithdrawalPointsDTO> toDTOWithdrawalPoints(List<WithdrawalPoint> withdrawalPoints);

}
