package org.fullguys.swing.adapters.web.controllers.admin.withdrawalPoints;

import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.fullguys.swing.common.annotations.WebAdapter;
import org.fullguys.swing.domain.usecases.synchro.ports.in.services.LoadWithdrawalPointsDataService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

@WebAdapter
@CrossOrigin
@RestController
@RequiredArgsConstructor
@RequestMapping(value = "/withdrawalPoints")
public class LoadWithdrawalPointsDataController {

    private final LoadWithdrawalPointsDataService loadWithdrawalPointsDataService;

    @ApiOperation("Register withdrawal Points")
    @PostMapping(value = "", consumes = "multipart/form-data")
    public ResponseEntity<?> registerWithdrawalPoints(@RequestParam("file") MultipartFile withdrawalPointFile) {

        // TODO: Cambiar ID del admin por default
        loadWithdrawalPointsDataService.loadWithdrawalPointsData(withdrawalPointFile,1L);

        return ResponseEntity.ok().build();
    }
}
