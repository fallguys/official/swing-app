package org.fullguys.swing.adapters.web.controllers.admin.schedules;

import lombok.Data;

import java.time.LocalDate;
import java.time.LocalDateTime;

@Data
public class ShowScheduleDetailsDTO {
    private String adminName;
    private LocalDate initialDate;
    private LocalDate finalDate;
    private LocalDateTime creationDate;
    private Integer beneficiaries;
    private Integer withdrawalPoints;
    private Integer timeLeft;
    private String state;
}
