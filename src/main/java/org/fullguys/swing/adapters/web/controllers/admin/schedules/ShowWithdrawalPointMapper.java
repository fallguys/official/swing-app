package org.fullguys.swing.adapters.web.controllers.admin.schedules;

import org.fullguys.swing.adapters.persistence.views.models.InfoWithdrawalPointView;
import org.fullguys.swing.domain.entities.WithdrawalPoint;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import java.util.List;

@Mapper(componentModel="spring")
public interface ShowWithdrawalPointMapper {
    @Mapping(source = "withdrawalPointCode", target = "id")
    @Mapping(source = "address", target = "address")
    @Mapping(source = "ubigee", target = "ubigee")
    @Mapping(source = "assigned", target = "cantAssigned")
    @Mapping(source = "attended", target = "cantAttended")
    ShowWithdrawalPointDTO toShowWithdrawalPointDto(InfoWithdrawalPointView withdrawalPoint);
    List<ShowWithdrawalPointDTO> toShowWithdrawalPointDto(List<InfoWithdrawalPointView> withdrawalPoint);
}
