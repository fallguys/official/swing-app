package org.fullguys.swing.adapters.web.controllers.admin.schedules;

import io.swagger.models.auth.In;
import org.fullguys.swing.adapters.persistence.views.models.InfoBeneficiaryView;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import java.util.List;

@Mapper(componentModel="spring", uses = {InfoBeneficiaryView.class})
public interface ShowBeneficiariesInfoMapper {
    @Mapping(source = "beneficiaryCode", target ="id")
    @Mapping(source = "withdrawalPointName", target ="withdrawalPoint")
    @Mapping(source = "initialDateAssigned", target ="initialDate")
    @Mapping(source = "finalDateAssigned", target ="finalDate")
    @Mapping(source = "bonusRetired", target ="bonusRetired")
    @Mapping(source = "bonusRetiredDate", target = "bonusRetiredDate")
    @Mapping(source = "mistakes", target = "mistakes")
    ShowBeneficiariesInfoDTO toShowBeneficiariesInfoDTO(InfoBeneficiaryView infoBeneficiaryView);
    List<ShowBeneficiariesInfoDTO> toShowBeneficiariesInfoDTOList(List<InfoBeneficiaryView> infoBeneficiaryViews);
}
