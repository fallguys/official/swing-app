package org.fullguys.swing.adapters.web.controllers.admin.beneficiaries;

import org.fullguys.swing.domain.entities.WithdrawalPoint;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;

import java.util.List;

@Mapper(componentModel="spring")
public interface ShowWithdrawalPointDTOMapper {
    @Mappings({
            @Mapping(source = "code", target = "code"),
            @Mapping(source = "name", target = "name"),
            @Mapping(source = "address", target = "address")
    })
    List<ShowWithdrawalPointsDTO> toShowWithdrawalPointsDTOs(List<WithdrawalPoint> withdrawalPoint);
}
