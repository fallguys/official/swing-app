package org.fullguys.swing.adapters.web.controllers.admin.schedules;

import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.fullguys.swing.common.annotations.WebAdapter;
import org.fullguys.swing.domain.usecases.scheduling.ports.in.services.GenerateNewScheduleService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@WebAdapter
@CrossOrigin
@RestController
@RequiredArgsConstructor
@RequestMapping(value = "/schedules")
public class RegisterNewScheduleController {

    private final GenerateNewScheduleService generateNewScheduleService;
    private final RegisterNewScheduleMapper registerNewScheduleMapper;

    @PostMapping("")
    @ApiOperation("Register new schedule. It will be validated according to type (official or test).")
    public ResponseEntity<?> registerNewSchedule(@RequestBody RegisterNewScheduleDTO registerNewScheduleDTO) {
        var schedule = registerNewScheduleMapper.toSchedule(registerNewScheduleDTO);
        generateNewScheduleService.generate(schedule);
        return ResponseEntity.ok().build();
    }
}
