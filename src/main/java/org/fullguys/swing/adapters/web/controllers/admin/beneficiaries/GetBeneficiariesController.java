package org.fullguys.swing.adapters.web.controllers.admin.beneficiaries;

import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.fullguys.swing.common.annotations.WebAdapter;
import org.fullguys.swing.common.queries.Filters;
import org.fullguys.swing.common.queries.Paginator;
import org.fullguys.swing.domain.usecases.synchro.ports.in.services.GetBeneficiariesService;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;


@WebAdapter
@CrossOrigin
@RestController
@RequiredArgsConstructor
@RequestMapping(value = "/beneficiaries")
public class GetBeneficiariesController {

    private final GetBeneficiariesService getBeneficiariesService;
    private final BeneficiariesDTOMapper beneficiariesDTOMapper;

    @GetMapping("")
    @ApiOperation("Get beneficiaries")
    public ResponseEntity<CommandGetBeneficiaryDTO> getBeneficiaries(
            Pageable pageable,
            @RequestParam(defaultValue = "") String search) {

        var filters = Filters.builder()
                .page(pageable.getPageNumber())
                .pagesize(pageable.getPageSize())
                .search(search)
                .build();

        var beneficiaryCommand = getBeneficiariesService.getBeneficiaries(filters);
        var pagination = beneficiaryCommand.getPaginator();
        var data = beneficiariesDTOMapper.toDTOBeneficiaries(pagination.getData());
        var res = new Paginator<>(data, pagination.getPage(), pagination.getPagesize(), pagination.getTotal());

        var commandDTO = CommandGetBeneficiaryDTO.builder()
                .beneficiariesPaginator(res)
                .lastUpdated(beneficiaryCommand.getLastUpdated())
                .totalActive(beneficiaryCommand.getTotalActive())
                .build();

        return ResponseEntity.ok(commandDTO);
    }

}
