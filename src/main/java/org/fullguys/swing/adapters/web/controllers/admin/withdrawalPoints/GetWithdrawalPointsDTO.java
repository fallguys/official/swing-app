package org.fullguys.swing.adapters.web.controllers.admin.withdrawalPoints;

import lombok.Data;
import org.fullguys.swing.domain.entities.enums.BeneficiaryState;

import java.time.LocalTime;
import java.util.Date;

@Data
public class GetWithdrawalPointsDTO {
    private Long id;
    private String code;
    private String agency;
    private String department;
    private String province;
    private String district;
    private String ubigee;
    private String address;
    private LocalTime weekdayHourStart;
    private LocalTime weekdayHourEnd;
    private LocalTime weekendHourStart;
    private LocalTime weekendHourEnd;
    private Date registerDate;
    private Date updatedDate;
    private BeneficiaryState state;
}
