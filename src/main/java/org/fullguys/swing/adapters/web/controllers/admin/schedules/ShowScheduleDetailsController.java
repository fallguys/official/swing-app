package org.fullguys.swing.adapters.web.controllers.admin.schedules;

import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.fullguys.swing.common.annotations.WebAdapter;
import org.fullguys.swing.domain.entities.Schedule;
import org.fullguys.swing.domain.entities.enums.ScheduleState;
import org.fullguys.swing.domain.usecases.scheduling.ports.in.services.ShowScheduleDetailsService;
import org.fullguys.swing.domain.usecases.scheduling.ports.in.services.ShowTotalBeneficiariesForScheduleService;
import org.fullguys.swing.domain.usecases.scheduling.ports.in.services.ShowTotalWithdrawalPointsForScheduleService;
import org.fullguys.swing.domain.usecases.scheduling.ports.out.scheduler.ShowTotalBeneficiariesForSchedulePort;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.time.Period;

@WebAdapter
@CrossOrigin
@RestController
@RequiredArgsConstructor
@RequestMapping(value = "/schedule_details")
public class ShowScheduleDetailsController {

    private final ShowScheduleDetailsService showScheduleDetailsService;
    private final ShowTotalBeneficiariesForScheduleService showTotalBeneficiariesForScheduleService;
    private final ShowTotalWithdrawalPointsForScheduleService showTotalWithdrawalPointsForScheduleService;
    private final ShowScheduleDetailsMapper showScheduleDetailsMapper;

    @GetMapping("")
    @ApiOperation("Get schedule details for a given schedule id")
    public ResponseEntity<ShowScheduleDetailsDTO> showScheduleDetailsDTO(
            @RequestParam(defaultValue = "") Long scheduleId){
        try{
            var schedule = showScheduleDetailsService.showScheduleDetails(scheduleId);
            var scheduleDTO = showScheduleDetailsMapper.toShowScheduleDetailsDTO(schedule);
            scheduleDTO.setAdminName(schedule.getAdministrator().getNames()+" "+schedule.getAdministrator().getFatherLastname()+" "+schedule.getAdministrator().getMotherLastname());
            scheduleDTO.setState(schedule.getState().toString());
            scheduleDTO.setBeneficiaries(showTotalBeneficiariesForScheduleService.showTotalBeneficiariesForSchedule(scheduleId));
            scheduleDTO.setWithdrawalPoints(showTotalWithdrawalPointsForScheduleService.showTotalWithdrawalPointsForSchedule(scheduleId));
            return ResponseEntity.ok(scheduleDTO);
        }
        catch (Exception e){
            return ResponseEntity.ok(new ShowScheduleDetailsDTO());
        }
    }
}
