package org.fullguys.swing.adapters.web.controllers.admin.schedules;

import org.fullguys.swing.domain.entities.Schedule;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel="spring")
public interface UpdateScheduleStateMapper {

    @Mapping(source = "state", target = "state")
    @Mapping(source = "idSchedule", target = "id")
    Schedule toSchedule(UpdateScheduleStateDTO updateScheduleStateDTO);

}
