package org.fullguys.swing.adapters.web.controllers.admin.schedules;

import org.fullguys.swing.domain.entities.Schedule;
import org.mapstruct.*;

@Mapper(componentModel="spring")
public interface RegisterNewScheduleMapper {

    @Mapping(source = "scheduleType", target = "type")
    @Mapping(source = "adminId", target = "administrator.id")
    @Mapping(source = "initialDate", target = "initialDate")
    @Mapping(target = "id", ignore = true)
    @Mapping(target = "state", ignore = true)
    @Mapping(target = "creationDate", ignore = true)
    @Mapping(target = "finalDate", ignore = true)
    @Mapping(target = "administrator", ignore = true)
    @Mapping(target = "assignments", ignore = true)
    Schedule toSchedule(RegisterNewScheduleDTO registerNewScheduleDTO);
}
