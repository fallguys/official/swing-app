package org.fullguys.swing.adapters.web.controllers.admin.synchro;

import lombok.Data;

import java.time.LocalDateTime;

@Data
public class ShowSynchroDetailsDTO {
    String type;
    String status;
    LocalDateTime initialDate;
    Integer cantFailed;
    Integer cantTotal;
    String sourceDataFile;
    String responsible;
}
