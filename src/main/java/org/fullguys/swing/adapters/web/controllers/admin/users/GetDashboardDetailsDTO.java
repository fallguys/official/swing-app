package org.fullguys.swing.adapters.web.controllers.admin.users;

import lombok.Data;

@Data
public class GetDashboardDetailsDTO {
    private boolean scheduleActive;
    private boolean beneficiarySynchroActive;
    private boolean contagionAreaSynchroActive;
    private boolean withdrawalPointSynchroActive;
}
