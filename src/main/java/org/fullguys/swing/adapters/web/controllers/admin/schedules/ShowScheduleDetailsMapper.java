package org.fullguys.swing.adapters.web.controllers.admin.schedules;

import org.fullguys.swing.domain.entities.Schedule;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import java.util.List;

@Mapper(componentModel="spring")
public interface ShowScheduleDetailsMapper {
    @Mapping(target = "adminName", ignore = true)
    @Mapping(source = "creationDate", target = "creationDate")
    @Mapping(source = "initialDate", target = "initialDate")
    @Mapping(source = "finalDate", target = "finalDate")
    @Mapping(target = "beneficiaries", ignore = true)
    @Mapping(target = "withdrawalPoints", ignore = true)
    @Mapping(target = "timeLeft", ignore = true)
    ShowScheduleDetailsDTO toShowScheduleDetailsDTO(Schedule schedule);
    List<ShowScheduleDetailsDTO> toShowScheduleDetailsDTOList(List<Schedule> schedule);
}
