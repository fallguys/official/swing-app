package org.fullguys.swing.adapters.web.controllers.admin.synchro;

import org.fullguys.swing.domain.entities.Synchro;
import org.mapstruct.AfterMapping;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;

@Mapper(componentModel="spring")
public interface ShowSynchroDetailsMapper {
    @Mapping(source = "initialDate", target = "initialDate")
    @Mapping(source = "result.totalRegistries", target = "cantTotal")
    @Mapping(source = "result.failedRegistries", target = "cantFailed")
    @Mapping(target = "type", ignore = true)
    @Mapping(target = "responsible", ignore = true)
    @Mapping(target = "status", ignore = true)
    @Mapping(source = "result.sourceDataFile", target = "sourceDataFile")

    ShowSynchroDetailsDTO toShowSynchroDetailsDTO(Synchro synchro);

    @AfterMapping
    default void setResponsibleAndTypeAndState(Synchro synchro, @MappingTarget ShowSynchroDetailsDTO showSynchroDetailsDTO){
        var admin = synchro.getAdministrator();
        var responsible = String.format("%s %s", admin.getNames(), admin.getFatherLastname());
        showSynchroDetailsDTO.setResponsible(responsible);
        showSynchroDetailsDTO.setType(synchro.getFileType().toString());
        showSynchroDetailsDTO.setStatus(synchro.getState().toString());
    }

}
