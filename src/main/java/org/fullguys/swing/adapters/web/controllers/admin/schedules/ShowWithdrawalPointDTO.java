package org.fullguys.swing.adapters.web.controllers.admin.schedules;

import lombok.Data;

@Data
public class ShowWithdrawalPointDTO {
    private String id;
    private String address;
    private String ubigee;
    private Integer cantAssigned;
    private Integer cantAttended;
    private String type;
}
