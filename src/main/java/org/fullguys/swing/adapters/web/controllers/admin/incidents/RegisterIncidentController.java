package org.fullguys.swing.adapters.web.controllers.admin.incidents;

import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.fullguys.swing.common.annotations.WebAdapter;
import org.fullguys.swing.domain.usecases.incidents.errors.BeneficiaryDoesntExistError;
import org.fullguys.swing.domain.usecases.incidents.ports.in.services.GetBeneficiaryByCodeService;
import org.fullguys.swing.domain.usecases.incidents.ports.in.services.RegisterIncidentService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@WebAdapter
@CrossOrigin
@RestController
@RequiredArgsConstructor
@RequestMapping(value = "/incident")
public class RegisterIncidentController {

    private final RegisterIncidentService registerIncidentService;
    private final GetBeneficiaryByCodeService getBeneficiaryByCodeService;
    private final IncidentDTOMapper incidentDTOMapper;

    @PostMapping("")
    @ApiOperation("Register an incident")
    public ResponseEntity<Long> registerIncident(@RequestBody RegisterIncidentDTO incidentDTO) {
        try{
            var incident = incidentDTOMapper.toRegisterIncident(incidentDTO);
            incident.setBeneficiary(getBeneficiaryByCodeService.getBeneficiaryByCode(incidentDTO.getCode()));
            var id = registerIncidentService.register(incident).getId();
            return ResponseEntity.ok(id);
        }catch (BeneficiaryDoesntExistError e){
            return ResponseEntity.ok(-1l);
        }
    }
}
