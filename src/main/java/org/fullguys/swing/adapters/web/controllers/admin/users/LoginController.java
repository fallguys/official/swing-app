package org.fullguys.swing.adapters.web.controllers.admin.users;

import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.fullguys.swing.common.annotations.WebAdapter;
import org.fullguys.swing.domain.usecases.user.ports.in.services.LoginService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@WebAdapter
@RestController
@RequiredArgsConstructor
@RequestMapping(value = "/users")
public class LoginController {
    private final LoginService loginService;

    @PostMapping("/login")
    @ApiOperation("Given right credentials, login to the system")
    public ResponseEntity<?> login(@RequestBody String email, @RequestBody String password) {
        loginService.login(email, password);
        return ResponseEntity.ok().build();
    }
}
