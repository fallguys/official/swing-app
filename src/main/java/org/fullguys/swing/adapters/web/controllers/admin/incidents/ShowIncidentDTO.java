package org.fullguys.swing.adapters.web.controllers.admin.incidents;

import lombok.Data;

import java.time.LocalDateTime;

@Data
public class ShowIncidentDTO {
    private String id;
    private String message;
    private String type;
    private LocalDateTime date;
}

