package org.fullguys.swing.adapters.web.controllers.sim;

import lombok.Data;

@Data
public class GetParticipantPointDTO {
    private Long id;
    private String code;
    private String name;
    private String address;
}
