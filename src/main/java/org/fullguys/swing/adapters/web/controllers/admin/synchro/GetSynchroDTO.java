package org.fullguys.swing.adapters.web.controllers.admin.synchro;

import lombok.Data;
import org.fullguys.swing.domain.entities.enums.SynchroFileType;

import java.time.LocalDateTime;

@Data
public class GetSynchroDTO {
    private Long id;
    private String responsible;
    private String fileType;
    private Long successfullRegistries;
    private LocalDateTime date;
    private String state;
}
