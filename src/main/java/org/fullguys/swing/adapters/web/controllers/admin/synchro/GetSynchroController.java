package org.fullguys.swing.adapters.web.controllers.admin.synchro;


import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.fullguys.swing.common.annotations.WebAdapter;
import org.fullguys.swing.common.queries.Filters;
import org.fullguys.swing.common.queries.Paginator;
import org.fullguys.swing.domain.entities.Synchro;
import org.fullguys.swing.domain.entities.enums.ScheduleType;
import org.fullguys.swing.domain.usecases.synchro.ports.in.services.GetSynchrosService;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@WebAdapter
@CrossOrigin
@RestController
@RequiredArgsConstructor
@RequestMapping(value = "/synchros")
public class GetSynchroController {

    private final GetSynchrosService getSynchrosService;
    private final GetSynchroDTOMapper getSynchroDTOMapper;

    @GetMapping("")
    @ApiOperation("Get synchros")
    public ResponseEntity<Paginator<GetSynchroDTO>> getSynchros(
            Pageable pageable,
            @RequestParam(defaultValue = "") String search
    ){

        var filters = Filters.builder()
                .page(pageable.getPageNumber())
                .pagesize(pageable.getPageSize())
                .search(search)
                .build();

        var pagination = getSynchrosService.getSynchros(filters);
        var data = getSynchroDTOMapper.toDTOSynchros(pagination.getData());
        var res = new Paginator<>(data, pagination.getPage(), pagination.getPagesize(), pagination.getTotal());

        return ResponseEntity.ok(res);

    }
}
