package org.fullguys.swing.adapters.web.controllers.admin.beneficiaries;

import lombok.Data;

@Data
public class LoadBeneficiariesDTO {
    String code;
    String department;
    String province;
    String district;
    String gender;
    Boolean isDisabled;
    Boolean isOlderAdult;
}
