package org.fullguys.swing.adapters.web.controllers.admin.withdrawalPoints;

import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.fullguys.swing.common.annotations.WebAdapter;
import org.fullguys.swing.domain.entities.WithdrawalPoint;
import org.fullguys.swing.domain.usecases.beneficiaries.ports.in.services.SelectWithdrawalPointsService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Collections;
import java.util.List;

@WebAdapter
@CrossOrigin
@RestController
@RequiredArgsConstructor
@RequestMapping(value = "/selectWithdrawalPoints")
public class SelectWithdrawalPointsController {
    private final SelectWithdrawalPointsService selectWithdrawalPointsService;

    @PostMapping("")
    @ApiOperation("Select Withdrawal Points")
    public ResponseEntity<List<WithdrawalPoint>> selectWithdrawalPoints(@RequestBody SelectWithdrawalPointDTO points) {
        var withdrawalPoints = selectWithdrawalPointsService.selectWithdrawalPoints(points.getWithdrawalPointCodes(), points.getBeneficiaryCode());
        return ResponseEntity.ok(withdrawalPoints);
    }
}
