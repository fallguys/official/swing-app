package org.fullguys.swing.adapters.web.controllers.sim;

import org.fullguys.swing.domain.entities.WithdrawalPoint;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;

@Mapper(componentModel = "spring")
public interface ParticipantPointDTOMapper {
    @Mappings({
            @Mapping(source = "id", target = "id"),
            @Mapping(source = "code", target = "code"),
            @Mapping(source = "name", target = "name"),
            @Mapping(source = "address", target = "address")
    })
    GetParticipantPointDTO toDTOParticipantPoint(WithdrawalPoint withdrawalPoint);


}
