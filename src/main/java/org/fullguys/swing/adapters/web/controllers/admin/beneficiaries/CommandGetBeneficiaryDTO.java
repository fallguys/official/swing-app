package org.fullguys.swing.adapters.web.controllers.admin.beneficiaries;

import lombok.Builder;
import lombok.Data;
import org.fullguys.swing.common.queries.Paginator;

import java.time.LocalDateTime;

@Data
@Builder
public class CommandGetBeneficiaryDTO {
    private Paginator<GetBeneficiaryDTO> beneficiariesPaginator;
    private LocalDateTime lastUpdated;
    private Long totalActive;
}
