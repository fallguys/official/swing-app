package org.fullguys.swing.adapters.web.controllers.admin.incidents;

import lombok.Data;
import org.fullguys.swing.domain.entities.enums.IncidentTypeState;

@Data
public class ShowIncidentTypesDTO {
    private String name;
    private String description;
    private IncidentTypeState state;
}
