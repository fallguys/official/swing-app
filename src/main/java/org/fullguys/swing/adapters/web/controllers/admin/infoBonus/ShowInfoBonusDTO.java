package org.fullguys.swing.adapters.web.controllers.admin.infoBonus;

import lombok.Data;

import java.time.LocalDateTime;
import java.util.Date;

@Data
public class ShowInfoBonusDTO {
    private String withdrawalPointName;
    private String address;
    private LocalDateTime assignedInitialDate;
    private LocalDateTime assignedFinalDate;
    private Long found;
    private String message;
}
