package org.fullguys.swing.adapters.web.controllers.admin.contagionAreas;

import org.fullguys.swing.domain.entities.ContagionArea;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;

import java.util.List;

@Mapper(componentModel="spring")
public interface ContagionAreaDTOMapper {

    @Mappings({
            @Mapping(source = "id", target = "id"),
            @Mapping(source = "district.fatherUbigee.fatherUbigee.name", target = "department"),
            @Mapping(source = "district.fatherUbigee.name", target = "province"),
            @Mapping(source = "district.name", target = "district"),
            @Mapping(source = "district.code", target = "ubigee"),
            @Mapping(source = "quantity", target = "quantity"),
            @Mapping(source = "createdAt", target = "registerDate"),
            @Mapping(source = "updatedAt", target = "updatedDate")
    })
    GetContagionAreaDTO toDTOContagionArea(ContagionArea contagionArea);
    List<GetContagionAreaDTO> toDTOContagionAreas(List<ContagionArea> contagionAreas);

}
