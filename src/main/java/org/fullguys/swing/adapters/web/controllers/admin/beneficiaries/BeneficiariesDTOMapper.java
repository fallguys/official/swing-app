package org.fullguys.swing.adapters.web.controllers.admin.beneficiaries;

import org.fullguys.swing.domain.entities.Beneficiary;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;

import java.util.List;

@Mapper(componentModel="spring")
public interface BeneficiariesDTOMapper {

    @Mappings({
            @Mapping(source = "id", target = "id"),
            @Mapping(source = "code", target = "code"),
            @Mapping(source = "district.fatherUbigee.fatherUbigee.name", target = "department"),
            @Mapping(source = "district.fatherUbigee.name", target = "province"),
            @Mapping(source = "district.name", target = "district"),
            @Mapping(source = "district.code", target = "ubigee"),
            @Mapping(source = "createdAt", target = "registerDate"),
            @Mapping(source = "updatedAt", target = "updatedDate"),
            @Mapping(source = "state", target = "state")
    })
    GetBeneficiaryDTO toDTOBeneficiary(Beneficiary beneficiary);
    List<GetBeneficiaryDTO> toDTOBeneficiaries(List<Beneficiary> beneficiaries);

    @Mappings({
            @Mapping(source = "code", target = "code"),
            @Mapping(source = "district", target = "district.name"),
            @Mapping(source = "isOlderAdult", target = "isOlderAdult"),
            @Mapping(source = "isDisabled", target = "isDisabled"),
            @Mapping(source = "gender", target = "gender"),
            @Mapping(target = "id", ignore = true),
            @Mapping(target = "state", ignore = true),
            @Mapping(target = "createdAt", ignore = true),
            @Mapping(target = "updatedAt", ignore = true),
            @Mapping(target = "pointsChoosed", ignore = true),
    })
    Beneficiary toRegisterBeneficiary(LoadBeneficiariesDTO loadBeneficiariesDTO);
    List<Beneficiary> toRegisterBeneficiaries(List<LoadBeneficiariesDTO> loadBeneficiariesDTOs);

}
