package org.fullguys.swing.adapters.web.controllers.admin.schedules;

import lombok.Data;

@Data
public class ShowWithdrawalPointInfoSearch {
    Long scheduleId;
    String ubigee;
    String withdrawalPointCode;
}
