package org.fullguys.swing.adapters.web.controllers.admin.schedules;

import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.fullguys.swing.common.annotations.WebAdapter;
import org.fullguys.swing.common.queries.Filters;
import org.fullguys.swing.common.queries.Paginator;
import org.fullguys.swing.domain.entities.enums.ScheduleType;
import org.fullguys.swing.domain.usecases.scheduling.ports.in.services.GetSchedulesService;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@WebAdapter
@CrossOrigin
@RestController
@RequiredArgsConstructor
@RequestMapping(value = "/schedules")
public class GetSchedulesController {

    private final GetSchedulesService getSchedulesService;
    private final GetSchedulesMapper getSchedulesMapper;

    @GetMapping("")
    @ApiOperation("Get schedules by type")
    public ResponseEntity<Paginator<GetScheduleDTO>> getSchedules(
            Pageable pageable,
            @RequestParam(defaultValue = "") String search,
            @RequestParam ScheduleType type) {

        var filters = Filters.builder()
                .page(pageable.getPageNumber())
                .pagesize(pageable.getPageSize())
                .search(search)
                .build();

        var pagination = getSchedulesService.getSchedules(type, filters);
        var data = getSchedulesMapper.toGetSchedulesDTO(pagination.getData());
        var res = new Paginator<>(data, pagination.getPage(), pagination.getPagesize(), pagination.getTotal());

        return ResponseEntity.ok(res);
    }
}
