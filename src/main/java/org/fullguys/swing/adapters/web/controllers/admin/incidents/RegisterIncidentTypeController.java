package org.fullguys.swing.adapters.web.controllers.admin.incidents;

import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.fullguys.swing.common.annotations.WebAdapter;
import org.fullguys.swing.domain.usecases.incidents.ports.in.services.RegisterIncidentTypeService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@WebAdapter
@CrossOrigin
@RestController
@RequiredArgsConstructor
@RequestMapping(value = "/incidentType")
public class RegisterIncidentTypeController {

    private final RegisterIncidentTypeService registerIncidentTypeService;
    private final IncidentTypeDTOMapper incidentTypeMapper;

    @PostMapping("")
    @ApiOperation("Register an incident type")
    public ResponseEntity<Long> registerIncidentType(@RequestBody RegisterIncidentTypeDTO incidentTypeDTO) {
        var incidentType = incidentTypeMapper.toIncidentType(incidentTypeDTO);
        var id = registerIncidentTypeService.register(incidentType).getId();
        return ResponseEntity.ok(id);
    }
}
