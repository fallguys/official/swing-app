package org.fullguys.swing.adapters.web.controllers.admin.schedules;

import lombok.Data;

import java.time.LocalDateTime;

@Data
public class ShowBeneficiariesInfoDTO {
    String id;
    String withdrawalPoint;
    LocalDateTime initialDate;
    LocalDateTime finalDate;
    Boolean bonusRetired;
    LocalDateTime bonusRetiredDate;
    Long mistakes;
}
