package org.fullguys.swing.adapters.web.controllers.admin.withdrawalPoints;

import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.fullguys.swing.common.annotations.WebAdapter;
import org.fullguys.swing.domain.entities.WithdrawalPoint;
import org.fullguys.swing.domain.usecases.beneficiaries.ports.in.services.GetWithdrawalPointsByUbigeeService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@WebAdapter
@CrossOrigin
@RestController
@RequiredArgsConstructor
@RequestMapping(value = "/withdrawalPoints/ubigee")
public class GetWithdrawalPointsByUbigeeController {
    private final GetWithdrawalPointsByUbigeeService getWithdrawalPointsByUbigeeService;

    @GetMapping("")
    @ApiOperation("Get Withdrawal Points by Ubigee in an active Schedule")
    public ResponseEntity<List<WithdrawalPoint>> getWithdrawalPointsByUbigee(
            @RequestParam String ubigee
    ){
        var withdrawalPoints = getWithdrawalPointsByUbigeeService.getWithdrawalPointsByUbigee(ubigee);
        return ResponseEntity.ok(withdrawalPoints);
    }
}
