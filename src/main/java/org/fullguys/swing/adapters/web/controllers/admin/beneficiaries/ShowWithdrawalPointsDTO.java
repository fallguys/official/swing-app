package org.fullguys.swing.adapters.web.controllers.admin.beneficiaries;

import lombok.Data;

@Data
public class ShowWithdrawalPointsDTO {
    private String code;
    private String name;
    private String address;
}
