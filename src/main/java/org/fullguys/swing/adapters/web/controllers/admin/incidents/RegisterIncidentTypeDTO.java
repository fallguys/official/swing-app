package org.fullguys.swing.adapters.web.controllers.admin.incidents;

import lombok.Data;

@Data
public class RegisterIncidentTypeDTO {
    private String name;
    private String description;
}
