package org.fullguys.swing.adapters.web.controllers.admin.incidents;

import lombok.Data;

@Data
public class RegisterIncidentDTO {
    String code;
    String message;
    Long incidentTypeId;
}
