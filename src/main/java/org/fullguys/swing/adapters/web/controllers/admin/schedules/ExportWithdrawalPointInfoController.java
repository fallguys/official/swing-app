package org.fullguys.swing.adapters.web.controllers.admin.schedules;

import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.fullguys.swing.adapters.persistence.views.adapters.InfoWithdrawalPointAdapter;
import org.fullguys.swing.common.annotations.WebAdapter;
import org.mapstruct.Context;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.util.List;

@WebAdapter
@CrossOrigin
@RestController
@RequiredArgsConstructor
@RequestMapping(value = "/schedule/withdrawalPoint/export")
public class ExportWithdrawalPointInfoController {

    private final InfoWithdrawalPointAdapter infoWithdrawalPointAdapter;
    private final ShowWithdrawalPointMapper showWithdrawalPointMapper;

    @GetMapping("")
    @ApiOperation("Export the withdrawal points info for a schedule")
    public ResponseEntity<List<ShowWithdrawalPointDTO>> exportBeneficiariesInfo(
            @Context HttpServletResponse response,
            @ModelAttribute ShowWithdrawalPointInfoSearch showWithdrawalPointInfoSearch,
            @RequestParam String type
    ){
        var rows = infoWithdrawalPointAdapter.showAllInfoBeneficiaries(showWithdrawalPointInfoSearch);
        var showWithdrawalPointDTOList = showWithdrawalPointMapper.toShowWithdrawalPointDto(rows);
        try{
            infoWithdrawalPointAdapter.exportWithdrawalPoints(showWithdrawalPointDTOList, response.getOutputStream(), type);
        }
        catch(Exception e){
            return null;
        }

        return ResponseEntity.ok(showWithdrawalPointDTOList);
    }
}
