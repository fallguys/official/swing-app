package org.fullguys.swing.adapters.web.controllers.admin.schedules;

import lombok.Builder;
import lombok.Data;

import java.time.LocalDate;
import java.time.LocalDateTime;

@Data
@Builder
public class GetScheduleDTO {
    private Long id;
    private String responsible;
    private LocalDateTime creationDate;
    private LocalDate initialDate;
    private LocalDate finalDate;
    private String type;
    private String state;
}
