package org.fullguys.swing.domain.usecases.scheduling.errors;

import org.fullguys.swing.common.formatter.TimeFormatter;

import java.time.LocalDate;

public class InvalidChangeStateForDateInitialInActiveScheduleError extends RuntimeException{
    private final LocalDate initialDate;
    public InvalidChangeStateForDateInitialInActiveScheduleError(LocalDate initialDate){
        super(String.format("No se ha podido poner en vigencia el cronograma, ya que aún su fecha inicial es (%s)",
                initialDate.format(TimeFormatter.DATE)));
        this.initialDate = initialDate;
    }
}
