package org.fullguys.swing.domain.usecases.scheduling.impl;

import lombok.RequiredArgsConstructor;
import org.fullguys.swing.common.annotations.UseCase;
import org.fullguys.swing.domain.entities.Schedule;
import org.fullguys.swing.domain.usecases.feedback.ports.out.persistence.GetCurrentSchedulePort;
import org.fullguys.swing.domain.usecases.scheduling.ports.in.services.GetCurrentScheduleService;
import org.fullguys.swing.domain.usecases.scheduling.ports.out.persistence.GetLastSchedulePort;

import java.util.Optional;

@UseCase
@RequiredArgsConstructor
public class GetLastScheduleUseCase implements GetCurrentScheduleService {

    private final GetCurrentSchedulePort getCurrentSchedulePort;

    @Override
    public Optional<Schedule> getCurrentSchedule() {
        return getCurrentSchedulePort.getCurrentSchedule();
    }
}
