package org.fullguys.swing.domain.usecases.synchro.ports.in.services;

import org.fullguys.swing.domain.entities.Synchro;

public interface ShowSynchroDetailService {
    Synchro showSynchroDetail(Long id);
}
