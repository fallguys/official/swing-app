package org.fullguys.swing.domain.usecases.scheduling.ports.out.persistence;

import org.fullguys.swing.domain.entities.Schedule;
import org.fullguys.swing.domain.entities.enums.ScheduleState;
import org.fullguys.swing.domain.entities.enums.ScheduleType;

import java.util.Optional;

public interface GetLastScheduleRegisteredPort {
    Optional<Schedule> getLastRegistered(ScheduleType scheduleType, ScheduleState scheduleState);
}
