package org.fullguys.swing.domain.usecases.scheduling.errors;

import lombok.Getter;
import org.fullguys.swing.common.formatter.TimeFormatter;

import java.time.LocalDate;
import java.time.LocalDateTime;

@Getter
public class InvalidChangeStateForDateInAvailableScheduleError extends RuntimeException{

    private final LocalDate finalDate;

    public InvalidChangeStateForDateInAvailableScheduleError(LocalDate finalDate){
        super(String.format("No se puede cambiar el estado del cronograma, debido a que hay un cronograma programado o vigente con fecha hasta el (%s)",
                finalDate.format(TimeFormatter.DATE)));
        this.finalDate = finalDate;
    }
}
