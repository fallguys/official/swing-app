package org.fullguys.swing.domain.usecases.synchro.ports.in.services;

import org.fullguys.swing.domain.entities.Beneficiary;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;
import java.util.List;

public interface LoadBeneficiariesDataService {
    void loadBeneficiariesDataService(MultipartFile beneficiariesArchive, Long adminId) throws IOException;
}
