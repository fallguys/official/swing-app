package org.fullguys.swing.domain.usecases.scheduling.ports.in.services;

import org.fullguys.swing.domain.usecases.scheduling.info.InfoBeforeGenerateSchedule;

public interface CheckForInfoBeforeGenerateScheduleService {
    InfoBeforeGenerateSchedule checkForInfoBeforeGenerateSchedule();
}
