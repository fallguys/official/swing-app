package org.fullguys.swing.domain.usecases.scheduling.errors;

public class ScheduleNotFoundError extends RuntimeException{
    public ScheduleNotFoundError(){
        super("No se ha encontrado el cronograma");
    }
}
