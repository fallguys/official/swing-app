package org.fullguys.swing.domain.usecases.scheduling.ports.out.scheduler;

import org.fullguys.swing.domain.entities.Schedule;

public interface ShowScheduleDetailsPort {
    Schedule showScheduleDetails(Long scheduleId);
}
