package org.fullguys.swing.domain.usecases.incidents.ports.out.persistence;

import org.fullguys.swing.domain.entities.Incident;

public interface SaveIncidentPort {
    Incident save(Incident incident);
}
