package org.fullguys.swing.domain.usecases.scheduling.ports.in.services;

import org.fullguys.swing.domain.entities.Schedule;
import org.fullguys.swing.domain.entities.enums.ScheduleType;

import java.util.Optional;

public interface GetCurrentScheduleService {
    Optional<Schedule> getCurrentSchedule();
}
