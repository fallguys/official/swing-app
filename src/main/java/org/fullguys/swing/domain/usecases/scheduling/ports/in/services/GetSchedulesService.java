package org.fullguys.swing.domain.usecases.scheduling.ports.in.services;

import org.fullguys.swing.common.queries.Filters;
import org.fullguys.swing.common.queries.Paginator;
import org.fullguys.swing.domain.entities.Schedule;
import org.fullguys.swing.domain.entities.enums.ScheduleType;

public interface GetSchedulesService {
    Paginator<Schedule> getSchedules(ScheduleType type, Filters filters);
}
