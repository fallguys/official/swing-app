package org.fullguys.swing.domain.usecases.synchro.ports.out.persistence;

import org.springframework.web.multipart.MultipartFile;

public interface SaveContagionAreasFilePort {
    String saveContagionAreasFile(MultipartFile file);
}
