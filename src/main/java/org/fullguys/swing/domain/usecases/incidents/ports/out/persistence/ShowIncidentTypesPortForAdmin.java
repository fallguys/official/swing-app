package org.fullguys.swing.domain.usecases.incidents.ports.out.persistence;

import org.fullguys.swing.common.queries.Filters;
import org.fullguys.swing.common.queries.Paginator;
import org.fullguys.swing.domain.entities.IncidentType;

import java.util.List;

public interface ShowIncidentTypesPortForAdmin {
    Paginator<IncidentType> showIncidentTypes(Filters filters);
}
