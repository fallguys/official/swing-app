package org.fullguys.swing.domain.usecases.scheduling.impl;

import lombok.RequiredArgsConstructor;
import org.fullguys.swing.common.annotations.UseCase;
import org.fullguys.swing.common.queries.Filters;
import org.fullguys.swing.common.queries.Paginator;
import org.fullguys.swing.domain.entities.WithdrawalPoint;
import org.fullguys.swing.domain.usecases.scheduling.ports.in.services.ShowTotalAssignedService;
import org.fullguys.swing.domain.usecases.scheduling.ports.in.services.ShowWithdrawalPointInfoService;
import org.fullguys.swing.domain.usecases.scheduling.ports.out.scheduler.ShowTotalAssignedPort;
import org.fullguys.swing.domain.usecases.scheduling.ports.out.scheduler.ShowWithdrawalPointInfoPort;

import java.util.List;

@UseCase
@RequiredArgsConstructor
public class ShowWithdrawalPointInfoUseCase implements ShowWithdrawalPointInfoService, ShowTotalAssignedService {
    private final ShowWithdrawalPointInfoPort showWithdrawalPointInfoPort;
    private final ShowTotalAssignedPort showTotalAssignedPort;

    @Override
    public Paginator<WithdrawalPoint> showBeneficiariesInfo(Filters filters) {
        return showWithdrawalPointInfoPort.showBeneficiariesInfo(filters);
    }

    @Override
    public List<Integer> showTotalAssignedAndAttendedForWithdrawalPointForSchedule(String withdrawalPointCode) {
        return showTotalAssignedPort.showTotalAssignedAndAttendedForWithdrawalPointForSchedule(withdrawalPointCode);
    }
}
