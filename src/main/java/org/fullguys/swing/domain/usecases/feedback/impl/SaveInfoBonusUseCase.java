package org.fullguys.swing.domain.usecases.feedback.impl;

import lombok.RequiredArgsConstructor;
import org.fullguys.swing.common.annotations.UseCase;
import org.fullguys.swing.domain.usecases.feedback.ports.in.services.CheckPickUpBonusService;
import org.fullguys.swing.domain.usecases.feedback.ports.in.services.SaveInfoBonusService;
import org.fullguys.swing.domain.usecases.feedback.ports.out.persistence.SaveInfoBonusPort;
import org.fullguys.swing.domain.usecases.feedback.ports.out.persistence.UpdateBeneficiaryPort;

@UseCase
@RequiredArgsConstructor
public class SaveInfoBonusUseCase implements SaveInfoBonusService {

    private final SaveInfoBonusPort saveInfoBonusPort;
    private final CheckPickUpBonusService checkPickUpBonusService;
    private final UpdateBeneficiaryPort updateBeneficiaryPort;

    @Override
    public void saveInfoBonus(String codeBeneficiary, String codePoint) {

        var command = checkPickUpBonusService.checkPickUpBonus(codeBeneficiary, codePoint);
        updateBeneficiaryPort.updateHitBeneficiary(command.getIdBeneficiary());
        var bonusSaved = saveInfoBonusPort.saveInfoBonus(command.getIdParticipantBeneficiary());

    }
}
