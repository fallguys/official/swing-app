package org.fullguys.swing.domain.usecases.scheduling.ports.out.scheduler;

import java.util.List;

public interface ShowTotalAssignedPort {
    List<Integer> showTotalAssignedAndAttendedForWithdrawalPointForSchedule(String withdrawalPointCode);
}
