package org.fullguys.swing.domain.usecases.beneficiaries.ports.out.persistence;

import org.fullguys.swing.common.queries.Paginator;
import org.fullguys.swing.domain.entities.Assignment;

public interface ShowInfoBonusPort {
    Assignment showInfoBonus(String search);
}
