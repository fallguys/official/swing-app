package org.fullguys.swing.domain.usecases.synchro.ports.out.persistence;

import org.fullguys.swing.common.queries.Filters;
import org.fullguys.swing.common.queries.Paginator;
import org.fullguys.swing.domain.entities.Synchro;
import org.fullguys.swing.domain.entities.enums.SynchroFileType;
import org.fullguys.swing.domain.entities.enums.SynchroState;

import java.util.Optional;

public interface GetSynchroPort {
    Optional<Synchro> getLastActiveSynchro(SynchroFileType synchroFileType, SynchroState synchroState);
    Paginator<Synchro> getSynchros(Filters filters);
}
