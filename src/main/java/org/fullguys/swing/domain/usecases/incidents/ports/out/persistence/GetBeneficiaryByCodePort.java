package org.fullguys.swing.domain.usecases.incidents.ports.out.persistence;

import org.fullguys.swing.domain.entities.Beneficiary;

import java.util.Optional;

public interface GetBeneficiaryByCodePort {
    Optional<Beneficiary> getBeneficiaryByCode(String code);
}
