package org.fullguys.swing.domain.usecases.scheduling.errors;

public class SynchroProcessingError extends RuntimeException {
    public SynchroProcessingError() {
        super("No se puede generar un nuevo cronograma porque existe al menos una sincronización en marcha");
    }
}
