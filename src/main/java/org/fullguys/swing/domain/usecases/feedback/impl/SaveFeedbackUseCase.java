package org.fullguys.swing.domain.usecases.feedback.impl;

import lombok.RequiredArgsConstructor;
import org.fullguys.swing.common.annotations.UseCase;
import org.fullguys.swing.domain.usecases.feedback.ports.in.services.CheckPickUpBonusService;
import org.fullguys.swing.domain.usecases.feedback.ports.in.services.GetParticipantWithdrawalPointService;
import org.fullguys.swing.domain.usecases.feedback.ports.in.services.SaveFeedbackService;
import org.fullguys.swing.domain.usecases.feedback.ports.out.persistence.GetParticipantWithdrawalPointPort;
import org.fullguys.swing.domain.usecases.feedback.ports.out.persistence.SaveFeedbackPort;
import org.fullguys.swing.domain.usecases.feedback.ports.out.persistence.UpdateBeneficiaryPort;
import org.fullguys.swing.domain.usecases.feedback.ports.out.persistence.UpdatePointPort;

@UseCase
@RequiredArgsConstructor
public class SaveFeedbackUseCase implements SaveFeedbackService {

    private final CheckPickUpBonusService checkPickUpBonusService;
    private final GetParticipantWithdrawalPointPort getParticipantWithdrawalPointPort;
    private final SaveFeedbackPort saveFeedbackPort;
    private final UpdateBeneficiaryPort updateBeneficiaryPort;
    private final GetParticipantWithdrawalPointService getParticipantWithdrawalPointService;
    private final UpdatePointPort updatePointPort;

    @Override
    public void saveFeedback(String codeBeneficiary, String codePoint) {

        var command = checkPickUpBonusService.checkPickUpBonus(codeBeneficiary,codePoint);
        var point = getParticipantWithdrawalPointService.getParticipantWithdrawalPoint(codePoint);
        if(!command.getIsPointAssigned()){
            updatePointPort.updatePoint(point.getId());
        }
        updateBeneficiaryPort.updateInfractionBeneficiary(command.getIdBeneficiary());
        var idParticipantPoint = getParticipantWithdrawalPointPort.getIdParticipantPoint(point.getId(), command.getIdSchedule());
        command.setIdParticipantPoint(idParticipantPoint);
        saveFeedbackPort.saveFeedback(command);

    }
}
