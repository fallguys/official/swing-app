package org.fullguys.swing.domain.usecases.incidents.ports.in.services;

import org.fullguys.swing.domain.entities.Incident;

public interface RegisterIncidentService {
    Incident register(Incident incident);
}
