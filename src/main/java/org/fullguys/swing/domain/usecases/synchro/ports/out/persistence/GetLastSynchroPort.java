package org.fullguys.swing.domain.usecases.synchro.ports.out.persistence;

import org.fullguys.swing.domain.entities.Synchro;
import org.fullguys.swing.domain.entities.enums.SynchroFileType;

import java.util.Optional;

public interface GetLastSynchroPort {
    Optional<Synchro> getLastSynchro(SynchroFileType synchroFileType);
}
