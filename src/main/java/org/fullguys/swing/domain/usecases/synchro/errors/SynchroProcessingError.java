package org.fullguys.swing.domain.usecases.synchro.errors;

public class SynchroProcessingError extends RuntimeException{
    public SynchroProcessingError(){
        super("No se puede sincronizar este recurso porque existe al menos una sincronización en marcha");
    }
}
