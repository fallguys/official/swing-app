package org.fullguys.swing.domain.usecases.feedback.errors;

public class CurrentScheduleNotFoundError extends RuntimeException{
    public CurrentScheduleNotFoundError(){
        super("No se ha encontrado un cronograma oficial vigente");
    }
}
