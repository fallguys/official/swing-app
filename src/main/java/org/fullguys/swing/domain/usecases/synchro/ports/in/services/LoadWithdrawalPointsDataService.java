package org.fullguys.swing.domain.usecases.synchro.ports.in.services;

import org.springframework.web.multipart.MultipartFile;

public interface LoadWithdrawalPointsDataService {
    void loadWithdrawalPointsData(MultipartFile withdrawalPointFile, Long adminId);
}
