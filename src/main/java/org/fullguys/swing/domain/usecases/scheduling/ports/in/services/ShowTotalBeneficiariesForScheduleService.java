package org.fullguys.swing.domain.usecases.scheduling.ports.in.services;

public interface ShowTotalBeneficiariesForScheduleService {
    Integer showTotalBeneficiariesForSchedule(Long scheduleId);
}
