package org.fullguys.swing.domain.usecases.synchro.ports.out.synchronizer;

import org.fullguys.swing.domain.entities.Beneficiary;

import java.io.File;
import java.util.List;

public interface RegisterBeneficiariesDataBatchPort {
    void registerBeneficiariesDataBatch();
}
