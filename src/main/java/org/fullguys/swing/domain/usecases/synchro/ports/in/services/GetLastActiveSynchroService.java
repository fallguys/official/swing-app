package org.fullguys.swing.domain.usecases.synchro.ports.in.services;

import org.fullguys.swing.domain.entities.Synchro;
import org.fullguys.swing.domain.entities.enums.SynchroFileType;
import org.fullguys.swing.domain.entities.enums.SynchroState;

import java.util.Optional;
import java.util.List;

public interface GetLastActiveSynchroService {
    List<Optional<Synchro>> getLastActiveSynchro();
}
