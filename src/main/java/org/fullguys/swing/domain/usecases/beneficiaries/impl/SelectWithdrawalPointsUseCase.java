package org.fullguys.swing.domain.usecases.beneficiaries.impl;

import lombok.RequiredArgsConstructor;
import org.fullguys.swing.common.annotations.UseCase;
import org.fullguys.swing.domain.entities.WithdrawalPoint;
import org.fullguys.swing.domain.usecases.beneficiaries.ports.in.services.SelectWithdrawalPointsService;
import org.fullguys.swing.domain.usecases.beneficiaries.ports.out.persistence.SelectWithdrawalPointsPort;

import java.util.Collections;
import java.util.List;

@UseCase
@RequiredArgsConstructor
public class SelectWithdrawalPointsUseCase implements SelectWithdrawalPointsService{
    private final SelectWithdrawalPointsPort selectWithdrawalPoints;

    @Override
    public List<WithdrawalPoint> selectWithdrawalPoints(List<String> codes, String beneficiaryCode) {
        return selectWithdrawalPoints.selectWithdrawalPoints(codes, beneficiaryCode);
    }
}
