package org.fullguys.swing.domain.usecases.synchro.impl;

import lombok.RequiredArgsConstructor;
import org.fullguys.swing.common.annotations.UseCase;
import org.fullguys.swing.common.queries.Filters;
import org.fullguys.swing.common.queries.Paginator;
import org.fullguys.swing.domain.entities.Synchro;
import org.fullguys.swing.domain.usecases.synchro.ports.in.services.GetSynchrosService;
import org.fullguys.swing.domain.usecases.synchro.ports.out.persistence.GetSynchroPort;


@RequiredArgsConstructor
@UseCase
public class GetSynchrosUseCase implements GetSynchrosService {

    private final GetSynchroPort getSynchroPort;

    @Override
    public Paginator<Synchro> getSynchros(Filters filters) {
        return getSynchroPort.getSynchros(filters);
    }
}
