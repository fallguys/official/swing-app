package org.fullguys.swing.domain.usecases.incidents.errors;

public class BeneficiaryDoesntExistError extends RuntimeException{
    public BeneficiaryDoesntExistError(String codeBeneficiary){
        super(String.format("No se ha encontrado el beneficiario con codigo %s en el padrón actual", codeBeneficiary));
    }
}
