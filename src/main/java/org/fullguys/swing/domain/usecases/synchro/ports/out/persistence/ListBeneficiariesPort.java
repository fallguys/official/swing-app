package org.fullguys.swing.domain.usecases.synchro.ports.out.persistence;

import org.fullguys.swing.common.queries.Filters;
import org.fullguys.swing.common.queries.Paginator;
import org.fullguys.swing.domain.entities.Beneficiary;

import java.util.List;

public interface ListBeneficiariesPort {
    Paginator<Beneficiary> listBeneficiaries(Filters filters);
}
