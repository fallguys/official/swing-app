package org.fullguys.swing.domain.usecases.beneficiaries.impl;

import lombok.RequiredArgsConstructor;
import org.fullguys.swing.common.annotations.UseCase;
import org.fullguys.swing.common.queries.Filters;
import org.fullguys.swing.common.queries.Paginator;
import org.fullguys.swing.domain.entities.WithdrawalPoint;
import org.fullguys.swing.domain.usecases.beneficiaries.ports.in.services.ShowWithdrawalPointService;
import org.fullguys.swing.domain.usecases.beneficiaries.ports.out.persistence.ShowWithdrawalPointsForUbigeePort;

@UseCase
@RequiredArgsConstructor
public class ShowWithdrawalPointUseCase implements ShowWithdrawalPointService {
    private final ShowWithdrawalPointsForUbigeePort showWithdrawalPointsForUbigeePort;

    @Override
    public Paginator<WithdrawalPoint> showWithdrawalPoint(Filters filters) {
        return showWithdrawalPointsForUbigeePort.showWithdrawalPoints(filters);
    }
}
