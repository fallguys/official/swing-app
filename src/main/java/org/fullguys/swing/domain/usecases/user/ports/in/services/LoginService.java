package org.fullguys.swing.domain.usecases.user.ports.in.services;

import org.fullguys.swing.domain.entities.Administrator;

public interface LoginService {
    Administrator login(String email, String password);
}
