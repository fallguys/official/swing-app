package org.fullguys.swing.domain.usecases.synchro.ports.out.persistence;

import org.fullguys.swing.domain.entities.enums.WithdrawalPointState;

public interface CountWithdrawalPointsByStatusPort {
    long countByState(WithdrawalPointState withdrawalPointState);
}
