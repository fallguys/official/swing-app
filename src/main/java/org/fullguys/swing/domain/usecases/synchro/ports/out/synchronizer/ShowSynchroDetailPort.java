package org.fullguys.swing.domain.usecases.synchro.ports.out.synchronizer;

import org.fullguys.swing.domain.entities.Synchro;

public interface ShowSynchroDetailPort {
    Synchro showSynchroDetail(Long id);
}
