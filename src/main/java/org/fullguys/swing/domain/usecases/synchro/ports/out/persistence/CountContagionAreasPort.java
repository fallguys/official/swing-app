package org.fullguys.swing.domain.usecases.synchro.ports.out.persistence;

public interface CountContagionAreasPort {
    long count();
}
