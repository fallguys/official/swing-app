package org.fullguys.swing.domain.usecases.scheduling.ports.out.persistence;

import org.fullguys.swing.domain.entities.Schedule;
import org.fullguys.swing.domain.entities.enums.ScheduleType;

import java.util.Optional;

public interface GetLastSchedulePort {
    Optional<Schedule> getLastSchedule(ScheduleType scheduleType);
}
