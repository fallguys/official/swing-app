package org.fullguys.swing.domain.usecases.scheduling.ports.in.services;

public interface ShowTotalWithdrawalPointsForScheduleService {
    Integer showTotalWithdrawalPointsForSchedule(Long scheduleId);
}
