package org.fullguys.swing.domain.usecases.scheduling.ports.in.services;

import java.util.List;

public interface ShowTotalAssignedService {
    List<Integer> showTotalAssignedAndAttendedForWithdrawalPointForSchedule(String withdrawalPointCode);
}
