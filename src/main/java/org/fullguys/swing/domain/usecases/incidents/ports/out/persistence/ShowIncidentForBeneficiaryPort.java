package org.fullguys.swing.domain.usecases.incidents.ports.out.persistence;

import org.fullguys.swing.domain.entities.Incident;

import java.util.List;

public interface ShowIncidentForBeneficiaryPort {
    List<Incident> showIncidentTypesForBeneficiary(String beneficiaryCode);
}
