package org.fullguys.swing.domain.usecases.scheduling.impl;

import lombok.RequiredArgsConstructor;
import org.fullguys.swing.common.annotations.UseCase;
import org.fullguys.swing.common.queries.Filters;
import org.fullguys.swing.common.queries.Paginator;
import org.fullguys.swing.domain.entities.Schedule;
import org.fullguys.swing.domain.entities.enums.ScheduleType;
import org.fullguys.swing.domain.usecases.scheduling.ports.in.services.GetSchedulesService;
import org.fullguys.swing.domain.usecases.scheduling.ports.out.persistence.GetSchedulesPort;

@UseCase
@RequiredArgsConstructor
public class GetSchedulesUseCase implements GetSchedulesService {

    private final GetSchedulesPort getSchedulesPort;

    @Override
    public Paginator<Schedule> getSchedules(ScheduleType type, Filters filters) {
        return getSchedulesPort.getSchedules(type, filters);
    }
}
