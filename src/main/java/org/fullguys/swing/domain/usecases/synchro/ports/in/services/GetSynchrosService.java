package org.fullguys.swing.domain.usecases.synchro.ports.in.services;

import org.fullguys.swing.common.queries.Filters;
import org.fullguys.swing.common.queries.Paginator;
import org.fullguys.swing.domain.entities.Synchro;


public interface GetSynchrosService {
    Paginator<Synchro> getSynchros(Filters filters);
}
