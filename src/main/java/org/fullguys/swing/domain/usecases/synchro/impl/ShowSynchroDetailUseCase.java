package org.fullguys.swing.domain.usecases.synchro.impl;

import lombok.RequiredArgsConstructor;
import org.fullguys.swing.common.annotations.UseCase;
import org.fullguys.swing.domain.entities.Synchro;
import org.fullguys.swing.domain.usecases.synchro.ports.in.services.ShowSynchroDetailService;
import org.fullguys.swing.domain.usecases.synchro.ports.out.synchronizer.ShowSynchroDetailPort;

@RequiredArgsConstructor
@UseCase
public class ShowSynchroDetailUseCase implements ShowSynchroDetailService {
    private final ShowSynchroDetailPort showSynchroDetailPort;

    @Override
    public Synchro showSynchroDetail(Long id) {
        return showSynchroDetailPort.showSynchroDetail(id);
    }
}
