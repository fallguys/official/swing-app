package org.fullguys.swing.domain.usecases.incidents.impl;

import lombok.RequiredArgsConstructor;
import org.fullguys.swing.common.annotations.UseCase;
import org.fullguys.swing.common.queries.Filters;
import org.fullguys.swing.common.queries.Paginator;
import org.fullguys.swing.domain.entities.Incident;
import org.fullguys.swing.domain.usecases.incidents.ports.in.services.ShowIncidentService;
import org.fullguys.swing.domain.usecases.incidents.ports.out.persistence.ShowIncidentPort;

import java.util.List;

@UseCase
@RequiredArgsConstructor
public class ShowIncidentUseCase implements ShowIncidentService {
    private final ShowIncidentPort showIncidentPort;

    @Override
    public Paginator<Incident> showIncident(Filters filters) {
        return showIncidentPort.showIncident(filters);
    }
}
