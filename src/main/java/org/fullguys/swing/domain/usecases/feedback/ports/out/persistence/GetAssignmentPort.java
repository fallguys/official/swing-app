package org.fullguys.swing.domain.usecases.feedback.ports.out.persistence;

import org.fullguys.swing.domain.entities.Assignment;

public interface GetAssignmentPort {
    Assignment getAssignment(Long idParticipantBeneficiary);
}
