package org.fullguys.swing.domain.usecases.scheduling.ports.in.services;

import org.fullguys.swing.domain.entities.Schedule;

public interface GenerateNewScheduleService {
    void generate(Schedule schedule);
}
