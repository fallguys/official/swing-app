package org.fullguys.swing.domain.usecases.synchro.ports.out.persistence;

import org.fullguys.swing.common.queries.Filters;
import org.fullguys.swing.common.queries.Paginator;
import org.fullguys.swing.domain.entities.ContagionArea;

import java.util.List;

public interface ListContagionAreaPort {
    Paginator<ContagionArea> listContagionAreas(Filters filters);
}
