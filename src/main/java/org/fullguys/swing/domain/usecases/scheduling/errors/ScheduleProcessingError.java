package org.fullguys.swing.domain.usecases.scheduling.errors;

public class ScheduleProcessingError extends RuntimeException {
    public ScheduleProcessingError() {
        super("No se puede generar un nuevo cronograma porque se está generando otro cronograma");
    }
}
