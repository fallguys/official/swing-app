package org.fullguys.swing.domain.usecases.synchro.ports.in.services;

import org.fullguys.swing.common.queries.Filters;
import org.fullguys.swing.common.queries.Paginator;
import org.fullguys.swing.domain.entities.ContagionArea;
import org.fullguys.swing.domain.usecases.synchro.commands.ContagionAreasCommand;

import java.util.List;

public interface GetContagionAreaService {
    ContagionAreasCommand getContagionAreas(Filters filters);
}
