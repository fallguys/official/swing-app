package org.fullguys.swing.domain.usecases.synchro.commands;

import lombok.Data;
import org.fullguys.swing.common.queries.Paginator;
import org.fullguys.swing.domain.entities.Beneficiary;

import java.time.LocalDateTime;

@Data
public class BeneficiaryCommand {
    private Paginator<Beneficiary> paginator;
    private Long totalActive;
    private LocalDateTime lastUpdated;
}
