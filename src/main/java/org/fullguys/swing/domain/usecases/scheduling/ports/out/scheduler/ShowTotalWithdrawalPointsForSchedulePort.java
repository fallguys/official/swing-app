package org.fullguys.swing.domain.usecases.scheduling.ports.out.scheduler;

public interface ShowTotalWithdrawalPointsForSchedulePort {
    Integer showTotalWithdrawalPointsForSchedule(Long scheduleId);
}
