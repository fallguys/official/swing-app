package org.fullguys.swing.domain.usecases.synchro.impl;

import lombok.RequiredArgsConstructor;
import org.fullguys.swing.common.annotations.UseCase;
import org.fullguys.swing.domain.entities.Synchro;
import org.fullguys.swing.domain.entities.enums.SynchroFileType;
import org.fullguys.swing.domain.usecases.synchro.ports.in.services.GetLastActiveSynchroService;
import org.fullguys.swing.domain.usecases.synchro.ports.out.persistence.GetLastSynchroPort;

import java.util.ArrayList;
import java.util.Optional;
import java.util.List;

@RequiredArgsConstructor
@UseCase
public class GetLastActiveSynchroUseCase implements GetLastActiveSynchroService {

    private final GetLastSynchroPort getLastSynchroPort;

    @Override
    public List<Optional<Synchro>> getLastActiveSynchro() {
        var beneficiarySynchro = getLastSynchroPort.
                getLastSynchro(SynchroFileType.BENEFICIARY);

        var contagionAreaSynchro = getLastSynchroPort.
                getLastSynchro(SynchroFileType.CONTAGION_AREA);


        var withdrawalPointSynchro = getLastSynchroPort.
                getLastSynchro(SynchroFileType.WITHDRAWAL_POINT);


        List<Optional<Synchro>> activeSynchros = new ArrayList<>();
        activeSynchros.add(beneficiarySynchro);
        activeSynchros.add(contagionAreaSynchro);
        activeSynchros.add(withdrawalPointSynchro);

        return activeSynchros;
    }
}
