package org.fullguys.swing.domain.usecases.synchro.ports.in.services;

import org.fullguys.swing.common.queries.Filters;
import org.fullguys.swing.common.queries.Paginator;
import org.fullguys.swing.domain.entities.Beneficiary;
import org.fullguys.swing.domain.usecases.synchro.commands.BeneficiaryCommand;

import java.util.List;

public interface GetBeneficiariesService {
    BeneficiaryCommand getBeneficiaries(Filters filters);
}
