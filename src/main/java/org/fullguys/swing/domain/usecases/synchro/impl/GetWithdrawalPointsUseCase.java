package org.fullguys.swing.domain.usecases.synchro.impl;

import lombok.RequiredArgsConstructor;
import org.fullguys.swing.common.annotations.UseCase;
import org.fullguys.swing.common.queries.Filters;
import org.fullguys.swing.common.queries.Paginator;
import org.fullguys.swing.domain.entities.WithdrawalPoint;
import org.fullguys.swing.domain.entities.enums.SynchroFileType;
import org.fullguys.swing.domain.entities.enums.SynchroState;
import org.fullguys.swing.domain.entities.enums.WithdrawalPointState;
import org.fullguys.swing.domain.usecases.synchro.commands.PointCommand;
import org.fullguys.swing.domain.usecases.synchro.ports.in.services.GetWithdrawalPointsService;
import org.fullguys.swing.domain.usecases.synchro.ports.out.persistence.CountWithdrawalPointsByStatusPort;
import org.fullguys.swing.domain.usecases.synchro.ports.out.persistence.GetSynchroPort;
import org.fullguys.swing.domain.usecases.synchro.ports.out.persistence.ListWithdrawalPointsPort;

import java.util.List;

@RequiredArgsConstructor
@UseCase
public class GetWithdrawalPointsUseCase implements GetWithdrawalPointsService {

    private final ListWithdrawalPointsPort listWithdrawalPointsPort;
    private final CountWithdrawalPointsByStatusPort countWithdrawalPointsByStatusPort;
    private final GetSynchroPort getSynchroPort;

    @Override
    public PointCommand getWithdrawalPoints(Filters filters) {

        var points = listWithdrawalPointsPort.listWithdrawalPoints(filters);
        var synchro = getSynchroPort.getLastActiveSynchro(SynchroFileType.WITHDRAWAL_POINT, SynchroState.COMPLETED);
        var actives = countWithdrawalPointsByStatusPort.countByState(WithdrawalPointState.ACTIVE);

        PointCommand pointCommand = new PointCommand();
        pointCommand.setPaginator(points);
        if(synchro.isPresent()){
            pointCommand.setLastUpdated(synchro.get().getInitialDate());
        }
        pointCommand.setTotalActive(actives);

        return pointCommand;
    }
}
