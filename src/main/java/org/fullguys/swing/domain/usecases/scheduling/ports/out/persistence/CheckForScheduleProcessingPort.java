package org.fullguys.swing.domain.usecases.scheduling.ports.out.persistence;

public interface CheckForScheduleProcessingPort {
    boolean checkForScheduleProcessing();
}
