package org.fullguys.swing.domain.usecases.scheduling.ports.out.scheduler;

public interface ShowTotalBeneficiariesForSchedulePort {
    Integer showTotalBeneficiariesForSchedule(Long scheduleId);
}
