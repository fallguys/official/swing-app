package org.fullguys.swing.domain.usecases.scheduling.errors;

import org.fullguys.swing.common.formatter.TimeFormatter;

import java.time.LocalDate;

public class InvalidChangeStateForDateFinalInFinishedScheduleError extends RuntimeException{
    private final LocalDate finalDate;

    public InvalidChangeStateForDateFinalInFinishedScheduleError(LocalDate finalDate){
        super(String.format("No se puede terminar el cronograma, ya que aún no se ha llegado a su fecha fin (%s)",
                finalDate.format(TimeFormatter.DATE)));
        this.finalDate = finalDate;
    }
}
