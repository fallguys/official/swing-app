package org.fullguys.swing.domain.usecases.beneficiaries.ports.in.services;

import org.fullguys.swing.common.queries.Filters;
import org.fullguys.swing.common.queries.Paginator;
import org.fullguys.swing.domain.entities.WithdrawalPoint;

public interface ShowWithdrawalPointService {
    Paginator<WithdrawalPoint> showWithdrawalPoint(Filters filters);
}
