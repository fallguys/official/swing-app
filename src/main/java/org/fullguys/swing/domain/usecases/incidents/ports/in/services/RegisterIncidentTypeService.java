package org.fullguys.swing.domain.usecases.incidents.ports.in.services;

import org.fullguys.swing.domain.entities.IncidentType;

public interface RegisterIncidentTypeService {
    IncidentType register(IncidentType incidentType);
}
