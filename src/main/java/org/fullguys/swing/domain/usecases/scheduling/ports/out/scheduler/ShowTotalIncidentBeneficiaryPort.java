package org.fullguys.swing.domain.usecases.scheduling.ports.out.scheduler;

public interface ShowTotalIncidentBeneficiaryPort {
    Integer showTotalIncidentsForBeneficiary(String beneficiaryCode);
}
