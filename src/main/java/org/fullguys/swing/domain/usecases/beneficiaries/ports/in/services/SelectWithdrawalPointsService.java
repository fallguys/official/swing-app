package org.fullguys.swing.domain.usecases.beneficiaries.ports.in.services;

import org.fullguys.swing.domain.entities.WithdrawalPoint;

import java.util.List;

public interface SelectWithdrawalPointsService {
    List<WithdrawalPoint> selectWithdrawalPoints(List<String> withdrawalPointsCodes, String beneficiaryCode);
}
