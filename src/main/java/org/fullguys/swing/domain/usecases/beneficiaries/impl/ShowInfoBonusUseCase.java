package org.fullguys.swing.domain.usecases.beneficiaries.impl;

import lombok.RequiredArgsConstructor;
import org.fullguys.swing.common.annotations.UseCase;
import org.fullguys.swing.common.queries.Paginator;
import org.fullguys.swing.domain.entities.Assignment;
import org.fullguys.swing.domain.usecases.beneficiaries.ports.in.services.ShowInfoBonusService;
import org.fullguys.swing.domain.usecases.beneficiaries.ports.out.persistence.ShowInfoBonusPort;

@UseCase
@RequiredArgsConstructor
public class ShowInfoBonusUseCase implements ShowInfoBonusService {
    private final ShowInfoBonusPort showInfoBonusPort;

    @Override
    public Assignment showInfoBonus(String search) {
        return showInfoBonusPort.showInfoBonus(search);
    }
}
