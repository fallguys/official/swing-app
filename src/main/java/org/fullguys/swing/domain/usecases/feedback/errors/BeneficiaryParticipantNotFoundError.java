package org.fullguys.swing.domain.usecases.feedback.errors;

public class BeneficiaryParticipantNotFoundError extends RuntimeException{
    public BeneficiaryParticipantNotFoundError(String codeBeneficiary){
        super(String.format("No se ha encontrado al beneficiario participante con código %s", codeBeneficiary));
    }
}
