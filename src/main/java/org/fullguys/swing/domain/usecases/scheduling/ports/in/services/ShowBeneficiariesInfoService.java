package org.fullguys.swing.domain.usecases.scheduling.ports.in.services;

import org.fullguys.swing.common.queries.Filters;
import org.fullguys.swing.common.queries.Paginator;
import org.fullguys.swing.domain.entities.Assignment;

public interface ShowBeneficiariesInfoService {
    Paginator<Assignment> showBeneficiariesInfo(Filters filters);
}
