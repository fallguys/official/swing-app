package org.fullguys.swing.domain.usecases.user.impl;

import lombok.RequiredArgsConstructor;
import org.fullguys.swing.common.annotations.UseCase;
import org.fullguys.swing.common.Constants;
import org.fullguys.swing.domain.entities.Administrator;
import org.fullguys.swing.domain.usecases.user.errors.AccountNotFoundError;
import org.fullguys.swing.domain.usecases.user.errors.WrongPasswordError;
import org.fullguys.swing.domain.usecases.user.ports.in.services.LoginService;
import org.fullguys.swing.domain.usecases.user.ports.out.persistence.GetAdminPort;

import javax.transaction.Transactional;

@UseCase
@Transactional
@RequiredArgsConstructor
public class LoginUseCase implements LoginService {
    private final GetAdminPort getAdminPort;

    @Override
    public Administrator login(String email, String password) {
        var admin = getAdminPort
                .getAdmin(email)
                .orElseThrow(AccountNotFoundError::new);

        if (admin.getPassword().compareTo(password) != Constants.EQUAL_NUMBER) {
            throw new WrongPasswordError();
        }

        return admin;
    }
}
