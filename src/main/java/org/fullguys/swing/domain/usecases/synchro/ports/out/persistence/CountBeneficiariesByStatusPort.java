package org.fullguys.swing.domain.usecases.synchro.ports.out.persistence;

import org.fullguys.swing.domain.entities.enums.BeneficiaryState;

public interface CountBeneficiariesByStatusPort {
    long countByState(BeneficiaryState beneficiaryState);
}
