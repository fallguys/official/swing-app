package org.fullguys.swing.domain.usecases.synchro.commands;

import lombok.Data;
import org.fullguys.swing.common.queries.Paginator;
import org.fullguys.swing.domain.entities.WithdrawalPoint;

import java.time.LocalDateTime;

@Data
public class PointCommand {
    private Paginator<WithdrawalPoint> paginator;
    private Long totalActive;
    private LocalDateTime lastUpdated;
}
