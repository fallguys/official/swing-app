package org.fullguys.swing.domain.usecases.scheduling.impl;

import lombok.AllArgsConstructor;
import org.fullguys.swing.common.annotations.UseCase;
import org.fullguys.swing.config.parameter.TimerParameters;
import org.fullguys.swing.domain.entities.Schedule;
import org.fullguys.swing.domain.entities.enums.ScheduleState;
import org.fullguys.swing.domain.entities.enums.ScheduleType;
import org.fullguys.swing.domain.usecases.scheduling.errors.ScheduleInitialDateMustBeAfterTodayError;
import org.fullguys.swing.domain.usecases.scheduling.errors.ScheduleNotAllowBeforeLastOfficialError;
import org.fullguys.swing.domain.usecases.scheduling.errors.ScheduleProcessingError;
import org.fullguys.swing.domain.usecases.scheduling.errors.SynchroProcessingError;
import org.fullguys.swing.domain.usecases.scheduling.ports.in.services.GenerateNewScheduleService;
import org.fullguys.swing.domain.usecases.scheduling.ports.in.services.GetLastOfficialScheduleService;
import org.fullguys.swing.domain.usecases.scheduling.ports.out.persistence.CheckForScheduleProcessingPort;
import org.fullguys.swing.domain.usecases.scheduling.ports.out.persistence.CheckForSynchrosProcessingPort;
import org.fullguys.swing.domain.usecases.scheduling.ports.out.persistence.RegisterSchedulePort;
import org.fullguys.swing.domain.usecases.scheduling.ports.out.scheduler.RegisterRequestToGenerateScheduleBatchPort;

import java.time.*;

@UseCase
@AllArgsConstructor
public class GenerateNewScheduleUseCase implements GenerateNewScheduleService {

    private final GetLastOfficialScheduleService getLastOfficialScheduleService;

    private final CheckForSynchrosProcessingPort checkForSynchrosProcessingPort;
    private final CheckForScheduleProcessingPort checkForScheduleProcessingPort;

    private final RegisterSchedulePort registerSchedulePort;
    private final RegisterRequestToGenerateScheduleBatchPort registerRequestToGenerateScheduleBatchPort;
    private final TimerParameters timerParameters;

    @Override
    public void generate(Schedule schedule) {
        //this.validateInitialDateIsAfterToday(schedule);
        this.validateIfExistCurrentSynchroProcessing();
        this.validateIfExistCurrentScheduleProcessing();

        if (schedule.getType() == ScheduleType.OFFICIAL) {
            this.validateInitNewScheduleAfterLastOfficial(schedule);
        }

        schedule.setCreationDate(LocalDateTime.now(ZoneId.of(timerParameters.getZone())));
        schedule.setState(ScheduleState.PROCESSING);
        var saved = registerSchedulePort.registerSchedule(schedule);
        registerRequestToGenerateScheduleBatchPort.registerRequestToGenerateSchedule(saved.getId());
    }

    private void validateInitialDateIsAfterToday(Schedule schedule) {
        var today = LocalDate.now(ZoneId.of(timerParameters.getZone()));
        if (!schedule.getInitialDate().isAfter(today)) {
            throw new ScheduleInitialDateMustBeAfterTodayError(schedule.getInitialDate(), ZoneId.of(timerParameters.getZone()));
        }
    }

    private void validateIfExistCurrentSynchroProcessing() {
        if (checkForSynchrosProcessingPort.checkForSynchrosProcessing()) {
            throw new SynchroProcessingError();
        }
    }

    private void validateIfExistCurrentScheduleProcessing() {
        if (checkForScheduleProcessingPort.checkForScheduleProcessing()) {
            throw new ScheduleProcessingError();
        }
    }

    private void validateInitNewScheduleAfterLastOfficial(Schedule schedule) {
        var lastSchedule = getLastOfficialScheduleService.getLast();
        if (lastSchedule.isEmpty()) {
            return;
        }

        var last = lastSchedule.get();
        if (!schedule.getInitialDate().isAfter(last.getFinalDate())) {
            throw new ScheduleNotAllowBeforeLastOfficialError(schedule.getInitialDate(), last.getFinalDate());
        }
    }
}
