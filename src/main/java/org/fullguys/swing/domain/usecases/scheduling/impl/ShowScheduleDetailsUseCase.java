package org.fullguys.swing.domain.usecases.scheduling.impl;

import lombok.RequiredArgsConstructor;
import org.fullguys.swing.common.annotations.UseCase;
import org.fullguys.swing.domain.entities.Schedule;
import org.fullguys.swing.domain.usecases.scheduling.ports.in.services.ShowScheduleDetailsService;
import org.fullguys.swing.domain.usecases.scheduling.ports.in.services.ShowTotalBeneficiariesForScheduleService;
import org.fullguys.swing.domain.usecases.scheduling.ports.in.services.ShowTotalWithdrawalPointsForScheduleService;
import org.fullguys.swing.domain.usecases.scheduling.ports.out.scheduler.ShowScheduleDetailsPort;
import org.fullguys.swing.domain.usecases.scheduling.ports.out.scheduler.ShowTotalBeneficiariesForSchedulePort;
import org.fullguys.swing.domain.usecases.scheduling.ports.out.scheduler.ShowTotalWithdrawalPointsForSchedulePort;

@UseCase
@RequiredArgsConstructor
public class ShowScheduleDetailsUseCase implements ShowScheduleDetailsService, ShowTotalBeneficiariesForScheduleService, ShowTotalWithdrawalPointsForScheduleService {
    private final ShowScheduleDetailsPort showScheduleDetailsPort;
    private final ShowTotalBeneficiariesForSchedulePort showTotalBeneficiariesForSchedulePort;
    private final ShowTotalWithdrawalPointsForSchedulePort showTotalWithdrawalPointsForSchedulePort;

    @Override
    public Schedule showScheduleDetails(Long scheduleId) {
        return showScheduleDetailsPort.showScheduleDetails(scheduleId);
    }

    @Override
    public Integer showTotalBeneficiariesForSchedule(Long scheduleId) {
        return showTotalBeneficiariesForSchedulePort.showTotalBeneficiariesForSchedule(scheduleId);
    }

    @Override
    public Integer showTotalWithdrawalPointsForSchedule(Long scheduleId) {
        return showTotalWithdrawalPointsForSchedulePort.showTotalWithdrawalPointsForSchedule(scheduleId);
    }
}
