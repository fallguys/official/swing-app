package org.fullguys.swing.domain.usecases.scheduling.ports.out.persistence;

import org.fullguys.swing.domain.entities.Schedule;

public interface UpdateScheduleStatePort {
    void updateScheduleState(Schedule schedule);
}
