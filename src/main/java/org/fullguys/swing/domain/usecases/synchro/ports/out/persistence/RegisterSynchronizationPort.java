package org.fullguys.swing.domain.usecases.synchro.ports.out.persistence;

import org.fullguys.swing.domain.entities.Synchro;

public interface RegisterSynchronizationPort {
    Synchro save(Synchro synchro);
    void saveResult(Synchro synchro, String urlFile);
}
