package org.fullguys.swing.domain.usecases.scheduling.errors;

import org.fullguys.swing.common.formatter.TimeFormatter;

import java.time.LocalDate;

public class InvalidChangeStateForActiveScheduleError extends RuntimeException{
    private final LocalDate finalDate;
    public InvalidChangeStateForActiveScheduleError(LocalDate finalDate){
        super(String.format("No se puede poner vigente este cronograma, ya que hay uno corriendo con fecha final (%s)",
                finalDate.format(TimeFormatter.DATE)));
        this.finalDate = finalDate;
    }
}
