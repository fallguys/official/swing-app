package org.fullguys.swing.domain.usecases.feedback.errors;

public class BeneficiaryAlreadyRetiredBonusError extends RuntimeException {
    public BeneficiaryAlreadyRetiredBonusError(String codeBeneficiary){
        super(String.format("El beneficiario con código %s ya cobró su bono", codeBeneficiary));
    }
}
