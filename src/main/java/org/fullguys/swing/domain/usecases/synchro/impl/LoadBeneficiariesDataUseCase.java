package org.fullguys.swing.domain.usecases.synchro.impl;

import joinery.DataFrame;
import lombok.RequiredArgsConstructor;
import org.fullguys.swing.common.annotations.UseCase;
import org.fullguys.swing.config.parameter.TimerParameters;
import org.fullguys.swing.domain.entities.Synchro;
import org.fullguys.swing.domain.entities.SynchroResult;
import org.fullguys.swing.domain.entities.enums.SynchroFileType;
import org.fullguys.swing.domain.entities.enums.SynchroState;
import org.fullguys.swing.domain.usecases.scheduling.ports.out.persistence.CheckForScheduleProcessingPort;
import org.fullguys.swing.domain.usecases.scheduling.ports.out.persistence.CheckForSynchrosProcessingPort;
import org.fullguys.swing.domain.usecases.synchro.errors.ScheduleProcessingError;
import org.fullguys.swing.domain.usecases.synchro.errors.SynchroProcessingError;
import org.fullguys.swing.domain.usecases.synchro.ports.in.services.LoadBeneficiariesDataService;
import org.fullguys.swing.domain.usecases.synchro.ports.out.persistence.RegisterSynchronizationPort;
import org.fullguys.swing.domain.usecases.synchro.ports.out.persistence.SaveBeneficiaryFilePort;
import org.fullguys.swing.domain.usecases.synchro.ports.out.synchronizer.RegisterBeneficiariesDataBatchPort;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.time.LocalDateTime;
import java.time.ZoneId;

@RequiredArgsConstructor
@UseCase
public class LoadBeneficiariesDataUseCase implements LoadBeneficiariesDataService {


    private final RegisterBeneficiariesDataBatchPort registerBeneficiariesDataBatchPort;
    private final RegisterSynchronizationPort registerSynchronizationPort;
    private final SaveBeneficiaryFilePort saveBeneficiaryFilePort;
    private final CheckForScheduleProcessingPort checkForScheduleProcessingPort;
    private final CheckForSynchrosProcessingPort checkForSynchrosProcessingPort;
    private final TimerParameters timerParameters;

    @Override
    public void loadBeneficiariesDataService(MultipartFile beneficiaries, Long adminId){

        if(checkForSynchrosProcessingPort.checkForSynchrosProcessing()) throw new SynchroProcessingError();

        if(checkForScheduleProcessingPort.checkForScheduleProcessing()) throw new ScheduleProcessingError();

        Synchro synchro = Synchro.builder()
                .fileType(SynchroFileType.BENEFICIARY)
                .initialDate(LocalDateTime.now(ZoneId.of(timerParameters.getZone())))
                .result(new SynchroResult())
                .state(SynchroState.IN_PROCESS)
                .build();

        String urlFile = saveBeneficiaryFilePort.saveBeneficiaryFile(beneficiaries);

        Synchro synchroSaved = registerSynchronizationPort.save(synchro);

        registerSynchronizationPort.saveResult(synchroSaved, urlFile);

        registerBeneficiariesDataBatchPort.registerBeneficiariesDataBatch();

    }

    public boolean validateNumberOfColumns(File file) throws IOException {
        var dataSet = DataFrame.readCsv(new FileInputStream(file), ";");
        return false;
    }

    public boolean validateNamesOfColumns(File file) throws IOException {
        var dataSet = DataFrame.readCsv(new FileInputStream(file), ";");
        return false;
    }

    public boolean validateEmptyRegistriesOfColumns(File file) throws IOException {
        var dataSet = DataFrame.readCsv(new FileInputStream(file), ";");
        return false;
    }
}
