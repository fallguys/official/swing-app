package org.fullguys.swing.domain.usecases.scheduling.info;

import lombok.Builder;
import lombok.Data;

import java.time.LocalDate;

@Data
@Builder
public class InfoBeforeGenerateSchedule {
    private long activeBeneficiaries;
    private long activeWithdrawalPoints;
    private boolean synchrosProcessing;
    private boolean scheduleProcessing;
    private LocalDate officialScheduleFinalDate;
}
