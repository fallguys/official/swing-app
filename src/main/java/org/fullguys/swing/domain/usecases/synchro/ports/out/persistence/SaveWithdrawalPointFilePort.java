package org.fullguys.swing.domain.usecases.synchro.ports.out.persistence;

import org.springframework.web.multipart.MultipartFile;

public interface SaveWithdrawalPointFilePort {
    String saveWithdrawalPointFile(MultipartFile file);
}
