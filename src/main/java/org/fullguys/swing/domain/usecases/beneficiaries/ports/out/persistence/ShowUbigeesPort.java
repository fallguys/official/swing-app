package org.fullguys.swing.domain.usecases.beneficiaries.ports.out.persistence;

import org.fullguys.swing.domain.entities.Ubigee;
import org.fullguys.swing.domain.entities.enums.UbigeeType;

import java.util.List;

public interface ShowUbigeesPort {
    List<Ubigee> showUbigees(Long id, UbigeeType ubigeeType);
}
