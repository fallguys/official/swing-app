package org.fullguys.swing.domain.usecases.scheduling.errors;

import lombok.Getter;
import org.fullguys.swing.common.formatter.TimeFormatter;

import java.time.LocalDate;
import java.time.ZoneId;

@Getter
public class ScheduleInitialDateMustBeAfterTodayError extends RuntimeException {
    private final LocalDate initialDate;

    public ScheduleInitialDateMustBeAfterTodayError(LocalDate initialDate, ZoneId zoneId) {
        super(String.format("La fecha inicial (%s) del cronograma debe ser después de la fecha actual de hoy (%s)",
                initialDate.format(TimeFormatter.DATE),
                LocalDate.now(zoneId).format(TimeFormatter.DATE)));

        this.initialDate = initialDate;
    }
}
