package org.fullguys.swing.domain.usecases.beneficiaries.ports.in.services;

import org.fullguys.swing.domain.entities.Ubigee;
import org.fullguys.swing.domain.entities.enums.UbigeeType;

import java.util.List;

public interface ShowUbigeesService {
    List<Ubigee> showUbigees(Long id, UbigeeType ubigeeType);

}
