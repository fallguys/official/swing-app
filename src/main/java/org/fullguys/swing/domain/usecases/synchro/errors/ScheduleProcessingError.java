package org.fullguys.swing.domain.usecases.synchro.errors;

public class ScheduleProcessingError extends RuntimeException{
    public ScheduleProcessingError() {
        super("No se puede sincronizar este recurso porque se está generando un cronograma");
    }
}
