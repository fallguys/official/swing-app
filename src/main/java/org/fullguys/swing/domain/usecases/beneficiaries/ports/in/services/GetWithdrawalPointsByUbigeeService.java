package org.fullguys.swing.domain.usecases.beneficiaries.ports.in.services;

import org.fullguys.swing.domain.entities.WithdrawalPoint;

import java.util.List;

public interface GetWithdrawalPointsByUbigeeService {
    List<WithdrawalPoint> getWithdrawalPointsByUbigee(String ubigee);
}
