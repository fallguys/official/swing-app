package org.fullguys.swing.domain.usecases.feedback.commands;

import lombok.Data;

import java.time.LocalDateTime;

@Data
public class CheckBonusCommand {
    private Long idBeneficiary;
    private Long idParticipantBeneficiary;
    private Long idSchedule;
    private Long idParticipantPoint;
    private Boolean isPointAssigned;
    private String namePointAssigned;
    private Boolean isValidateDate;
    private LocalDateTime initialDateAssigned;
    private LocalDateTime finalDateAssigned;
    private Boolean retiredBonus;
    private LocalDateTime dateRetiredBonus;
}
