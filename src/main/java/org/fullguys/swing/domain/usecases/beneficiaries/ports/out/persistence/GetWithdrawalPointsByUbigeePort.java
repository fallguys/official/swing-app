package org.fullguys.swing.domain.usecases.beneficiaries.ports.out.persistence;

import org.fullguys.swing.domain.entities.WithdrawalPoint;

import java.util.List;

public interface GetWithdrawalPointsByUbigeePort {
    List<WithdrawalPoint> getWithdrawalPointsByUbigee(String ubigee);
}
