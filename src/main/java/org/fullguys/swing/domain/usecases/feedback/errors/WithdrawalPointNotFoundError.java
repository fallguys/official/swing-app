package org.fullguys.swing.domain.usecases.feedback.errors;

public class WithdrawalPointNotFoundError extends RuntimeException{
    public WithdrawalPointNotFoundError(String codeWithdrawalPoint){
        super(String.format("No se ha encontrado el punto de retiro participante con código %s", codeWithdrawalPoint));
    }
}
