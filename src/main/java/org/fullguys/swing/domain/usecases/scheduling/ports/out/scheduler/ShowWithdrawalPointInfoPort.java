package org.fullguys.swing.domain.usecases.scheduling.ports.out.scheduler;

import org.fullguys.swing.common.queries.Filters;
import org.fullguys.swing.common.queries.Paginator;
import org.fullguys.swing.domain.entities.WithdrawalPoint;

public interface ShowWithdrawalPointInfoPort {
    Paginator<WithdrawalPoint> showBeneficiariesInfo(Filters filters);
}
