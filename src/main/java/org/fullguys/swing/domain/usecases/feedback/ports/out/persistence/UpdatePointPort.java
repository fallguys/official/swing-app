package org.fullguys.swing.domain.usecases.feedback.ports.out.persistence;

public interface UpdatePointPort {
    void updatePoint(Long idPoint);
}
