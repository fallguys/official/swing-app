package org.fullguys.swing.domain.usecases.beneficiaries.ports.out.persistence;

import org.fullguys.swing.common.queries.Filters;
import org.fullguys.swing.common.queries.Paginator;
import org.fullguys.swing.domain.entities.WithdrawalPoint;

public interface ShowWithdrawalPointsForUbigeePort {
    Paginator<WithdrawalPoint> showWithdrawalPoints(Filters filters);
}
