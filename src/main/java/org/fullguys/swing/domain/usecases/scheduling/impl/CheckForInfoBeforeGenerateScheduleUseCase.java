package org.fullguys.swing.domain.usecases.scheduling.impl;

import lombok.RequiredArgsConstructor;
import org.fullguys.swing.common.annotations.UseCase;
import org.fullguys.swing.domain.entities.enums.BeneficiaryState;
import org.fullguys.swing.domain.entities.enums.WithdrawalPointState;
import org.fullguys.swing.domain.usecases.synchro.ports.out.persistence.CountBeneficiariesByStatusPort;
import org.fullguys.swing.domain.usecases.scheduling.info.InfoBeforeGenerateSchedule;
import org.fullguys.swing.domain.usecases.scheduling.ports.in.services.CheckForInfoBeforeGenerateScheduleService;
import org.fullguys.swing.domain.usecases.scheduling.ports.in.services.GetLastOfficialScheduleService;
import org.fullguys.swing.domain.usecases.scheduling.ports.out.persistence.CheckForScheduleProcessingPort;
import org.fullguys.swing.domain.usecases.scheduling.ports.out.persistence.CheckForSynchrosProcessingPort;
import org.fullguys.swing.domain.usecases.synchro.ports.out.persistence.CountWithdrawalPointsByStatusPort;

@UseCase
@RequiredArgsConstructor
public class CheckForInfoBeforeGenerateScheduleUseCase implements CheckForInfoBeforeGenerateScheduleService {

    private final GetLastOfficialScheduleService getLastOfficialScheduleService;
    private final CheckForSynchrosProcessingPort checkForSynchrosProcessingPort;
    private final CheckForScheduleProcessingPort checkForScheduleProcessingPort;
    private final CountBeneficiariesByStatusPort countBeneficiariesByStatusPort;
    private final CountWithdrawalPointsByStatusPort countWithdrawalPointsByStatusPort;

    @Override
    public InfoBeforeGenerateSchedule checkForInfoBeforeGenerateSchedule() {
        var existSynchrosProcessing = checkForSynchrosProcessingPort.checkForSynchrosProcessing();
        var existScheduleProcessing = checkForScheduleProcessingPort.checkForScheduleProcessing();
        var activeBeneficiaries = countBeneficiariesByStatusPort.countByState(BeneficiaryState.ACTIVE);
        var activePoints = countWithdrawalPointsByStatusPort.countByState(WithdrawalPointState.ACTIVE);
        var lastOfficialSchedule = getLastOfficialScheduleService.getLast();

        var info = InfoBeforeGenerateSchedule
                .builder()
                .synchrosProcessing(existSynchrosProcessing)
                .scheduleProcessing(existScheduleProcessing)
                .activeBeneficiaries(activeBeneficiaries)
                .activeWithdrawalPoints(activePoints);

        if (lastOfficialSchedule.isPresent()) {
            return info
                    .officialScheduleFinalDate(lastOfficialSchedule.get().getFinalDate())
                    .build();
        }

        return info.build();
    }
}
