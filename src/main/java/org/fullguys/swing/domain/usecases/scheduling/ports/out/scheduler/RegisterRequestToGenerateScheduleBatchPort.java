package org.fullguys.swing.domain.usecases.scheduling.ports.out.scheduler;

public interface RegisterRequestToGenerateScheduleBatchPort {
    void registerRequestToGenerateSchedule(Long scheduleId);
}
