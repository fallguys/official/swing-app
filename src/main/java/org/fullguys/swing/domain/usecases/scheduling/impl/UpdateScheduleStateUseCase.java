package org.fullguys.swing.domain.usecases.scheduling.impl;

import lombok.RequiredArgsConstructor;
import org.fullguys.swing.common.annotations.UseCase;
import org.fullguys.swing.config.parameter.TimerParameters;
import org.fullguys.swing.domain.entities.Schedule;
import org.fullguys.swing.domain.entities.enums.ScheduleState;
import org.fullguys.swing.domain.entities.enums.ScheduleType;
import org.fullguys.swing.domain.usecases.scheduling.errors.*;
import org.fullguys.swing.domain.usecases.scheduling.ports.in.services.UpdateScheduleStateService;
import org.fullguys.swing.domain.usecases.scheduling.ports.out.persistence.GetLastScheduleRegisteredPort;
import org.fullguys.swing.domain.usecases.scheduling.ports.out.persistence.GetSchedulesPort;
import org.fullguys.swing.domain.usecases.scheduling.ports.out.persistence.UpdateScheduleStatePort;

import java.time.LocalDate;
import java.time.ZoneId;
import java.util.function.Consumer;

@UseCase
@RequiredArgsConstructor
public class UpdateScheduleStateUseCase implements UpdateScheduleStateService {

    private final UpdateScheduleStatePort updateScheduleStatePort;
    private final GetSchedulesPort getSchedulesPort;
    private final GetLastScheduleRegisteredPort getLastScheduleRegisteredPort;
    private final TimerParameters timerParameters;

    @Override
    public void updateScheduleState(Schedule scheduleDTO) {
        var stateToAsign = scheduleDTO.getState();
        var scheduleToUpdate = getSchedulesPort
                .getSchedule(scheduleDTO.getId())
                .orElseThrow(ScheduleNotFoundError::new);

        this.checkStateChanging(scheduleToUpdate, stateToAsign);
        this.checkUpdateIfTestScheduleWillBecomeAnOfficialProgrammed(scheduleToUpdate, stateToAsign);
        this.checkUpdateIfOfficialScheduleWillBecomeAnTestAvailable(scheduleToUpdate, stateToAsign);
        this.checkUpdateIfOfficialScheduleWillBeActive(scheduleToUpdate, stateToAsign);
        this.checkUpdateIfOfficialScheduleWillFinish(scheduleToUpdate, stateToAsign);

        scheduleToUpdate.setState(stateToAsign);
        updateScheduleStatePort.updateScheduleState(scheduleToUpdate);
    }

    private void checkUpdateIfOfficialScheduleWillFinish(Schedule scheduleToUpdate, ScheduleState stateToAsign) {
        var currentType = scheduleToUpdate.getType();
        var currentFinalDate = scheduleToUpdate.getFinalDate();

        if (currentType.isOfficial() && stateToAsign.is(ScheduleState.FINISHED)) {
            var timeRightNow = LocalDate.now(ZoneId.of(timerParameters.getZone()));

            if (currentFinalDate.isAfter(timeRightNow)) {
                throw new InvalidChangeStateForDateFinalInFinishedScheduleError(currentFinalDate);
            }
        }
    }

    private void checkUpdateIfOfficialScheduleWillBeActive(Schedule scheduleToUpdate, ScheduleState stateToAsign) {
        var currentType = scheduleToUpdate.getType();

        if(currentType.isOfficial() && stateToAsign.is(ScheduleState.ACTIVE)) {
            getLastScheduleRegisteredPort
                    .getLastRegistered(ScheduleType.OFFICIAL, ScheduleState.ACTIVE)
                    .ifPresent((lastSchedule) -> {
                        throw new InvalidChangeStateForActiveScheduleError(lastSchedule.getFinalDate());
                    });

            var timeRightNow = LocalDate.now(ZoneId.of(timerParameters.getZone()));
            var initialScheduleDate = scheduleToUpdate.getInitialDate();

            if(initialScheduleDate.isAfter(timeRightNow)) {
                throw new InvalidChangeStateForDateInitialInActiveScheduleError(initialScheduleDate);
            }

            scheduleToUpdate.setState(stateToAsign);
            updateScheduleStatePort.updateScheduleState(scheduleToUpdate);
        }
    }

    private void checkUpdateIfOfficialScheduleWillBecomeAnTestAvailable(Schedule scheduleToUpdate, ScheduleState stateToAsign) {
        var currentType = scheduleToUpdate.getType();

        if(currentType.isOfficial() && stateToAsign.is(ScheduleState.AVAILABLE)){
            scheduleToUpdate.setState(stateToAsign);
            scheduleToUpdate.setType(ScheduleType.TEST);
            updateScheduleStatePort.updateScheduleState(scheduleToUpdate);
        }
    }

    private void checkUpdateIfTestScheduleWillBecomeAnOfficialProgrammed(Schedule scheduleToUpdate, ScheduleState stateToAsign) {
        var currentType = scheduleToUpdate.getType();

        if(currentType.isTest() && stateToAsign.is(ScheduleState.PROGRAMMED)) {
            var activeSchedule = getLastScheduleRegisteredPort.getLastRegistered(ScheduleType.OFFICIAL, ScheduleState.ACTIVE);
            var programmedSchedule = getLastScheduleRegisteredPort.getLastRegistered(ScheduleType.OFFICIAL, ScheduleState.PROGRAMMED);

            Consumer<Schedule> checkIfInitialDateIsAfterFinishSchedule = schedule -> {
                var finalActiveSchedule = schedule.getFinalDate();

                if (!scheduleToUpdate.getInitialDate().isAfter(finalActiveSchedule)) {
                    throw new InvalidChangeStateForDateInAvailableScheduleError(finalActiveSchedule);
                }
            };

            activeSchedule.ifPresent(checkIfInitialDateIsAfterFinishSchedule);
            programmedSchedule.ifPresent(checkIfInitialDateIsAfterFinishSchedule);

            scheduleToUpdate.setState(stateToAsign);
            scheduleToUpdate.setType(ScheduleType.OFFICIAL);
            updateScheduleStatePort.updateScheduleState(scheduleToUpdate);
        }
    }

    private void checkStateChanging(Schedule schedule, ScheduleState stateToAsign) {
        if (!canChangeState(schedule, stateToAsign)) {
            throw new InvalidChangeStatePerTypeError();
        }
    }

    private boolean canChangeState(Schedule schedule, ScheduleState stateToAsign) {
        var currentType = schedule.getType();
        var currentState = schedule.getState();

        // TEST SCHEDULE
        if(currentType.isTest()) {

            if (currentState.is(ScheduleState.PROCESSING)) {
                return stateToAsign.isIn(ScheduleState.AVAILABLE);
            }

            if (currentState.is(ScheduleState.AVAILABLE)) {
                return stateToAsign.isIn(ScheduleState.PROGRAMMED, ScheduleState.DISCARTED);
            }

            if (currentState.is(ScheduleState.DISCARTED)) {
                return stateToAsign.is(ScheduleState.AVAILABLE);
            }
        }

        // OFFICIAL SCHEDULE
        if (currentState.is(ScheduleState.PROCESSING)) {
            return stateToAsign.is(ScheduleState.PROGRAMMED);
        }

        if (currentState.is(ScheduleState.PROGRAMMED)) {
            return stateToAsign.isIn(ScheduleState.CANCELED, ScheduleState.ACTIVE);
        }

        if (currentState.is(ScheduleState.ACTIVE)) {
            return stateToAsign.is(ScheduleState.FINISHED);
        }

        if (currentState.is(ScheduleState.CANCELED)) {
            return stateToAsign.is(ScheduleState.AVAILABLE);
        }

        // FINISHED official schedules cannot change their state
        return false;
    }
}
