package org.fullguys.swing.domain.usecases.incidents.impl;

import lombok.RequiredArgsConstructor;
import org.fullguys.swing.common.annotations.UseCase;
import org.fullguys.swing.domain.entities.IncidentType;
import org.fullguys.swing.domain.entities.enums.IncidentTypeState;
import org.fullguys.swing.domain.usecases.incidents.ports.in.services.RegisterIncidentTypeService;
import org.fullguys.swing.domain.usecases.incidents.ports.out.persistence.SaveIncidentTypePort;

import java.util.Date;

@UseCase
@RequiredArgsConstructor
public class RegisterIncidentTypeUseCase implements RegisterIncidentTypeService {

    private final SaveIncidentTypePort saveIncidentTypePort;

    @Override
    public IncidentType register(IncidentType incidentType) {
        incidentType.setState(IncidentTypeState.ENABLED);
        incidentType.setCreatedAt(new Date());
        return saveIncidentTypePort.save(incidentType);
    }
}
