package org.fullguys.swing.domain.usecases.feedback.impl;

import lombok.RequiredArgsConstructor;
import org.fullguys.swing.common.annotations.UseCase;
import org.fullguys.swing.config.parameter.TimerParameters;
import org.fullguys.swing.domain.usecases.feedback.commands.CheckBonusCommand;
import org.fullguys.swing.domain.usecases.feedback.errors.BeneficiaryParticipantNotFoundError;
import org.fullguys.swing.domain.usecases.feedback.errors.CurrentScheduleNotFoundError;
import org.fullguys.swing.domain.usecases.feedback.ports.in.services.CheckPickUpBonusService;
import org.fullguys.swing.domain.usecases.feedback.ports.out.persistence.*;

import java.time.LocalDateTime;
import java.time.ZoneId;

@UseCase
@RequiredArgsConstructor
public class CheckPickUpBonusUseCase implements CheckPickUpBonusService {

    private final GetCurrentSchedulePort getCurrentSchedulePort;
    private final GetParticipantBeneficiaryPort getParticipantBeneficiaryPort;
    private final GetAssignmentPort getAssignmentPort;
    private final TimerParameters timerParameters;

    @Override
    public CheckBonusCommand checkPickUpBonus(String codeBeneficiary, String codePoint) {

        var schedule = getCurrentSchedulePort
                .getCurrentSchedule()
                .orElseThrow(CurrentScheduleNotFoundError::new);

        var beneficiary = getParticipantBeneficiaryPort
                .getParticipantBeneficiary(codeBeneficiary, schedule.getId())
                .orElseThrow(() -> new BeneficiaryParticipantNotFoundError(codeBeneficiary));


        var idParticipantBeneficiary = getParticipantBeneficiaryPort
                .getIdParticipantBeneficiary(beneficiary.getId(), schedule.getId());

        var assigment = getAssignmentPort.getAssignment(idParticipantBeneficiary);
        var infoBonus = assigment.getInfoBonus();

        CheckBonusCommand checkBonusCommand = new CheckBonusCommand();

        checkBonusCommand.setIdBeneficiary(beneficiary.getId());
        checkBonusCommand.setIdParticipantBeneficiary(idParticipantBeneficiary);
        checkBonusCommand.setIdSchedule(schedule.getId());

        if(assigment.getWithdrawalPoint().getCode().equals(codePoint)){
            checkBonusCommand.setIsPointAssigned(true);
        }
        else {
            checkBonusCommand.setIsPointAssigned(false);
        }

        checkBonusCommand.setNamePointAssigned(assigment.getWithdrawalPoint().getName());

        if(infoBonus.getBonusRetired()){
            checkBonusCommand.setRetiredBonus(true);
            checkBonusCommand.setDateRetiredBonus(infoBonus.getBonusRetiredDate());
        }
        else {
            checkBonusCommand.setRetiredBonus(false);
        }

        checkBonusCommand.setInitialDateAssigned(infoBonus.getAssignedInitialDate());
        checkBonusCommand.setFinalDateAssigned(infoBonus.getAssignedFinalDate());

        var todayDate = LocalDateTime.now(ZoneId.of(timerParameters.getZone()));

        if(!infoBonus.getAssignedInitialDate().isAfter(todayDate) && !infoBonus.getAssignedFinalDate().isBefore(todayDate)){
           checkBonusCommand.setIsValidateDate(true);
        }
        else {
            checkBonusCommand.setIsValidateDate(false);
        }

        return checkBonusCommand;

    }
}
