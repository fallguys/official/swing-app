package org.fullguys.swing.domain.usecases.user.ports.out.persistence;

import org.fullguys.swing.domain.entities.Administrator;

import java.util.Optional;

public interface GetAdminPort {
    Optional<Administrator> getAdmin(String email);
}
