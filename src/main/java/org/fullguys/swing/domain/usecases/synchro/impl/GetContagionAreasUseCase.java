package org.fullguys.swing.domain.usecases.synchro.impl;

import lombok.RequiredArgsConstructor;
import org.fullguys.swing.common.annotations.UseCase;
import org.fullguys.swing.common.queries.Filters;
import org.fullguys.swing.common.queries.Paginator;
import org.fullguys.swing.domain.entities.ContagionArea;
import org.fullguys.swing.domain.entities.enums.SynchroFileType;
import org.fullguys.swing.domain.entities.enums.SynchroState;
import org.fullguys.swing.domain.usecases.synchro.commands.ContagionAreasCommand;
import org.fullguys.swing.domain.usecases.synchro.ports.in.services.GetContagionAreaService;
import org.fullguys.swing.domain.usecases.synchro.ports.out.persistence.CountContagionAreasPort;
import org.fullguys.swing.domain.usecases.synchro.ports.out.persistence.GetSynchroPort;
import org.fullguys.swing.domain.usecases.synchro.ports.out.persistence.ListContagionAreaPort;

import java.util.List;
@RequiredArgsConstructor
@UseCase
public class GetContagionAreasUseCase implements GetContagionAreaService {

    private final ListContagionAreaPort listContagionAreaPort;
    private final CountContagionAreasPort countContagionAreasPort;
    private final GetSynchroPort getSynchroPort;

    @Override
    public ContagionAreasCommand getContagionAreas(Filters filters) {

        var contagionAreas = listContagionAreaPort.listContagionAreas(filters);
        var synchro = getSynchroPort.getLastActiveSynchro(SynchroFileType.CONTAGION_AREA, SynchroState.COMPLETED);
        var actives = countContagionAreasPort.count();

        ContagionAreasCommand contagionAreasCommand = new ContagionAreasCommand();
        contagionAreasCommand.setPaginator(contagionAreas);
        if(synchro.isPresent()){
            contagionAreasCommand.setLastUpdated(synchro.get().getInitialDate());
        }
        contagionAreasCommand.setTotalActive(actives);

        return contagionAreasCommand;
    }
}
