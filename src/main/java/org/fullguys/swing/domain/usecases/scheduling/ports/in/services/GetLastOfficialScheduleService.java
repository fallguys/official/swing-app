package org.fullguys.swing.domain.usecases.scheduling.ports.in.services;

import org.fullguys.swing.domain.entities.Schedule;

import java.util.Optional;

public interface GetLastOfficialScheduleService {
    Optional<Schedule> getLast();
}
