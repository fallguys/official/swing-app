package org.fullguys.swing.domain.usecases.incidents.ports.out.persistence;

import org.fullguys.swing.domain.entities.IncidentType;

public interface SaveIncidentTypePort {
    IncidentType save(IncidentType incidentType);
}
