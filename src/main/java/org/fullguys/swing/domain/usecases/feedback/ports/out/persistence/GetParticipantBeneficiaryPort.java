package org.fullguys.swing.domain.usecases.feedback.ports.out.persistence;

import org.fullguys.swing.domain.entities.Beneficiary;

import java.util.Optional;

public interface GetParticipantBeneficiaryPort {
    Optional<Beneficiary> getParticipantBeneficiary(String codeBeneficiary, Long idSchedule);
    Long getIdParticipantBeneficiary(Long idBeneficiary, Long idSchedule);
}
