package org.fullguys.swing.domain.usecases.synchro.impl;

import lombok.RequiredArgsConstructor;
import org.fullguys.swing.common.annotations.UseCase;
import org.fullguys.swing.config.parameter.TimerParameters;
import org.fullguys.swing.domain.entities.Synchro;
import org.fullguys.swing.domain.entities.enums.SynchroFileType;
import org.fullguys.swing.domain.entities.enums.SynchroState;
import org.fullguys.swing.domain.usecases.scheduling.ports.out.persistence.CheckForScheduleProcessingPort;
import org.fullguys.swing.domain.usecases.scheduling.ports.out.persistence.CheckForSynchrosProcessingPort;
import org.fullguys.swing.domain.usecases.synchro.errors.ScheduleProcessingError;
import org.fullguys.swing.domain.usecases.synchro.errors.SynchroProcessingError;
import org.fullguys.swing.domain.usecases.synchro.ports.in.services.LoadWithdrawalPointsDataService;
import org.fullguys.swing.domain.usecases.synchro.ports.out.persistence.RegisterSynchronizationPort;
import org.fullguys.swing.domain.usecases.synchro.ports.out.persistence.SaveWithdrawalPointFilePort;
import org.fullguys.swing.domain.usecases.synchro.ports.out.synchronizer.RegisterWithdrawalPointsDataBatchPort;
import org.springframework.web.multipart.MultipartFile;

import java.time.LocalDateTime;
import java.time.ZoneId;

@RequiredArgsConstructor
@UseCase
public class LoadWithdrawalPointsDataUseCase implements LoadWithdrawalPointsDataService {

    private final RegisterWithdrawalPointsDataBatchPort registerWithdrawalPointsDataBatchPort;
    private final RegisterSynchronizationPort registerSynchronizationPort;
    private final SaveWithdrawalPointFilePort saveWithdrawalPointFilePort;
    private final CheckForScheduleProcessingPort checkForScheduleProcessingPort;
    private final CheckForSynchrosProcessingPort checkForSynchrosProcessingPort;
    private final TimerParameters timerParameters;

    @Override
    public void loadWithdrawalPointsData(MultipartFile withdrawalPointFile, Long adminId) {

        if(checkForSynchrosProcessingPort.checkForSynchrosProcessing()) throw new SynchroProcessingError();

        if(checkForScheduleProcessingPort.checkForScheduleProcessing()) throw new ScheduleProcessingError();

        Synchro synchro = Synchro.builder()
                .fileType(SynchroFileType.WITHDRAWAL_POINT)
                .initialDate(LocalDateTime.now(ZoneId.of(timerParameters.getZone())))
                .state(SynchroState.IN_PROCESS)
                .build();

        String urlFile = saveWithdrawalPointFilePort.saveWithdrawalPointFile(withdrawalPointFile);

        Synchro synchroSaved = registerSynchronizationPort.save(synchro);

        registerSynchronizationPort.saveResult(synchroSaved, urlFile);

        registerWithdrawalPointsDataBatchPort.registerWithdrawalPointsDataBatch();

    }
}
