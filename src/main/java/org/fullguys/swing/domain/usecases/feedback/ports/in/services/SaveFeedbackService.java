package org.fullguys.swing.domain.usecases.feedback.ports.in.services;

public interface SaveFeedbackService {
    void saveFeedback(String codeBeneficiary, String codePoint);
}
