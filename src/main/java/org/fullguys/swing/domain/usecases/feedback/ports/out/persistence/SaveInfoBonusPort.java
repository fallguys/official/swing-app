package org.fullguys.swing.domain.usecases.feedback.ports.out.persistence;

import org.fullguys.swing.domain.entities.InfoBonus;

public interface SaveInfoBonusPort {

    InfoBonus saveInfoBonus(Long idParticipantBeneficiary);

}
