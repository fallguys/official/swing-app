package org.fullguys.swing.domain.usecases.feedback.ports.in.services;

public interface SaveInfoBonusService {
    void saveInfoBonus(String codeBeneficiary, String codePoint);
}
