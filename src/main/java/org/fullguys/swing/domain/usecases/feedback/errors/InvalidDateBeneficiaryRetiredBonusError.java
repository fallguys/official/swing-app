package org.fullguys.swing.domain.usecases.feedback.errors;

public class InvalidDateBeneficiaryRetiredBonusError extends RuntimeException{
    public InvalidDateBeneficiaryRetiredBonusError(){
        super("El beneficiario no puede recoger el bono en estos momentos");
    }
}
