package org.fullguys.swing.domain.usecases.feedback.ports.out.persistence;

import org.fullguys.swing.domain.entities.Schedule;

import java.util.Optional;

public interface GetCurrentSchedulePort {
    Optional<Schedule> getCurrentSchedule();
}
