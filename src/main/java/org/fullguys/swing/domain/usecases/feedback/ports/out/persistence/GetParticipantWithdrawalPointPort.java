package org.fullguys.swing.domain.usecases.feedback.ports.out.persistence;

import org.fullguys.swing.domain.entities.Assignment;
import org.fullguys.swing.domain.entities.WithdrawalPoint;

import java.util.Optional;

public interface GetParticipantWithdrawalPointPort {
    Optional<WithdrawalPoint> getParticipantWithdrawalPoint(String codeWithdrawalPoint, Long idSchedule);
    Long getIdParticipantPoint(Long idPoint, Long idSchedule);
    WithdrawalPoint getPoint(String code);
}
