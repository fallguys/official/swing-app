package org.fullguys.swing.domain.usecases.scheduling.ports.out.persistence;

import org.fullguys.swing.domain.entities.Schedule;

public interface RegisterSchedulePort {
    Schedule registerSchedule(Schedule schedule);
}
