package org.fullguys.swing.domain.usecases.scheduling.ports.out.persistence;

import org.fullguys.swing.common.queries.Filters;
import org.fullguys.swing.common.queries.Paginator;
import org.fullguys.swing.domain.entities.Schedule;
import org.fullguys.swing.domain.entities.enums.ScheduleType;

import java.util.Optional;

public interface GetSchedulesPort {
    Paginator<Schedule> getSchedules(ScheduleType type, Filters filters);
    Optional<Schedule> getSchedule(Long idSchedule);
}
