package org.fullguys.swing.domain.usecases.beneficiaries.ports.in.services;

import org.fullguys.swing.domain.entities.Assignment;

public interface ShowInfoBonusService {
    Assignment showInfoBonus(String search);
}
