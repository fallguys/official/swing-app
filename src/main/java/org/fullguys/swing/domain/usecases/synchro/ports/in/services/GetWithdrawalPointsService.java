package org.fullguys.swing.domain.usecases.synchro.ports.in.services;

import org.fullguys.swing.common.queries.Filters;
import org.fullguys.swing.common.queries.Paginator;
import org.fullguys.swing.domain.entities.WithdrawalPoint;
import org.fullguys.swing.domain.usecases.synchro.commands.PointCommand;

import java.util.List;

public interface GetWithdrawalPointsService {
    PointCommand getWithdrawalPoints(Filters filters);
}
