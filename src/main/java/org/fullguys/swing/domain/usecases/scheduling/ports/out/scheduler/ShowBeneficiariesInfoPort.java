package org.fullguys.swing.domain.usecases.scheduling.ports.out.scheduler;

import org.fullguys.swing.common.queries.Filters;
import org.fullguys.swing.common.queries.Paginator;
import org.fullguys.swing.domain.entities.Assignment;

public interface ShowBeneficiariesInfoPort {
    Paginator<Assignment> showBeneficiariesInfo(Filters filters);
}
