package org.fullguys.swing.domain.usecases.scheduling.errors;

public class InvalidStateScheduleByTypeRefactor extends RuntimeException{
    public InvalidStateScheduleByTypeRefactor(){
        super("No es posible cambiar el estado del cronograma por el tipo al que pertenece");
    }
}
