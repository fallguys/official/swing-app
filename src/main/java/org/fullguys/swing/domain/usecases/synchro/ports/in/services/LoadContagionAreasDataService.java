package org.fullguys.swing.domain.usecases.synchro.ports.in.services;

import org.springframework.web.multipart.MultipartFile;

public interface LoadContagionAreasDataService {
    void loadContagionAreasData(MultipartFile ContagionAreasFile, Long adminId);
}
