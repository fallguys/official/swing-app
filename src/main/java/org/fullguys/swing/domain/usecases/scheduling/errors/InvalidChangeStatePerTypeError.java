package org.fullguys.swing.domain.usecases.scheduling.errors;

public class InvalidChangeStatePerTypeError extends RuntimeException{
    public InvalidChangeStatePerTypeError(){
        super("No es posible cambiar el estado dentro de ese tipo");
    }
}
