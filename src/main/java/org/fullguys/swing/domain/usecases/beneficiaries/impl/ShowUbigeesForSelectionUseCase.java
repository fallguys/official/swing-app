package org.fullguys.swing.domain.usecases.beneficiaries.impl;

import lombok.RequiredArgsConstructor;
import org.fullguys.swing.common.annotations.UseCase;
import org.fullguys.swing.domain.entities.Ubigee;
import org.fullguys.swing.domain.entities.enums.UbigeeType;
import org.fullguys.swing.domain.usecases.beneficiaries.ports.in.services.ShowUbigeesService;
import org.fullguys.swing.domain.usecases.beneficiaries.ports.out.persistence.ShowUbigeesPort;

import java.util.List;

@UseCase
@RequiredArgsConstructor
public class ShowUbigeesForSelectionUseCase implements ShowUbigeesService {
    private final ShowUbigeesPort showUbigeesPort;

    @Override
    public List<Ubigee> showUbigees(Long id, UbigeeType ubigeeType) {
        return showUbigeesPort.showUbigees(id, ubigeeType);
    }
}
