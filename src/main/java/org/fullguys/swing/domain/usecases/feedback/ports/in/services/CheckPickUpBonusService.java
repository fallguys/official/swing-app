package org.fullguys.swing.domain.usecases.feedback.ports.in.services;

import org.fullguys.swing.domain.usecases.feedback.commands.CheckBonusCommand;

public interface CheckPickUpBonusService {
    CheckBonusCommand checkPickUpBonus(String codeBeneficiary, String codePoint);
}
