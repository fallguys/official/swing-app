package org.fullguys.swing.domain.usecases.incidents.impl;

import lombok.RequiredArgsConstructor;
import org.fullguys.swing.common.annotations.UseCase;
import org.fullguys.swing.common.queries.Filters;
import org.fullguys.swing.common.queries.Paginator;
import org.fullguys.swing.domain.entities.IncidentType;
import org.fullguys.swing.domain.usecases.incidents.ports.in.services.ShowIncidentTypesService;
import org.fullguys.swing.domain.usecases.incidents.ports.out.persistence.ShowIncidentTypesPortForAdmin;

import java.time.ZoneId;
import java.util.Date;


@UseCase
@RequiredArgsConstructor
public class ShowIncidentTypesUseCase implements ShowIncidentTypesService {
    private final ShowIncidentTypesPortForAdmin showIncidentTypesPortForAdmin;

    @Override
    public Paginator<IncidentType> showIncidentTypes(Filters filters) {
        return showIncidentTypesPortForAdmin.showIncidentTypes(filters);
    }
}
