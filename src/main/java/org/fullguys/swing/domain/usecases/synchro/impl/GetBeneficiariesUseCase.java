package org.fullguys.swing.domain.usecases.synchro.impl;

import lombok.RequiredArgsConstructor;
import org.fullguys.swing.common.annotations.UseCase;
import org.fullguys.swing.common.queries.Filters;
import org.fullguys.swing.domain.entities.enums.BeneficiaryState;
import org.fullguys.swing.domain.entities.enums.SynchroFileType;
import org.fullguys.swing.domain.entities.enums.SynchroState;
import org.fullguys.swing.domain.usecases.synchro.commands.BeneficiaryCommand;
import org.fullguys.swing.domain.usecases.synchro.ports.in.services.GetBeneficiariesService;
import org.fullguys.swing.domain.usecases.synchro.ports.out.persistence.CountBeneficiariesByStatusPort;
import org.fullguys.swing.domain.usecases.synchro.ports.out.persistence.GetSynchroPort;
import org.fullguys.swing.domain.usecases.synchro.ports.out.persistence.ListBeneficiariesPort;

@RequiredArgsConstructor
@UseCase
public class GetBeneficiariesUseCase implements GetBeneficiariesService {

    private final ListBeneficiariesPort listBeneficiariesPort;
    private final CountBeneficiariesByStatusPort countBeneficiariesByStatusPort;
    private final GetSynchroPort getSynchroPort;

    @Override
    public BeneficiaryCommand getBeneficiaries(Filters filters) {

        var beneficiaries = listBeneficiariesPort.listBeneficiaries(filters);
        var synchro = getSynchroPort.getLastActiveSynchro(SynchroFileType.BENEFICIARY, SynchroState.COMPLETED);
        var actives = countBeneficiariesByStatusPort.countByState(BeneficiaryState.ACTIVE);

        BeneficiaryCommand beneficiaryCommand = new BeneficiaryCommand();
        beneficiaryCommand.setPaginator(beneficiaries);
        if(synchro.isPresent()){
            beneficiaryCommand.setLastUpdated(synchro.get().getInitialDate());
        }
        beneficiaryCommand.setTotalActive(actives);

        return beneficiaryCommand;
    }
}
