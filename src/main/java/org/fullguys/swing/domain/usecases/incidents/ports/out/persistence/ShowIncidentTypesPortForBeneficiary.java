package org.fullguys.swing.domain.usecases.incidents.ports.out.persistence;

import org.fullguys.swing.domain.entities.IncidentType;

import java.util.List;

public interface ShowIncidentTypesPortForBeneficiary {
    List<IncidentType> showIncidentTypes();
}
