package org.fullguys.swing.domain.usecases.synchro.ports.out.persistence;

import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;

public interface SaveBeneficiaryFilePort {
    String saveBeneficiaryFile(MultipartFile file);
}
