package org.fullguys.swing.domain.usecases.incidents.ports.in.services;

import org.fullguys.swing.common.queries.Filters;
import org.fullguys.swing.common.queries.Paginator;
import org.fullguys.swing.domain.entities.IncidentType;

import java.util.Date;

public interface ShowIncidentTypesService {
    Paginator<IncidentType> showIncidentTypes(Filters filters);
}
