package org.fullguys.swing.domain.usecases.scheduling.impl;

import lombok.RequiredArgsConstructor;
import org.fullguys.swing.common.annotations.UseCase;
import org.fullguys.swing.domain.entities.Schedule;
import org.fullguys.swing.domain.entities.enums.ScheduleState;
import org.fullguys.swing.domain.entities.enums.ScheduleType;
import org.fullguys.swing.domain.usecases.scheduling.ports.in.services.GetLastOfficialScheduleService;
import org.fullguys.swing.domain.usecases.scheduling.ports.out.persistence.GetLastScheduleRegisteredPort;

import java.util.Optional;

@UseCase
@RequiredArgsConstructor
public class GetLastOfficialScheduleUseCase implements GetLastOfficialScheduleService {

    private final GetLastScheduleRegisteredPort getLastScheduleRegisteredPort;

    @Override
    public Optional<Schedule> getLast() {
        // Programmed schedule is always greater than current active schedule
        var programmed = getLastScheduleRegisteredPort.getLastRegistered(ScheduleType.OFFICIAL, ScheduleState.PROGRAMMED);
        if (programmed.isPresent()) {
            return programmed;
        }

        // Active schedule is the last one if no programmed schedules exists.
        // Doesn't count Finished schedules, they're in the history.
        return getLastScheduleRegisteredPort.getLastRegistered(ScheduleType.OFFICIAL, ScheduleState.ACTIVE);
    }
}
