package org.fullguys.swing.domain.usecases.scheduling.impl;

import lombok.RequiredArgsConstructor;
import org.fullguys.swing.common.annotations.UseCase;
import org.fullguys.swing.common.queries.Filters;
import org.fullguys.swing.common.queries.Paginator;
import org.fullguys.swing.domain.entities.Assignment;
import org.fullguys.swing.domain.usecases.scheduling.ports.in.services.ShowBeneficiariesInfoService;
import org.fullguys.swing.domain.usecases.scheduling.ports.in.services.ShowTotalIncidentBeneficiaryService;
import org.fullguys.swing.domain.usecases.scheduling.ports.out.scheduler.ShowBeneficiariesInfoPort;
import org.fullguys.swing.domain.usecases.scheduling.ports.out.scheduler.ShowTotalIncidentBeneficiaryPort;

@UseCase
@RequiredArgsConstructor
public class ShowBeneficiariesInfoUseCase implements ShowBeneficiariesInfoService, ShowTotalIncidentBeneficiaryService {
    private final ShowBeneficiariesInfoPort showBeneficiariesInfoPort;
    private final ShowTotalIncidentBeneficiaryPort showTotalIncidentBeneficiaryPort;
    @Override
    public Paginator<Assignment> showBeneficiariesInfo(Filters filters) {
        return showBeneficiariesInfoPort.showBeneficiariesInfo(filters);
    }

    @Override
    public Integer showTotalIncidentsForBeneficiary(String beneficiaryCode) {
        return showTotalIncidentBeneficiaryPort.showTotalIncidentsForBeneficiary(beneficiaryCode);
    }
}
