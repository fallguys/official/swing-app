package org.fullguys.swing.domain.usecases.scheduling.errors;

import lombok.Getter;
import org.fullguys.swing.common.formatter.TimeFormatter;

import java.time.LocalDate;

@Getter
public class ScheduleNotAllowBeforeLastOfficialError extends RuntimeException {
    private final LocalDate initialDate;
    private final LocalDate officialFinalDate;

    public ScheduleNotAllowBeforeLastOfficialError(LocalDate initialDate, LocalDate officialFinalDate) {
        super(String.format("No se puede registrar un cronograma oficial (%s) antes del término del último oficial programado (%s)",
                initialDate.format(TimeFormatter.DATE),
                officialFinalDate.format(TimeFormatter.DATE)));

        this.initialDate = initialDate;
        this.officialFinalDate = officialFinalDate;
    }
}
