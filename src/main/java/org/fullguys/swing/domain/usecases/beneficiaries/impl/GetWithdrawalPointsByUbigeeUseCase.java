package org.fullguys.swing.domain.usecases.beneficiaries.impl;

import lombok.RequiredArgsConstructor;
import org.fullguys.swing.common.annotations.UseCase;
import org.fullguys.swing.domain.entities.WithdrawalPoint;
import org.fullguys.swing.domain.usecases.beneficiaries.ports.in.services.GetWithdrawalPointsByUbigeeService;
import org.fullguys.swing.domain.usecases.beneficiaries.ports.out.persistence.GetWithdrawalPointsByUbigeePort;

import java.util.List;

@UseCase
@RequiredArgsConstructor
public class GetWithdrawalPointsByUbigeeUseCase implements GetWithdrawalPointsByUbigeeService {
    private final GetWithdrawalPointsByUbigeePort getWithdrawalPointsByUbigeePort;

    @Override
    public List<WithdrawalPoint> getWithdrawalPointsByUbigee(String ubigee) {
        return getWithdrawalPointsByUbigeePort.getWithdrawalPointsByUbigee(ubigee);
    }
}
