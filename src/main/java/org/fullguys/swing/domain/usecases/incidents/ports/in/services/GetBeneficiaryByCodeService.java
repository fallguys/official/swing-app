package org.fullguys.swing.domain.usecases.incidents.ports.in.services;

import org.fullguys.swing.domain.entities.Beneficiary;

public interface GetBeneficiaryByCodeService {
    Beneficiary getBeneficiaryByCode(String code);
}
