package org.fullguys.swing.domain.usecases.incidents.impl;

import lombok.RequiredArgsConstructor;
import org.fullguys.swing.common.annotations.UseCase;
import org.fullguys.swing.config.parameter.TimerParameters;
import org.fullguys.swing.domain.entities.Beneficiary;
import org.fullguys.swing.domain.entities.Incident;
import org.fullguys.swing.domain.usecases.incidents.errors.BeneficiaryDoesntExistError;
import org.fullguys.swing.domain.usecases.incidents.ports.in.services.GetBeneficiaryByCodeService;
import org.fullguys.swing.domain.usecases.incidents.ports.in.services.RegisterIncidentService;
import org.fullguys.swing.domain.usecases.incidents.ports.out.persistence.GetBeneficiaryByCodePort;
import org.fullguys.swing.domain.usecases.incidents.ports.out.persistence.SaveIncidentPort;

import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.time.ZoneId;

@UseCase
@RequiredArgsConstructor
public class RegisterIncidentUseCase implements RegisterIncidentService, GetBeneficiaryByCodeService {

    private final SaveIncidentPort saveIncidentPort;
    private final GetBeneficiaryByCodePort getBeneficiaryByCodePort;
    private final TimerParameters timerParameters;

    @Override
    public Incident register(Incident incident) {
        var today = LocalDateTime.now(ZoneId.of(timerParameters.getZone()));
        incident.setReportedDate(Timestamp.valueOf(today));
        return saveIncidentPort.save(incident);
    }

    @Override
    public Beneficiary getBeneficiaryByCode(String code) {
        return getBeneficiaryByCodePort.getBeneficiaryByCode(code).orElseThrow(()-> new BeneficiaryDoesntExistError(code));
    }
}
