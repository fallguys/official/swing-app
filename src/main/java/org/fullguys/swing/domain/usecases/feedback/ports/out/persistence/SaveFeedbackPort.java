package org.fullguys.swing.domain.usecases.feedback.ports.out.persistence;

import org.fullguys.swing.domain.entities.Feedback;
import org.fullguys.swing.domain.usecases.feedback.commands.CheckBonusCommand;

public interface SaveFeedbackPort {

    Feedback saveFeedback(CheckBonusCommand checkBonusCommand);
}
