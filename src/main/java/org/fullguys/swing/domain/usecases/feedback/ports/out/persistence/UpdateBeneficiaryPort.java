package org.fullguys.swing.domain.usecases.feedback.ports.out.persistence;

public interface UpdateBeneficiaryPort {
    void updateHitBeneficiary(Long idBeneficiary);
    void updateInfractionBeneficiary(Long idBeneficiary);
}
