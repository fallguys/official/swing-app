package org.fullguys.swing.domain.usecases.scheduling.ports.in.services;

public interface ShowTotalIncidentBeneficiaryService {
    Integer showTotalIncidentsForBeneficiary(String beneficiaryCode);
}
