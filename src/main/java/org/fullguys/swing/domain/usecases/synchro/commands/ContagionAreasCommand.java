package org.fullguys.swing.domain.usecases.synchro.commands;

import lombok.Data;
import org.fullguys.swing.common.queries.Paginator;
import org.fullguys.swing.domain.entities.ContagionArea;

import java.time.LocalDateTime;

@Data
public class ContagionAreasCommand {
    private Paginator<ContagionArea> paginator;
    private Long totalActive;
    private LocalDateTime lastUpdated;
}
