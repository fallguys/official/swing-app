package org.fullguys.swing.domain.usecases.feedback.ports.in.services;

import org.fullguys.swing.domain.entities.Assignment;
import org.fullguys.swing.domain.entities.WithdrawalPoint;

import java.util.Optional;

public interface GetParticipantWithdrawalPointService {
    WithdrawalPoint getParticipantWithdrawalPoint(String codeWithdrawalPoint);
}
