package org.fullguys.swing.domain.usecases.feedback.impl;

import lombok.RequiredArgsConstructor;
import org.fullguys.swing.common.annotations.UseCase;
import org.fullguys.swing.domain.entities.WithdrawalPoint;
import org.fullguys.swing.domain.usecases.feedback.errors.CurrentScheduleNotFoundError;
import org.fullguys.swing.domain.usecases.feedback.errors.WithdrawalPointNotFoundError;
import org.fullguys.swing.domain.usecases.feedback.ports.in.services.GetParticipantWithdrawalPointService;
import org.fullguys.swing.domain.usecases.feedback.ports.out.persistence.GetCurrentSchedulePort;
import org.fullguys.swing.domain.usecases.feedback.ports.out.persistence.GetParticipantWithdrawalPointPort;

@UseCase
@RequiredArgsConstructor
public class GetParticipantWithdrawalPointUseCase implements GetParticipantWithdrawalPointService {

    private final GetParticipantWithdrawalPointPort getParticipantWithdrawalPointPort;
    private final GetCurrentSchedulePort getCurrentSchedulePort;

    @Override
    public WithdrawalPoint getParticipantWithdrawalPoint(String codeWithdrawalPoint) {

        var schedule = getCurrentSchedulePort
                .getCurrentSchedule()
                .orElseThrow(() -> new CurrentScheduleNotFoundError());

        var withdrawalPoint = getParticipantWithdrawalPointPort
                .getParticipantWithdrawalPoint(codeWithdrawalPoint, schedule.getId())
                .orElseThrow(() -> new WithdrawalPointNotFoundError(codeWithdrawalPoint));

        return withdrawalPoint;
    }
}

