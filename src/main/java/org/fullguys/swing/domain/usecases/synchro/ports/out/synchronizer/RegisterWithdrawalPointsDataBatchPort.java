package org.fullguys.swing.domain.usecases.synchro.ports.out.synchronizer;

public interface RegisterWithdrawalPointsDataBatchPort {
    void registerWithdrawalPointsDataBatch();
}
