package org.fullguys.swing.domain.usecases.incidents.ports.in.services;

import org.fullguys.swing.common.queries.Filters;
import org.fullguys.swing.common.queries.Paginator;
import org.fullguys.swing.domain.entities.Incident;

import java.util.List;

public interface ShowIncidentService {
    Paginator<Incident> showIncident(Filters filters);
}
