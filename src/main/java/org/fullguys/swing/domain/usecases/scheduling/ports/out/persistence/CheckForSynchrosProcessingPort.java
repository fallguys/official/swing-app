package org.fullguys.swing.domain.usecases.scheduling.ports.out.persistence;

public interface CheckForSynchrosProcessingPort {
    boolean checkForSynchrosProcessing();
}
