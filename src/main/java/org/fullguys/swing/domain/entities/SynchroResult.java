package org.fullguys.swing.domain.entities;

import lombok.Data;

@Data
public class SynchroResult {
    private String sourceDataFile;
    private String errorDataFile;
    private Long totalRegistries;
    private Long successfullRegistries;
    private Long failedRegistries;
}
