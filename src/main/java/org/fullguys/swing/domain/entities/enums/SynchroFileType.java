package org.fullguys.swing.domain.entities.enums;

public enum SynchroFileType {
    BENEFICIARY,
    WITHDRAWAL_POINT,
    CONTAGION_AREA
}
