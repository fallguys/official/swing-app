package org.fullguys.swing.domain.entities;

import lombok.Data;
import org.fullguys.swing.domain.entities.enums.UbigeeType;

@Data
public class Ubigee {
    private long id;
    private Ubigee fatherUbigee;
    private String code;
    private String name;
    private UbigeeType type;
}
