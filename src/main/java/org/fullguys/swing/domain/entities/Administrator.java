package org.fullguys.swing.domain.entities;

import lombok.Data;

@Data
public class Administrator {
    private Long id;
    private String names;
    private String fatherLastname;
    private String motherLastname;
    private String email;
    private String password;
}
