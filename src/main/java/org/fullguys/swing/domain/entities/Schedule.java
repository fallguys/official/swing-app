package org.fullguys.swing.domain.entities;

import lombok.Data;
import org.fullguys.swing.domain.entities.enums.ScheduleState;
import org.fullguys.swing.domain.entities.enums.ScheduleType;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;

@Data
public class Schedule {
    private Long id;
    private Administrator administrator;
    private LocalDateTime creationDate;
    private LocalDate initialDate;
    private LocalDate finalDate;
    private ScheduleType type;
    private ScheduleState state;
    private List<Assignment> assignments;
}
