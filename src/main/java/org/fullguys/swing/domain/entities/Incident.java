package org.fullguys.swing.domain.entities;

import lombok.Data;

import java.util.Date;

@Data
public class Incident {
    private Long id;
    private String message;
    private Date reportedDate;
    private IncidentType type;
    private Beneficiary beneficiary;
}
