package org.fullguys.swing.domain.entities.enums;

public enum ScheduleType {
    OFFICIAL,
    TEST;

    public boolean is(ScheduleType type) {
        return type.equals(this);
    }

    public boolean isOfficial() {
        return this.equals(ScheduleType.OFFICIAL);
    }

    public boolean isTest() {
        return this.equals(ScheduleType.TEST);
    }
}
