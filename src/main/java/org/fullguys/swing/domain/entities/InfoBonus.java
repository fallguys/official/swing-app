package org.fullguys.swing.domain.entities;

import lombok.Data;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Date;

@Data
public class InfoBonus {
    private LocalDateTime assignedInitialDate;
    private LocalDateTime assignedFinalDate;
    private LocalDateTime bonusRetiredDate;
    private Boolean bonusRetired;
}
