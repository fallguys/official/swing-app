package org.fullguys.swing.domain.entities.enums;

import java.util.Arrays;

public enum ScheduleState {
    PROCESSING,
    AVAILABLE,
    DISCARTED,
    CANCELED,
    PROGRAMMED,
    ACTIVE,
    FINISHED;

    public boolean isIn(ScheduleState... states) {
        return Arrays.asList(states).contains(this);
    }

    public boolean is(ScheduleState states) {
        return states.equals(this);
    }
}
