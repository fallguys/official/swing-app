package org.fullguys.swing.domain.entities.enums;

public enum SynchroState {
    IN_PROCESS,
    INCOMPLETE,
    FAILED,
    COMPLETED
}
