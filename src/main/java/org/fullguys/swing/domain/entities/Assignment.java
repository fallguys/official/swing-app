package org.fullguys.swing.domain.entities;

import lombok.Data;

@Data
public class Assignment {
    private Beneficiary beneficiary;
    private WithdrawalPoint withdrawalPoint;
    private InfoBonus infoBonus;
}
