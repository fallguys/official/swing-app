package org.fullguys.swing.domain.entities;

import lombok.Data;

import java.util.Date;

@Data
public class Feedback {
    private Date reportedErrorDate;
    private Assignment assignment;
    private WithdrawalPoint withdrawalPoint;
}
