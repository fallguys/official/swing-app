package org.fullguys.swing.domain.entities.enums;

public enum BeneficiaryState {
    ACTIVE,
    INACTIVE
}
