package org.fullguys.swing.domain.entities.enums;

public enum WithdrawalPointState {
    ACTIVE,
    INACTIVE
}
