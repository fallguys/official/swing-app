package org.fullguys.swing.domain.entities;

import lombok.Data;
import org.fullguys.swing.domain.entities.enums.BeneficiaryState;
import org.fullguys.swing.domain.entities.enums.Gender;

import java.time.LocalDateTime;
import java.util.Date;
import java.util.List;

@Data
public class Beneficiary {
    private long id;
    private Ubigee district;
    private String code;
    private Boolean isOlderAdult;
    private Boolean isDisabled;
    private Gender gender;
    private BeneficiaryState state;
    private LocalDateTime createdAt;
    private LocalDateTime updatedAt;
    private List<WithdrawalPoint> pointsChoosed;
}
