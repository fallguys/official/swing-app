package org.fullguys.swing.domain.entities;

import lombok.Data;

import java.time.LocalDateTime;
import java.util.Date;

@Data
public class ContagionArea {
    private Long id;
    private Ubigee district;
    private Long quantity;
    private LocalDateTime createdAt;
    private LocalDateTime updatedAt;
}
