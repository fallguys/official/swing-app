package org.fullguys.swing.domain.entities.enums;

public enum IncidentTypeState {
    ENABLED,
    DISABLED
}
