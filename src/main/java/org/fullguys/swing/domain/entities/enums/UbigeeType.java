package org.fullguys.swing.domain.entities.enums;

public enum UbigeeType {
    DEPARTMENT,
    PROVINCE,
    DISTRICT
}
