package org.fullguys.swing.domain.entities;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import org.fullguys.swing.domain.entities.enums.SynchroFileType;
import org.fullguys.swing.domain.entities.enums.SynchroState;

import java.time.LocalDateTime;

@Data @Builder
@AllArgsConstructor
public class Synchro {
    private Long id;
    private Administrator administrator;
    private SynchroFileType fileType;
    private LocalDateTime initialDate;
    private SynchroResult result;
    private SynchroState state;
}
