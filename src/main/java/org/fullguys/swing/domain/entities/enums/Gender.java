package org.fullguys.swing.domain.entities.enums;

public enum Gender {
    MALE("M"),
    FEMALE("F"),
    NO_ESPECIFIED("NE");

    private final String value;

    Gender(String value) {
        this.value = value;
    }
    public String getValue() {
        return this.value;
    }
}
