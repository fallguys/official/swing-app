package org.fullguys.swing.domain.entities;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import org.fullguys.swing.domain.entities.enums.IncidentTypeState;

import java.util.Date;

@Data
@Builder
@AllArgsConstructor
public class IncidentType {
    private Long id;
    private String name;
    private String description;
    private Date createdAt;
    private IncidentTypeState state;
}
