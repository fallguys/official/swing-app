# Core y Web API / Sistema Swing

Proyecto del curso Desarrollo de Programas 1.

## Base de Datos
Ingrese a Postgres para ejecutar comandos SQL.
Luego, ejecute los siguientes comandos:

1. Habilitar el uso de `UUID` como `PK`s usando un plugin de postgres:
    ```sql
    CREATE EXTENSION IF NOT EXISTS "uuid-ossp";
    ```

2. Crear la base de datos con todas las credenciales necesarias para la aplicación:
    ```sql
    CREATE DATABASE swing_app;
    CREATE USER swing_app WITH ENCRYPTED PASSWORD 'swing_app';
    GRANT ALL PRIVILEGES ON DATABASE swing_app TO swing_app;
    ```
   
   Las credenciales serían las siguientes:
   - base de datos: `swing_app`
   - usuario: `swing_app`
   - contraseña: `swing_app`

## Producción
La imagen docker de este proyecto se encuentra en los registros de GitLab.

Para probar el servicio, debe crear un archivo `production.env`, el cual lo puede
crear usando la plantilla `production.env.example`. Aquí se colocarán las credenciales de
los servicios externos como:
- AWS Batch 
- AWS Elastic Beanstalk
- Base de datos PostGreSQL

Para descargar y probar el contenedor, puede correr el siguiente comando:
```bash
docker-compose up
```

Con esto, obtendrá la imagen que se indica en la etiqueta del servicio.

__Observación:__ No se garantiza que exista una imagen 100% funcional por el momento, descargando
la imagen descrita en `docker-compose.yml`.

Puede llevar seguimiento de las imágenes publicadas
en:
```
https://gitlab.com/fallguys/official/swing-app/container_registry
```

Las etiquetas que se manejan son acorde al ambiente a desplegar y a los commits en los que fueron
generados:
- `staging-<COMMIT-HASH>`: imagen para ambiente de QA, no asegura pruebas
- `release-<COMMIT-HASH>`: imagen para ambiente de producción, asegura pruebas (esperamos)

