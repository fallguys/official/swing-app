#!/bin/bash

docker run                                                                              \
    -d                                                                                  \
    --restart   unless-stopped                                                          \
    --name      <container-name>                                                        \
    -e          POSTGRES_PASSWORD=<password-admin>                                      \
    -p          5432:5432                                                               \
    -v          <data-directory-route>:/var/lib/postgresql/data                         \
    postgres:12.4-alpine
