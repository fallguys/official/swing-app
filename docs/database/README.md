# Indicaciones para instancia Postgres

## Usando docker
Si tiene instalado docker:

1. deberá colocar las nuevas credenciales de su base de datos en el archivo `img_postgres.sh`:

    ```bash
    docker run                                                      \
        -d                                                          \
        --restart   unless-stopped                                  \
        --name      <container-name>                                \
        -e          POSTGRES_PASSWORD=<password-admin>              \
        -p          5432:5432                                       \
        -v          <data-directory-route>:/var/lib/postgresql/data \
        postgres:12.4-alpine
    ```
    
    En donde:
    - `<postgres-name>`: nombre del contenedor postgres.
    - `<password-admin>`: contraseña del acceso a la base de datos como administrador
    - `<data-directory-route>`: dirección de la carpeta en donde se guardarán los datos y no se pierdan cuando el contenedor
    muera o se detenga. En caso suceda que el contenedor deje de correr, los datos se estarán guardando en dicha carpeta y
    otro contenedor puede tomar la posta.

2. Luego, deberá correr el archivo configurado
    de la siguiente manera:
    ```bash
    bash img_postgres.sh
    ```
    
    __Observación:__ En caso no se le permita correr el comando especificado por falta de permisos, es porque correr el
    comando `docker` solo lo puede realizar el usuario root. Pruebe utilizando `sudo` antes del comando.


## Ingresando a la base de datos
En caso haya instanciado postgres como contenedor:

1. Ingresa en modo interactivo al contenedor de postgres:
    ```bash
    docker exec -it <container-name> bash 
    ```
    - `<container-name>`: nombre del contenedor que declaró en los pasos anteriores
    Este comando le permitirá entrar al Sistema Operativo simulado en donde está corriendo Postgres.

2. Ingresa a Postgres:
    ```bash
    psql -U postgres
    ```
   Este comando le permitirá ahora navegar en modo consola por Postgres y realizar consultas a la base de datos
   que por defecto se crea en todo BD Postgres. Desde aquí ya podrá ejecutar comandos SQL con privilegios de root.
