module.exports = {
  title: 'Sistema Swing',
  tagline: 'Administrar la entrega de bonos nunca fue tan fácil',
  url: 'https://your-docusaurus-test-site.com',
  baseUrl: '/official/swing-app/',
  onBrokenLinks: 'throw',
  onBrokenMarkdownLinks: 'warn',
  favicon: 'img/favicon.ico',
  organizationName: 'fullguys', // Usually your GitHub org/user name.
  projectName: 'swing-app', // Usually your repo name.
  themeConfig: {
    navbar: {
      title: 'Sistema Swing',
      logo: {
        alt: 'proyecto swing',
        src: 'img/logo.svg',
      },
      items: [
        {
          to: 'docs/',
          activeBasePath: 'docs',
          label: 'Docs',
          position: 'left',
        },
        {
          href: 'https://gitlab.com/fallguys/official',
          label: 'GitLab',
          position: 'right',
        },
      ],
    },
    footer: {
      style: 'dark',
      links: [
        {
          title: 'Docs',
          items: [
            {
              label: 'Guía de instalación',
              to: 'docs/',
            },
            {
              label: 'Guía para usuarios',
              to: 'docs/beneficiary-consult',
            },
          ],
        },
        {
          title: 'Sobre nosotros',
          items: [
            {
              label: 'Equipo',
              href: 'https://gitlab.com/fallguys',
            },
            {
              label: 'GitLab',
              href: 'https://gitlab.com/fallguys/official',
            },
          ],
        },
      ],
      copyright: `Copyright © ${new Date().getFullYear()} Fullguys, Inc. Hecho con Docusaurus.`,
    },
  },
  presets: [
    [
      '@docusaurus/preset-classic',
      {
        docs: {
          sidebarPath: require.resolve('./sidebars.js'),
          // Please change this to your repo.
          editUrl:
            'https://github.com/facebook/docusaurus/edit/master/website/',
        },
        blog: {
          showReadingTime: true,
          // Please change this to your repo.
          editUrl:
            'https://github.com/facebook/docusaurus/edit/master/website/blog/',
        },
        theme: {
          customCss: require.resolve('./src/css/custom.css'),
        },
      },
    ],
  ],
};
