module.exports = {
  docs: [
    {
      type: 'category',
      label: 'Instalación',
      items: [
        {
          type: 'doc',
          id: 'installation/installation-start',
        },
        {
          type: 'category',
          label: 'Imágenes de programas',
          items:[
            {
              type: 'doc',
              id: 'installation/docker-images/installation-images'
            },
            {
              type: 'category',
              label: 'Imágenes',
              items:[
                'installation/docker-images/installation-images-app',
                'installation/docker-images/installation-images-web',
                'installation/docker-images/installation-images-synchro',
                'installation/docker-images/installation-images-scheduler',
              ],
            },
          ],
        },
        {
          type: 'category',
          label: 'Servicios AWS',
          items:[
            'installation/aws-services/installation-services-database',
            'installation/aws-services/installation-services-storage',
            'installation/aws-services/installation-services-batch',
            'installation/aws-services/installation-services-beanstalk',
          ],
        },
      ],
    },
    {
      type: 'category',
      label: 'Guía para usuarios',
      items: [
        {
          type: 'category',
          label: 'Beneficiario',
          items:[
            'user-guide/beneficiary/beneficiary-consult',
            'user-guide/beneficiary/beneficiary-report-an-incident'
          ],
        },
        {
          type: 'category',
          label: 'Administrador',
          items:[
            {
              type: 'category',
              label: 'Sincronización',
              items:[
                'user-guide/administrator/sync/admin-history',
                'user-guide/administrator/sync/admin-sync-beneficiary',
                'user-guide/administrator/sync/admin-sync-withdrawal-points',
                'user-guide/administrator/sync/admin-sync-contagion-areas',
                'user-guide/administrator/sync/admin-sync',
              ],
            },
            {
              type: 'category',
              label: 'Cronogramas',
              items:[
                'user-guide/administrator/schedule/admin-schedule-history',
                'user-guide/administrator/schedule/admin-schedule-generate',
              ],
            },
            {
              type: 'category',
              label: 'Incidentes',
              items:[               
                'user-guide/administrator/incidents/show-incident',
                'user-guide/administrator/incidents/manage-incident-type' 
              ],
            }
          ],
        },
        {
          type: 'category',
          label: 'Simulador de bancos',
          items:[
            'user-guide/bank-simulation/simulator-tasks',
          ],
        },
      ],
    }
  ],
};
