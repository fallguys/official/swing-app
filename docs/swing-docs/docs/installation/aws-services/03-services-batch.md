---
id: installation-services-batch
title: Servicio para procesos Batch
sidebar_label: procesos Batch
slug: /installation/services/batch
---

El servicio de AWS Batch será usado para ejecutar los programas `swing-synchronizer` y `swing-scheduler` como jobs
para el preprocesamiento de datos a cargar y la generación de cronogramas.

Para la creación de servicios para estos programas, es necesario crear lo siguiente:
- entorno informático del Batch
- cola de trabajo del Batch (`job queue`)
- definición de trabajo del Batch (`job definition`)

## Variables de entorno
De este servicio, se obtendrán las credenciales de los dos servicios desplegados como jobs.

```properties
# Servicio AWS Batch para Scheduler
SCHEDULER_AWS_REGION=
SCHEDULER_AWS_JOB_NAME=
SCHEDULER_AWS_JOB_QUEUE=
SCHEDULER_AWS_JOB_DEFINITION=
SCHEDULER_AWS_CREDENTIALS_ACCESS_KEY=
SCHEDULER_AWS_CREDENTIALS_SECRET_KEY=

# Servicio AWS Batch para Synchronizer
SYNCHRONIZER_AWS_REGION=
SYNCHRONIZER_AWS_JOB_NAME=
SYNCHRONIZER_AWS_JOB_QUEUE=
SYNCHRONIZER_AWS_JOB_DEFINITION=
SYNCHRONIZER_AWS_CREDENTIALS_ACCESS_KEY=
SYNCHRONIZER_AWS_CREDENTIALS_SECRET_KEY=
```

## Pasos para despliegue
### Paso 1
Obtener las rutas de las imágenes para cada job. Seleccionar aquel publicado recientemente o buscar por versión según
su etiqueta. Aquellos a usar son los que inician con `release-<version>` como tag.

| Imagen               | Link al registro de contenedores del proyecto                                      |
|----------------------|------------------------------------------------------------------------------------|
| `swing-synchronizer` | https://gitlab.com/fallguys/official/swing-synchronizer/container_registry/1593712 |
| `swing-scheduler`    | https://gitlab.com/fallguys/official/swing-scheduler/container_registry/1593675    |

**Por ejemplo:**

| Imagen               | Link de la imagen escogida                                                |
|----------------------|---------------------------------------------------------------------------|
| `swing-synchronizer` | registry.gitlab.com/fallguys/official/swing-synchronizer:release-ba706c98 |
| `swing-scheduler`    | registry.gitlab.com/fallguys/official/swing-scheduler:release-4ac230cd    |

### Paso 2
Iniciar con la creación del entorno informático:

![img](../../../static/img/services/aws-batch-01-dashboard.png)

El nombre del entorno informático no se usará como credencial, pero sí se usará en los siguientes pasos. Las demás secciones
se llenan según los recursos a dar para el entorno.

![img](../../../static/img/services/aws-batch-02-environment.png)

### Paso 3
Crear la cola para los trabajos. Registrar el nombre de la cola para usarla como una de las variables de entorno a usar.

```properties
# Servicio AWS Batch para Synchronizer
SYNCHRONIZER_AWS_JOB_QUEUE=swing-synchronizer-queue
```

![img](../../../static/img/services/aws-batch-03-queue.png)
![img](../../../static/img/services/aws-batch-03-queue-environ.png)

### Paso 4
Crear la definición del trabajo usando la imagen del synchronizer. Registrar el nombre de la definición del trabajo
para usarlo como una de las variables de entorno.

`registry.gitlab.com/fallguys/official/swing-synchronizer:release-ba706c98`

```properties
# Servicio AWS Batch para Synchronizer
SYNCHRONIZER_AWS_JOB_DEFINITION=swing-synchronizer-definition
```

![img](../../../static/img/services/aws-batch-04-definition.png)

#### Agregar variables de entorno necesarias
Cada contenedor usará variables de entorno como credenciales para acceder a los archivos de carga del servicio de S3 y
la base de datos.


**Para el `swing-synchronizer`:**

```properties
# Base de datos
SPRING_DATASOURCE_URL=jdbc:postgresql://<domain>:<port>/<database>
SPRING_DATASOURCE_USERNAME=
SPRING_DATASOURCE_PASSWORD=

# Servicio AWS S3
STORAGE_AWS_REGION=
STORAGE_AWS_CREDENTIALS_ACCESS_KEY=
STORAGE_AWS_CREDENTIALS_SECRET_KEY=
STORAGE_AWS_BUCKET_NAME=
```


**Para el `swing-scheduler`:**

```properties
# Base de datos
SPRING_DATASOURCE_URL=jdbc:postgresql://<domain>:<port>/<database>
SPRING_DATASOURCE_USERNAME=
SPRING_DATASOURCE_PASSWORD=
```

Estas variables de entorno deben colocarse en la sección `Container properties / Configuración adicional / Variables de entorno`.

![img](../../../static/img/services/aws-batch-05-variables.png)

### Paso 5
Escoger un nombre para los trabajos que se pedirán ejecutar desde Swing App.

```properties
# Servicio AWS Batch para Synchronizer
SYNCHRONIZER_AWS_JOB_NAME=swing-synchronizer-job
```

:::info
Realizar los mismos pasos para el Swing Scheduler
:::
