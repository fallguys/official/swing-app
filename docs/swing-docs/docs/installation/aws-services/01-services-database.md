---
id: installation-services-database
title: Servicio para base de datos
sidebar_label: base de datos RDS
slug: /installation/services/database
---

Se utiliza el servicio de RDS para instanciar la base de datos relacional con las siguientes especificaciones:
- gestor de base de datos: PostgreSQL
- versión: 12.4

## Variables de entorno a obtener
```properties
# Base de datos
SPRING_DATASOURCE_URL=jdbc:postgresql://<domain>:<port>/<database>
SPRING_DATASOURCE_USERNAME=
SPRING_DATASOURCE_PASSWORD=
```

## Pasos
### Paso 1
Crear la base de datos según la plantilla de PostgreSQL de AWS. Escoger la versión `12.4-R1`.

![img](../../../static/img/services/aws-db-01-create-v2.png)

### Paso 2
Seleccionar la plantilla acorde al ambiente a desplegar. En la sección de `Settings / Credential Settings`, colocar
el nombre del usuario y contraseña del usuario root.

![img](../../../static/img/services/aws-db-02-template-v2.png)

### Paso 3
Configurar la disponibilidad y conectividad de la base de datos según la infraestructura y red de AWS que usará.

__Observación:__ En esta sección, se está habilitando el acceso público para ambiente de pruebas. Evitar exhibir la base
de datos y contenerlo en una red privada dentro de una VPC.

![img](../../../static/img/services/aws-db-03-connectivity-v2.png)

### Paso 4
Culminar con la configuración adicional de la base de datos y crear

![img](../../../static/img/services/aws-db-05-finish-v2.png)

### Paso 5
Obtener el endpoint para la conexión a la base de datos y crear la url

```properties
SPRING_DATASOURCE_URL=jdbc:postgresql://<domain>:<port>
```

![img](../../../static/img/services/aws-db-06-endpoint.png)


### Paso 6
Conectarse a la base de datos con algún software de conexión a BDs y crear el schema a usar por el Sistema Swing.

```sql
CREATE DATABASE swing_app_database;
CREATE USER swing_app WITH ENCRYPTED PASSWORD 'swing_app_password';
GRANT ALL PRIVILEGES ON DATABASE swing_app TO swing_app_user;
```

Las credenciales serían las siguientes:

```properties
# Base de datos
SPRING_DATASOURCE_URL=jdbc:postgresql://<domain>:<port>/swing_app_database
SPRING_DATASOURCE_USERNAME=swing_app_user
SPRING_DATASOURCE_PASSWORD=swing_app_password
```
