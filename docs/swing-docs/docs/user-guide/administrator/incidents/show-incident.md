---
id: show-incident
title: Consultar las incidencias
sidebar_label: Consulta de incidencias
slug: /administrator/incident/show-incident
---

Guía para visualizar las incidencias reportadas

## Consulta
*Precondición: Se asume que el administrador ya inició sesión*<br />
Para observar las incidencias registradas haga click [aqui](http://swing-web-client-release.eba-3matrg2z.us-east-1.elasticbeanstalk.com/admin/incident/reported-incidents). Se mostrará una pantalla como la siguiente.
![img](../../../../static/img/administrator/incidents/Administrador-Visualizar-Incidencia.png)
