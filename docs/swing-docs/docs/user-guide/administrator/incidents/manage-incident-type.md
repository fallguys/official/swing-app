---
id: manage-incident-type
title: Gestionar los tipos de incidencias
sidebar_label: Gestiona los tipos de incidencias
slug: /administrator/incident/manage-incident-type
---

Guía para registrar un tipo de incidencia

## Consulta
*Precondición: Se asume que el administrador ya inició sesión*<br />
Para observar los tipos de incidencia registrados haga click [aqui](http://swing-web-client-release.eba-3matrg2z.us-east-1.elasticbeanstalk.com/admin/incident/incident-types). Se mostrará una pantalla como la siguiente.
![img](../../../../static/img/administrator/incidents/Administrador-Visualizar-Tipo-Incidencia.png)

## Registro
*Precondición: Se asume que el administrador ya inició sesión*<br />
Para registrar un nuevo tipo de incidencia se seleccióna el botón de (``Nuevo Tipo``) el cual mostrará la siguiente ventana:
![img](../../../../static/img/administrator/incidents/Administrador-Registro-Nuevo-Tipo-Incidencia.png)
Tendrá que ingresar el nombre de tipo de incidencia en el campo (``Nombre``) de carácter obligatario y su descripción (``Descripción``), campo de carácter opcional.