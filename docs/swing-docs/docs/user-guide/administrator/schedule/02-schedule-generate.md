---
id: admin-schedule-generate
title: Generar un cronograma
sidebar_label: Generar un cronograma
slug: /schedule-generation
---

Guía para generar un cronograma

## Generar un cronograma
Para generar un cronograma primero debe hacer click en el boton de cronograma de la barra lateral, eso desplegará nuevas opciones en el menu lateral. Luego debe hacer click en Nuevo cronograma para ver la pantalla de generación de cronogramas tal y como se muestra en el ejemeplo.

![img](../../../../static/img/admin/schedule/Admin-schedule-generate.png)

Si no existe un cronograma que se está ejecutando se podrá crea nuevos cronogramas y para ello de selecccionar un día. Este día no puede ser un día que está siendo empleado por el cronograma que actualmente en vigencia. Y si se quiere que este cronograma sea __oficial__ debe activar el switch, en el caso de que solo quiera generar un cronograma de __prueba__ mantenga desactivado. 
Al hacer click en generar nuevo cronograma será redirigido al historial de cronogramas y podrá ver el estado en el que se encuentra su cronograma.

:::caution
En el caso de que un cronograma se encuentre generándose no se podrá generar otros cronogramas
:::

![img](../../../../static/img/admin/schedule/Admin-schedule-generate-pause.png)