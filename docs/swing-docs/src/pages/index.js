import React from 'react';
import clsx from 'clsx';
import Layout from '@theme/Layout';
import Link from '@docusaurus/Link';
import useDocusaurusContext from '@docusaurus/useDocusaurusContext';
import useBaseUrl from '@docusaurus/useBaseUrl';
import styles from './styles.module.css';

const features = [
  {
    title: 'Genera cronogramas de bonos',
    imageUrl: 'img/event.svg',
    description: (
      <>
        Swing esta diseñado para gestionar cronogramas de recojo
        de bonos usando puntos de retiro en todo el Perú.
      </>
    ),
  },
  {
    title: 'Consulta tu punto de retiro',
    imageUrl: 'img/map.svg',
    description: (
      <>
        Swing permite consultar a los beneficiarios participantes sobre
        sus puntos de retiro en el que puedan recoger sus bonos.
      </>
    ),
  },
  {
    title: 'Registra el recojo de bonos',
    imageUrl: 'img/cash.svg',
    description: (
      <>
        Swing ofrece a los encargados de los puntos de retiro registrar el
        recojo de los bonos por parte de los beneficiarios
      </>
    ),
  },
];

function Feature({imageUrl, title, description}) {
  const imgUrl = useBaseUrl(imageUrl);
  return (
    <div className={clsx('col col--4', styles.feature)}>
      {imgUrl && (
        <div className="text--center">
          <img className={styles.featureImage} src={imgUrl} alt={title} />
        </div>
      )}
      <h3>{title}</h3>
      <p>{description}</p>
    </div>
  );
}

function Home() {
  const context = useDocusaurusContext();
  const {siteConfig = {}} = context;
  const demoAdmin = 'http://swing-web-client-release.eba-3matrg2z.us-east-1.elasticbeanstalk.com/admin/home/login';
  const demoBenef = 'http://swing-web-client-release.eba-3matrg2z.us-east-1.elasticbeanstalk.com/bonus/consult';
  const demoSim = 'http://swing-web-client-release.eba-3matrg2z.us-east-1.elasticbeanstalk.com/sim';
  return (
    <Layout
      title={`${siteConfig.title}`}
      description="Description will go into a meta tag in <head />">
      <header className={clsx('hero hero--primary', styles.heroBanner)}>
        <div className="container">
          <h1 className="hero__title">{siteConfig.title}</h1>
          <p className="hero__subtitle">{siteConfig.tagline}</p>
          <div className={styles.buttons}>
            <Link
              className={clsx(
                'button button--outline button--secondary button--lg',
                styles.getStarted,
              )}
              to={demoAdmin}>
              Demo administrador
            </Link>
            <Link
              className={clsx(
                'button button--outline button--secondary button--lg',
                styles.getStarted,
              )}
              to={demoBenef}>
              Demo beneficiarios
            </Link>
            <Link
              className={clsx(
                'button button--outline button--secondary button--lg',
                styles.getStarted,
              )}
              to={demoSim}>
              Demo simulación
            </Link>
          </div>
        </div>
      </header>
      <main>
        {features && features.length > 0 && (
          <section className={styles.features}>
            <div className="container">
              <div className="row">
                {features.map((props, idx) => (
                  <Feature key={idx} {...props} />
                ))}
              </div>
            </div>
          </section>
        )}
      </main>
    </Layout>
  );
}

export default Home;
